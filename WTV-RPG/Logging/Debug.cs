﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;

namespace WTV_RPG.Logging
{
    static class Debug
    {
             /// <summary>
        /// Logs message to console.
        /// </summary>
        /// <param name="text">string to log.</param>
        public static void Log(string text)
        {
            API.shared.consoleOutput("[ Log ] " + text);
        }

    

    }
}
