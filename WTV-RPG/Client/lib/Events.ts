//**************************************
//********[Draw Menu Events]************
//**************************************

// On-Update Event -- Draws all of our stuff.
API.onUpdate.connect(() => {
	// Notifications can be global.
	drawNotification();
	drawTextNotification();

	if (!isReady) {
		return;
	}

	for (let i = 0; i < menuElements[currentPage].length; i++) {
		if (!isReady) {
			break;
		}
		menuElements[currentPage][i].draw();
		menuElements[currentPage][i].isHovered();
		menuElements[currentPage][i].isClicked();
	}
});