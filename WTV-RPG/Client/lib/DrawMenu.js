/// <reference path="../../types-gt-mp/index.d.ts" />
/// <reference path="_Vars.ts" />
// ReSharper disable once TsResolvedFromInaccessibleModule
/**
 * Initialize how many pages our menu is going to have.
 * @param pages - Number of pages.
 */
function createMenu(pages) {
    if (drawMenuCurrentMenu !== null) {
        isReady = false;
        currentPage = 0;
        selectedInput = null;
        tabIndex = [];
        menuElements = [];
    }
    drawMenuCurrentMenu = new MenuDraw(pages);
    return drawMenuCurrentMenu;
}
var MenuDraw = (function () {
    function MenuDraw(pages) {
        if (Array.isArray(menuElements[0])) {
            return;
        }
        for (var i = 0; i < pages; i++) {
            var emptyArray = [];
            menuElements.push(emptyArray);
        }
        this._blur = false;
    }
    Object.defineProperty(MenuDraw.prototype, "Ready", {
        get: function () {
            return isReady;
        },
        /** Start drawing our menu. */
        set: function (value) {
            isReady = value;
            currentPage = 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuDraw.prototype, "Blur", {
        get: function () {
            return this._blur;
        },
        /** Used to blur the background behind the menu. */
        set: function (value) {
            this._blur = value;
            if (value) {
                API.callNative("_TRANSITION_TO_BLURRED", 3000);
            }
            else {
                API.callNative("_TRANSITION_FROM_BLURRED", 3000);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuDraw.prototype, "Page", {
        get: function () {
            return currentPage;
        },
        /** Get the current menu page number or set the current menu page number */
        set: function (value) {
            currentPage = value;
        },
        enumerable: true,
        configurable: true
    });
    MenuDraw.prototype.DisableOverlays = function (value) {
        this._overlays = value;
        if (value) {
            API.setHudVisible(false);
            API.setChatVisible(false);
            API.setCanOpenChat(false);
            API.showCursor(true);
            return;
        }
        if (!value) {
            API.setHudVisible(true);
            API.setChatVisible(true);
            API.setCanOpenChat(true);
            API.showCursor(false);
            return;
        }
    };
    /**
     *  Delete any open menu instances.
     */
    MenuDraw.prototype.killMenu = function () {
        isReady = false;
        API.showCursor(false);
        API.setHudVisible(true);
        API.setChatVisible(true);
        API.setCanOpenChat(true);
        API.callNative("_TRANSITION_FROM_BLURRED", 3000);
    };
    MenuDraw.prototype.setLanguage = function (value) {
        language = value;
    };
    MenuDraw.prototype.nextPage = function () {
        if (currentPage + 1 > menuElements.length - 1) {
            currentPage = 0;
            return;
        }
        currentPage++;
    };
    MenuDraw.prototype.prevPage = function () {
        if (currentPage - 1 < 0) {
            currentPage = menuElements.length - 1;
            return;
        }
        currentPage--;
    };
    /**
     * Create a new panel.
     * @param page - The page you would like to add panels to.
     * @param xStart
     * @param yStart
     * @param xGridWidth
     * @param yGridHeight
     */
    MenuDraw.prototype.createPanel = function (page, xStart, yStart, xGridWidth, yGridHeight) {
        var newPanel = new Panel(page, xStart, yStart, xGridWidth, yGridHeight);
        menuElements[page].push(newPanel);
        return newPanel;
    };
    return MenuDraw;
}());
var ProgressBar = (function () {
    function ProgressBar(x, y, width, height, currentProgress) {
        this._xPos = x * panelMinX;
        this._yPos = y * panelMinY;
        this._width = width * panelMinX - 10;
        this._height = height * panelMinY - 10;
        this._currentProgress = currentProgress;
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._alpha = 255;
        this._drawText = true;
        API.sendChatMessage("Created");
    }
    ProgressBar.prototype.draw = function () {
        API.drawRectangle(this._xPos + 5, this._yPos + 5, ((this._width / 100) * this._currentProgress), this._height, this._r, this._g, this._b, this._alpha);
        if (this._drawText) {
            API.drawText("" + Math.round(this._currentProgress), this._xPos + (((this._width / 100) * this._currentProgress) / 2), this._yPos, 0.5, 255, 255, 255, 255, 4, 1, false, true, 100);
        }
    };
    ProgressBar.prototype.setColor = function (r, g, b) {
        this._r = r;
        this._g = g;
        this._b = b;
    };
    Object.defineProperty(ProgressBar.prototype, "Alpha", {
        /** The alpha property for the bar. */
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressBar.prototype, "DrawText", {
        /** Draw any text? */
        set: function (value) {
            this._drawText = value;
        },
        enumerable: true,
        configurable: true
    });
    ProgressBar.prototype.addProgress = function (value) {
        if (this._currentProgress + value > 100) {
            this._currentProgress = 100;
            return;
        }
        this._currentProgress += value;
    };
    ProgressBar.prototype.subtractProgress = function (value) {
        if (this._currentProgress - value < 0) {
            this._currentProgress = 0;
            return;
        }
        this._currentProgress -= value;
    };
    ProgressBar.prototype.setProgressAmount = function (value) {
        if (value >= 100) {
            this._currentProgress = 100;
            return;
        }
        if (value <= 0) {
            this._currentProgress = 0;
            return;
        }
        this._currentProgress = value;
        return;
    };
    ProgressBar.prototype.returnProgressAmount = function () {
        return this._currentProgress;
    };
    ProgressBar.prototype.returnType = function () {
        return "ProgressBar";
    };
    return ProgressBar;
}());
var DrawNotification = (function () {
    function DrawNotification(text, displayTime) {
        this._currentPosX = 26 * panelMinX; // Starting Position
        this._currentPosY = resY; // Starting Position Y
        this._targetX = 26 * panelMinX; // Ending Position
        this._targetY = 15 * panelMinY; // Ending Position Y
        this._width = panelMinX * 5;
        this._height = panelMinY * 3;
        // Text Settings
        this._text = text;
        this._r = 255;
        this._g = 165;
        this._b = 0;
        this._offset = 0;
        this._textScale = 0.5;
        // Animation Settings
        this._lastUpdateTime = new Date().getTime(); //ms
        this._alpha = 255;
        this._displayTime = displayTime;
        this._incrementer = 0;
        // Sound Settings
        this._sound = true;
    }
    DrawNotification.prototype.draw = function () {
        if (notification !== this) {
            return;
        }
        if (this._sound) {
            this._sound = false;
            API.playSoundFrontEnd("GOLF_NEW_RECORD", "HUD_AWARDS");
        }
        // Starts below max screen.
        API.drawRectangle(this._currentPosX, this._currentPosY - 5, this._width, 5, this._r, this._g, this._b, this._alpha - 30);
        API.drawRectangle(this._currentPosX, this._currentPosY, this._width, this._height, 0, 0, 0, this._alpha - 30);
        API.drawText(this._text, this._offset + this._currentPosX + (this._width / 2), this._currentPosY + (this._height / 4), this._textScale, 255, 255, 255, this._alpha, 4, 1, false, false, this._width - padding);
        this.animate();
    };
    DrawNotification.prototype.animate = function () {
        // Did we reach our goal?
        if (this._currentPosY <= this._targetY) {
            this._currentPosY = this._targetY;
            // Ready to fade?
            if (new Date().getTime() > this._lastUpdateTime + this._displayTime) {
                this.fade();
                return;
            }
            return;
        }
        this._lastUpdateTime = new Date().getTime();
        // If not let's reach our goal.
        if (this._currentPosY <= this._targetY + (this._height / 6)) {
            this._currentPosY -= 3;
            return;
        }
        else {
            this._currentPosY -= 5;
            return;
        }
    };
    DrawNotification.prototype.fade = function () {
        if (this._alpha <= 0) {
            this.cleanUpNotification();
            return;
        }
        this._alpha -= 5;
        return;
    };
    DrawNotification.prototype.cleanUpNotification = function () {
        notification = null;
    };
    DrawNotification.prototype.setText = function (value) {
        this._text = value;
    };
    DrawNotification.prototype.setColor = function (r, g, b) {
        this._r = r;
        this._g = g;
        this._b = b;
    };
    DrawNotification.prototype.setTextScale = function (value) {
        this._textScale = value;
    };
    DrawNotification.prototype.isHovered = function () {
        return;
    };
    DrawNotification.prototype.isClicked = function () {
        return;
    };
    DrawNotification.prototype.returnType = function () {
        return "DrawNotification";
    };
    return DrawNotification;
}());
var TextElement = (function () {
    // Constructor
    function TextElement(text, x, y, width, height, line) {
        this._xPos = x;
        this._yPos = y + (panelMinY * line);
        this._width = width;
        this._height = height;
        this._text = text;
        this._fontScale = 0.6;
        this._centered = false;
        this._centeredVertically = false;
        this._font = 4;
        this._fontR = 255;
        this._fontG = 255;
        this._fontB = 255;
        this._fontAlpha = 255;
        this._hoverTextAlpha = 255;
        this._hoverTextR = 255;
        this._hoverTextG = 255;
        this._hoverTextB = 255;
        this._offset = 0;
        this._padding = 10;
        this._hovered = false;
        this._shadow = false;
        this._outline = false;
    }
    TextElement.prototype.draw = function () {
        if (this._centered && this._centeredVertically) {
            this.drawAsCenteredAll();
            return;
        }
        if (this._centered) {
            this.drawAsCentered();
            return;
        }
        if (this._centeredVertically) {
            this.drawAsCenteredVertically();
            return;
        }
        this.drawAsNormal();
    };
    Object.defineProperty(TextElement.prototype, "Text", {
        //** Sets the text */
        set: function (value) {
            this._text = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Hovered", {
        get: function () {
            return this._hovered;
        },
        //** Is this text element in a hover state? */
        set: function (value) {
            this._hovered = value;
        },
        enumerable: true,
        configurable: true
    });
    //** Sets the color of the main text. A = Alpha */
    TextElement.prototype.Color = function (r, g, b, a) {
        this._fontR = r;
        this._fontG = g;
        this._fontB = b;
        this._fontAlpha = a;
    };
    TextElement.prototype.HoverColor = function (r, g, b, a) {
        this._hoverTextR = r;
        this._hoverTextG = g;
        this._hoverTextB = b;
        this._hoverTextAlpha = a;
    };
    Object.defineProperty(TextElement.prototype, "R", {
        /** Gets the color for RGB of R type. */
        get: function () {
            return this._fontR;
        },
        /** Sets the color for RGB of R type. Max of 255 */
        set: function (value) {
            this._fontR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "G", {
        /** Gets the color for RGB of G type. */
        get: function () {
            return this._fontG;
        },
        /** Sets the color for RGB of G type. Max of 255 */
        set: function (value) {
            this._fontG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "B", {
        /** Gets the color for RGB of B type. */
        get: function () {
            return this._fontB;
        },
        /** Sets the color for RGB of B type. Max of 255 */
        set: function (value) {
            this._fontB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Alpha", {
        get: function () {
            return this._fontAlpha;
        },
        /** Sets the font Alpha property */
        set: function (value) {
            this._fontAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverTextAlpha;
        },
        /** Sets the font Alpha property */
        set: function (value) {
            this._hoverTextAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverR", {
        get: function () {
            return this._hoverTextR;
        },
        /** Sets the hover color for the text RGB of R type. */
        set: function (value) {
            this._hoverTextR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverG", {
        get: function () {
            return this._hoverTextG;
        },
        /** Sets the hover color for the text RGB of G type. */
        set: function (value) {
            this._hoverTextG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverB", {
        get: function () {
            return this._hoverTextB;
        },
        /** Sets the hover color for the text RGB of B type. */
        set: function (value) {
            this._hoverTextB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Font", {
        get: function () {
            return this._font;
        },
        /** Set your font type. 0 - 7
        * 0 Normal
        * 1 Cursive
        * 2 All Caps
        * 3 Squares / Arrows / Etc.
        * 4 Condensed Normal
        * 5 Garbage
        * 6 Condensed Normal
        * 7 Bold GTA Style
        */
        set: function (value) {
            this._font = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "FontScale", {
        get: function () {
            return this._fontScale;
        },
        /** Sets the size of the text. 1 is quite large but is default. */
        set: function (value) {
            this._fontScale = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "VerticalCentered", {
        get: function () {
            return this._centeredVertically;
        },
        /** Centers the content vertically. Do not use if your box is not very high to begin with */
        set: function (value) {
            this._centeredVertically = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Centered", {
        get: function () {
            return this._centered;
        },
        /** Use this if you want centered content. */
        set: function (value) {
            this._centered = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Offset", {
        /**
         *  Set Offset
         */
        set: function (value) {
            this._offset = value;
        },
        enumerable: true,
        configurable: true
    });
    TextElement.prototype.drawAsCenteredAll = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 1, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 1, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsCenteredVertically = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos, this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 0, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos, this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 0, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsCentered = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._padding + this._yPos, this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 1, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._padding + this._yPos, this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 1, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsNormal = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + this._padding, this._yPos + this._padding, this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 0, this._shadow, this._outline, this._width - this._padding);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + this._padding, this._yPos + this._padding, this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 0, this._shadow, this._outline, this._width - this._padding);
    };
    return TextElement;
}());
var Panel = (function () {
    /**
     *
     * @param x - Max of 31. Starts on left side.
     * @param y - Max of 17. Starts at the top.
     * @param width - Max of 31. Each number fills a square.
     * @param height - Max of 17. Each number fills a square.
     */
    function Panel(page, x, y, width, height) {
        this._page = page;
        this._padding = 10;
        this._xPos = x * panelMinX;
        this._yPos = y * panelMinY;
        this._width = width * panelMinX;
        this._height = height * panelMinY;
        this._alpha = 225;
        this._border = false;
        this._header = false;
        this._headerR = 0;
        this._headerG = 0;
        this._headerB = 0;
        this._headerAlpha = 0;
        this._offset = 0;
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._textLines = [];
        this._inputPanels = [];
        this._progressBars = [];
        this._currentLine = 0;
        this._shadow = false;
        this._outline = false;
        this._tooltip = null;
        this._hovered = false;
        this._hoverTime = 0;
        this._hoverR = 0;
        this._hoverG = 0;
        this._hoverB = 0;
        this._hoverAlpha = 200;
        this._backgroundImage = null;
        this._backgroundImagePadding = 0;
        this._function = null;
        this._functionArgs = [];
        this._functionAudioLib = "Click";
        this._functionAudioName = "DLC_HEIST_HACKING_SNAKE_SOUNDS";
        this._hoverAudioLib = "Cycle_Item";
        this._hoverAudioName = "DLC_Dmod_Prop_Editor_Sounds";
        this._hoverAudio = true;
        this._functionClickAudio = true;
        this._hoverable = false;
        this._line = 0;
    }
    /**
     * Do not call this. It's specifically used for the menu builder file.
     */
    Panel.prototype.draw = function () {
        if (this._page !== currentPage) {
            return;
        }
        this.drawRectangles();
        if (!isReady) {
            return;
        }
        // Only used if using text lines.
        if (this._textLines.length > 0) {
            for (var i = 0; i < this._textLines.length; i++) {
                this._textLines[i].draw();
            }
        }
        // Only used if using input panels.
        if (this._inputPanels.length > 0) {
            for (var i = 0; i < this._inputPanels.length; i++) {
                this._inputPanels[i].draw();
            }
        }
        // Only used if using progress bars.
        if (this._progressBars.length > 0) {
            for (var i = 0; i < this._progressBars.length; i++) {
                this._progressBars[i].draw();
            }
        }
    };
    // Normal Versions
    Panel.prototype.drawRectangles = function () {
        if (this._backgroundImage !== null) {
            this.drawBackgroundImage();
            return;
        }
        if (this._hovered) {
            if (this._border) {
                API.drawRectangle(this._xPos - 4, this._yPos - 4, this._width + 8, this._height + 8, 0, 0, 0, 255);
            }
            API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._hoverR, this._hoverG, this._hoverB, this._hoverAlpha);
            if (this._header) {
                API.drawRectangle(this._xPos, this._yPos + this._height - 5, this._width, 5, this._headerR, this._headerG, this._headerB, this._headerAlpha);
            }
            return;
        }
        if (this._border) {
            API.drawRectangle(this._xPos - 4, this._yPos - 4, this._width + 8, this._height + 8, 0, 0, 0, 255);
        }
        API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._r, this._g, this._b, this._alpha);
        if (this._header) {
            API.drawRectangle(this._xPos, this._yPos + this._height - 5, this._width, 5, this._headerR, this._headerG, this._headerB, this._headerAlpha);
        }
    };
    Panel.prototype.drawBackgroundImage = function () {
        if (this._backgroundImagePadding > 1) {
            API.dxDrawTexture(this._backgroundImage, new Point(this._xPos + this._backgroundImagePadding, this._yPos + this._backgroundImagePadding), new Size(this._width - (this._backgroundImagePadding * 2), this._height - (this._backgroundImagePadding * 2)), 0);
            API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._r, this._g, this._b, this._alpha);
            return;
        }
        API.dxDrawTexture(this._backgroundImage, new Point(this._xPos, this._yPos), new Size(this._width, this._height), 0);
    };
    Object.defineProperty(Panel.prototype, "Function", {
        // Function Settings
        set: function (value) {
            this._function = value;
        },
        enumerable: true,
        configurable: true
    });
    /** Add an array or a single value as a function. IMPORTANT! Any function you write must be able to take an array of arguments. */
    Panel.prototype.addFunctionArgs = function (value) {
        this._functionArgs = value;
    };
    Object.defineProperty(Panel.prototype, "HoverAudioLib", {
        get: function () {
            return this._hoverAudioLib;
        },
        // HOVER AUDIO
        /** Sets the hover audio library. Ex: "Cycle_Item" */
        set: function (value) {
            this._hoverAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverAudioName", {
        get: function () {
            return this._hoverAudioName;
        },
        /** Sets the hover audio name. Ex: "DLC_Dmod_Prop_Editor_Sounds" */
        set: function (value) {
            this._hoverAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudioLib", {
        get: function () {
            return this._functionAudioLib;
        },
        // FUNCTION AUDIO
        /** Sets the function audio library. Ex: "Cycle_Item" */
        set: function (value) {
            this._functionAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudioName", {
        get: function () {
            return this._functionAudioName;
        },
        /** Sets the function audio name. Ex: "DLC_Dmod_Prop_Editor_Sounds" */
        set: function (value) {
            this._functionAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudio", {
        get: function () {
            return this._functionClickAudio;
        },
        /** Sets if the function audio plays. */
        set: function (value) {
            this._functionClickAudio = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainAlpha", {
        get: function () {
            return this._alpha;
        },
        // Background Alpha
        /** Sets the background alpha property */
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainBackgroundImagePadding", {
        get: function () {
            return this._backgroundImagePadding;
        },
        /** Sets the background image padding property */
        set: function (value) {
            this._backgroundImagePadding = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainBackgroundImage", {
        get: function () {
            return this._backgroundImage;
        },
        /** Uses a custom image for your panel background. Must include extension. EX. 'clientside/image.jpg' */
        set: function (value) {
            this._backgroundImage = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorR", {
        /** Gets the color for RGB of R type. */
        get: function () {
            return this._r;
        },
        /** Sets the color for RGB of R type. Max of 255 */
        set: function (value) {
            this._r = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorG", {
        /** Gets the color for RGB of G type. */
        get: function () {
            return this._g;
        },
        /** Sets the color for RGB of G type. Max of 255 */
        set: function (value) {
            this._g = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorB", {
        /** Gets the color for RGB of B type. */
        get: function () {
            return this._b;
        },
        /** Sets the color for RGB of B type. Max of 255 */
        set: function (value) {
            this._b = value;
        },
        enumerable: true,
        configurable: true
    });
    /** Sets RGB Color of Main */
    Panel.prototype.MainBackgroundColor = function (r, g, b, alpha) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._alpha = alpha;
    };
    /** Sets the RGB Color of Hover */
    Panel.prototype.HoverBackgroundColor = function (r, g, b, alpha) {
        this._hoverR = r;
        this._hoverG = g;
        this._hoverB = b;
        this._hoverAlpha = alpha;
    };
    /** sets RGB Color of Header bar */
    Panel.prototype.HeaderColor = function (r, g, b, alpha) {
        this._headerR = r;
        this._headerG = g;
        this._headerB = b;
        this._headerAlpha = alpha;
    };
    Object.defineProperty(Panel.prototype, "Hoverable", {
        get: function () {
            return this._hoverable;
        },
        /** Is there a hover state? */
        set: function (value) {
            this._hoverable = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverAlpha;
        },
        /** Sets the hover alpha */
        set: function (value) {
            this._hoverAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverR", {
        get: function () {
            return this._hoverR;
        },
        /** Sets the hover color for RGB of R type. */
        set: function (value) {
            this._hoverR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverG", {
        get: function () {
            return this._hoverG;
        },
        /** Sets the hover color for RGB of G type. */
        set: function (value) {
            this._hoverG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverB", {
        get: function () {
            return this._hoverB;
        },
        /** Sets the hover color for RGB of B type. */
        set: function (value) {
            this._hoverB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FontOutline", {
        get: function () {
            return this._outline;
        },
        /** Sets the font Outline property */
        set: function (value) {
            this._outline = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FontShadow", {
        get: function () {
            return this._shadow;
        },
        /** Sets the font Shadow property */
        set: function (value) {
            this._shadow = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Tooltip", {
        get: function () {
            return this._tooltip;
        },
        /** Sets the Tooltip text for your element. */
        set: function (value) {
            this._tooltip = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Border", {
        get: function () {
            return this._border;
        },
        /** Adds a stylized line under your your box. */
        set: function (value) {
            this._border = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Header", {
        get: function () {
            return this._header;
        },
        /** Adds a stylized line under your your box. */
        set: function (value) {
            this._header = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Offset", {
        get: function () {
            return this._offset;
        },
        /** If your text needs to be pushed in a certain direction either add or remove pixels here. */
        set: function (value) {
            this._offset = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     *  Used to add text elements to your panels.
     * @param value
     */
    Panel.prototype.addText = function (value) {
        var textElement = new TextElement(value, this._xPos, this._yPos, this._width, this._height, this._line);
        this._textLines.push(textElement);
        this._line += 1;
        return textElement;
    };
    /**
     *
     * @param x - Start position of X inside the panel.
     * @param y - Start Position of Y inside the panel.
     * @param width - How wide. Generally the width of your panel.
     * @param height - How tall. 1 or 2 is pretty normal.
     */
    Panel.prototype.addInput = function (x, y, width, height) {
        var inputPanel = new InputPanel(this._page, (x * panelMinX) + this._xPos, (y * panelMinY) + this._yPos, width, height);
        this._inputPanels.push(inputPanel);
        return inputPanel;
    };
    /**
     *
     * @param x - Start position of X inside the panel.
     * @param y - Start Position of Y inside the panel.
     * @param width
     * @param height
     * @param currentProgress - 0 to 100
     */
    Panel.prototype.addProgressBar = function (x, y, width, height, currentProgress) {
        var progressBar = new ProgressBar((x * panelMinX) + this._xPos, (y * panelMinY) + this._yPos, width, height, currentProgress);
        this._progressBars.push(progressBar);
        return progressBar;
    };
    // Hover Action
    Panel.prototype.isHovered = function () {
        if (!API.isCursorShown()) {
            return;
        }
        if (!this._hoverable) {
            return;
        }
        var cursorPos = API.getCursorPositionMaintainRatio();
        if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < this._yPos + this._height) {
            if (!this._hovered) {
                this._hovered = true;
                this.setTextHoverState(true);
                if (this._hoverAudio) {
                    API.playSoundFrontEnd(this._hoverAudioLib, this._hoverAudioName);
                }
            }
            this._hoverTime += 1;
            if (this._hoverTime > 50) {
                if (this._tooltip === null) {
                    return;
                }
                if (this._tooltip.length > 1) {
                    API.drawText(this._tooltip, cursorPos.X + 25, cursorPos.Y, 0.4, 255, 255, 255, 255, 4, 0, true, true, 200);
                }
            }
            return;
        }
        this._hovered = false;
        this._hoverTime = 0;
        this.setTextHoverState(false);
    };
    // Click Action
    Panel.prototype.isClicked = function () {
        // Is there even a cursor?
        if (!API.isCursorShown()) {
            return;
        }
        // Is there a function if they click it?
        if (this._function === null) {
            return;
        }
        // Are they even left clicking?
        if (API.isControlJustPressed(237)) {
            var cursorPos = API.getCursorPositionMaintainRatio();
            if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < this._yPos + this._height) {
                if (new Date().getTime() < clickDelay + 200) {
                    return;
                }
                clickDelay = new Date().getTime();
                if (this._functionClickAudio) {
                    API.playSoundFrontEnd(this._functionAudioLib, this._functionAudioName);
                }
                if (this._functionArgs !== null || this._functionArgs.length > 1) {
                    this._function(this._functionArgs);
                }
                else {
                    this._function();
                }
                return;
            }
        }
    };
    Panel.prototype.setTextHoverState = function (value) {
        for (var i = 0; i < this._textLines.length; i++) {
            this._textLines[i].Hovered = value;
        }
    };
    // Type
    Panel.prototype.returnType = function () {
        return "Panel";
    };
    return Panel;
}());
var InputPanel = (function () {
    function InputPanel(page, x, y, width, height) {
        this._xPos = x;
        this._yPos = y;
        this._width = width * panelMinX;
        this._height = height * panelMinY;
        this._protected = false;
        this._input = "";
        this._hovered = false;
        this._selected = false;
        this._maxLength = 397;
        this._isLongText = false;
        this._font = 4;
        this._numeric = false;
        this._isError = false;
        this._isTransparent = false;
        this._r = 255;
        this._g = 255;
        this._b = 255;
        this._alpha = 100;
        this._hoverR = 255;
        this._hoverG = 255;
        this._hoverB = 255;
        this._hoverAlpha = 125;
        this._selectR = 255;
        this._selectG = 255;
        this._selectB = 255;
        this._inputTextR = 0;
        this._inputTextG = 0;
        this._inputTextB = 0;
        this._inputTextAlpha = 255;
        this._selectAlpha = 125;
        this._inputAudioLib = "Click";
        this._inputAudioName = "DLC_HEIST_HACKING_SNAKE_SOUNDS";
        this._page = page;
        this._inputTextScale = 0.45;
        tabIndex.push(this);
    }
    Object.defineProperty(InputPanel.prototype, "isError", {
        /** Sets whether or not there is an error. */
        set: function (value) {
            this._isError = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Selected", {
        get: function () {
            return this._selected;
        },
        /** Sets whether or not this input is selected. */
        set: function (value) {
            this._selected = value;
            if (value) {
                selectedInput = this;
            }
            else {
                selectedInput = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "AudioLib", {
        get: function () {
            return this._inputAudioLib;
        },
        /** set audio sound for click effect */
        /* Set audio Library */
        set: function (value) {
            this._inputAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "AudioName", {
        get: function () {
            return this._inputAudioName;
        },
        /* Set audio Name */
        set: function (value) {
            this._inputAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Font", {
        get: function () {
            return this._font;
        },
        /** Set your font type. 0 - 7
        * 0 Normal
        * 1 Cursive
        * 2 All Caps
        * 3 Squares / Arrows / Etc.
        * 4 Condensed Normal
        * 5 Garbage
        * 6 Condensed Normal
        * 7 Bold GTA Style
        */
        set: function (value) {
            this._font = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverR", {
        get: function () {
            return this._hoverR;
        },
        // Hover BACKGROUND PARAMETERS
        /** Set R of RGB on hover background. */
        set: function (value) {
            this._hoverR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverG", {
        get: function () {
            return this._hoverG;
        },
        /** Set G of RGB on hover background. */
        set: function (value) {
            this._hoverG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverB", {
        get: function () {
            return this._hoverB;
        },
        /** Set B of RGB on hover background. */
        set: function (value) {
            this._hoverB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverAlpha;
        },
        /** Set Alpha of RGB on hover background. */
        set: function (value) {
            this._hoverAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    // Main BACKGROUND PARAMETERS
    InputPanel.prototype.MainBackgroundColor = function (r, g, b, alpha) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._alpha = alpha;
    };
    InputPanel.prototype.HoverBackgroundColor = function (r, g, b, alpha) {
        this._hoverR = r;
        this._hoverG = g;
        this._hoverB = b;
        this._hoverAlpha = alpha;
    };
    InputPanel.prototype.SelectBackgroundColor = function (r, g, b, alpha) {
        this._selectR = r;
        this._selectG = g;
        this._selectB = b;
        this._selectAlpha = alpha;
    };
    Object.defineProperty(InputPanel.prototype, "R", {
        get: function () {
            return this._r;
        },
        /** Set R of RGB on main background. */
        set: function (value) {
            this._r = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "G", {
        get: function () {
            return this._g;
        },
        /** Set G of RGB on main background. */
        set: function (value) {
            this._g = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "B", {
        get: function () {
            return this._b;
        },
        /** Set B of RGB on main background. */
        set: function (value) {
            this._b = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Alpha", {
        get: function () {
            return this._alpha;
        },
        /** Set Alpha of RGB on main background. */
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectR", {
        get: function () {
            return this._selectR;
        },
        // SELECTION BACKGROUND PARAMETERS
        /** Set R of RGB on main background. */
        set: function (value) {
            this._selectR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectG", {
        get: function () {
            return this._selectG;
        },
        /** Set G of RGB on main background. */
        set: function (value) {
            this._selectG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectB", {
        get: function () {
            return this._selectB;
        },
        /** Set B of RGB on main background. */
        set: function (value) {
            this._selectB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectAlpha", {
        get: function () {
            return this._selectAlpha;
        },
        /** Set Alpha of RGB on main background. */
        set: function (value) {
            this._selectAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     *  Sets the RGB Parameter for the INPUT Text;
     * @param r
     * @param g
     * @param b
     * @param alpha
     */
    InputPanel.prototype.InputTextColor = function (r, g, b, alpha) {
        this._inputTextR = r;
        this._inputTextG = g;
        this._inputTextB = b;
        this._inputTextAlpha = alpha;
    };
    Object.defineProperty(InputPanel.prototype, "TextR", {
        get: function () {
            return this._inputTextR;
        },
        /** Set R of RGB on input text. */
        set: function (value) {
            this._inputTextR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "TextG", {
        get: function () {
            return this._inputTextG;
        },
        /** Set G of RGB on input text. */
        set: function (value) {
            this._inputTextG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "TextB", {
        /** Set B of RGB on input text. */
        set: function (value) {
            this._inputTextB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputB", {
        get: function () {
            return this._inputTextB;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputAlpha", {
        get: function () {
            return this._inputTextAlpha;
        },
        /** Set Alpha of RGB on input text. */
        set: function (value) {
            this._inputTextAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputTextScale", {
        get: function () {
            return this._inputTextScale;
        },
        /** Input Text Size */
        set: function (value) {
            this._inputTextScale = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Input", {
        /** Returns whatever the current input is. */
        get: function () {
            return this._input;
        },
        /** Sets the input text. */
        set: function (value) {
            this._input = value;
        },
        enumerable: true,
        configurable: true
    });
    /** Removes the last character from the input. */
    InputPanel.prototype.removeFromInput = function () {
        this._input = this._input.substring(0, this._input.length - 1);
    };
    Object.defineProperty(InputPanel.prototype, "NumericOnly", {
        get: function () {
            return this._numeric;
        },
        /** Set whether the input should be numeric only. */
        set: function (value) {
            this._numeric = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "MaxLength", {
        get: function () {
            return this._maxLength;
        },
        /** Sets the max length for your text. */
        set: function (value) {
            this._maxLength = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "LongText", {
        get: function () {
            return this._isLongText;
        },
        /** Sets the textInput if long or not. */
        set: function (value) {
            this._isLongText = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Protected", {
        /**
         *  Sets whether the input should be protected or not. */
        set: function (value) {
            this._protected = value;
        },
        enumerable: true,
        configurable: true
    });
    // Draw what we need to draw.
    InputPanel.prototype.draw = function () {
        if (this._selected) {
            this.selectedDraw();
        }
        if (this._hovered) {
            this.hoveredDraw();
        }
        if (!this._hovered && !this._selected) {
            this.normalDraw();
        }
        this.isHovered();
        this.isClicked();
    };
    InputPanel.prototype.normalDraw = function () {
        if (this._isError) {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, 255, 0, 0, 100);
        }
        else {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._r, this._g, this._b, this._alpha);
        }
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText === true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
    };
    InputPanel.prototype.selectedDraw = function () {
        API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._selectR, this._selectG, this._selectB, this._selectAlpha);
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText === true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
        return;
    };
    InputPanel.prototype.hoveredDraw = function () {
        if (this._isError) {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, 255, 0, 0, 100);
        }
        else {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._hoverR, this._hoverG, this._hoverB, this._hoverAlpha);
        }
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText == true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
    };
    InputPanel.prototype.isHovered = function () {
        if (!API.isCursorShown()) {
            return;
        }
        var cursorPos = API.getCursorPositionMaintainRatio();
        if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < (this._yPos + this._height)) {
            if (this._selected) {
                this._hovered = false;
                return;
            }
            this._hovered = true;
        }
        else {
            this._hovered = false;
        }
    };
    InputPanel.prototype.isClicked = function () {
        if (API.isControlJustPressed(237)) {
            if (new Date().getTime() < clickDelay + 200) {
                return;
            }
            var cursorPos = API.getCursorPositionMaintainRatio();
            if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < (this._yPos + this._height)) {
                if (!this._selected) {
                    API.playSoundFrontEnd(this._inputAudioLib, this._inputAudioName);
                    this._selected = true;
                }
                selectedInput = this;
            }
            else {
                this._selected = false;
            }
        }
    };
    InputPanel.prototype.addToInput = function (text) {
        if (!this._numeric) {
            this._input += text;
            return this._input;
        }
        else {
            if (Number.isInteger(+text)) {
                this._input += text;
                return this._input;
            }
        }
    };
    InputPanel.prototype.returnType = function () {
        return "InputPanel";
    };
    return InputPanel;
}());
function drawTextNotification() {
    if (textnotification !== null) {
        textnotification.draw();
        return;
    }
    if (textnotifications.length <= 0) {
        return;
    }
    textnotification = textnotifications.shift();
    return;
}
function drawNotification() {
    if (notification !== null) {
        notification.draw();
        return;
    }
    if (notifications.length <= 0) {
        return;
    }
    notification = notifications.shift();
    return;
}
function createNotification(page, text, displayTime) {
    // Add to queue.
    var notify = new DrawNotification(text, displayTime);
    notifications.push(notify);
    return notify;
}
/**
 * Set the page number for whatever current menu is open.
 * @param value
 */
function setPage(value) {
    currentPage = value;
}
function killMenu() {
    isReady = false;
    currentPage = -1;
    selectedInput = null;
    tabIndex = [];
    API.showCursor(false);
    API.setHudVisible(true);
    API.setChatVisible(true);
    API.setCanOpenChat(true);
    API.callNative("_TRANSITION_FROM_BLURRED", 3000);
    menuElements = [];
}
// On-Keydown Event
