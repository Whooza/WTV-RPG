/// <reference path="../../types-gt-mp/index.d.ts" />
/// <reference path="_Vars.ts" />
/// <reference path="DrawMenu.ts" />

API.onKeyDown.connect((sender, e) => {
    if (!isReady) {
        return;
    }

    // Shift between Input Boxes.
    if (e.KeyCode === Keys.Tab) {
        if (tabIndex[0].Selected) {
            tabIndex[0].Selected = false;
        } else {
            tabIndex[0].Selected = true;
            return;
        }
        let removeItem = tabIndex.shift();
        tabIndex.push(removeItem);
        tabIndex[0].Selected = true;
    }

    if (selectedInput === null) {
        return;
    }

    if (e.KeyCode === Keys.Back) {
        selectedInput.removeFromInput();
        return;
    }

    let keypress = "";

    let shiftOn = false;
    let altOn = false;

    if (e.Shift) {
        shiftOn = true;
    }

    if (e.Alt) {
        altOn = true;
    }

    if (language === 0) {
        switch (e.KeyCode) {
        case Keys.Enter:
            keypress = "\n";
            break;
        case Keys.Space:
            keypress = " ";
            break;
        case Keys.A:
            keypress = "a";
            if (shiftOn) {
                keypress = "A";
            }
            break;
        case Keys.B:
            keypress = "b";
            if (shiftOn) {
                keypress = "B";
            }
            break;
        case Keys.C:
            keypress = "c";
            if (shiftOn) {
                keypress = "C";
            }
            break;
        case Keys.D:
            keypress = "d";
            if (shiftOn) {
                keypress = "D";
            }
            break;
        case Keys.E:
            keypress = "e";
            if (shiftOn) {
                keypress = "E";
            }
            break;
        case Keys.F:
            keypress = "f";
            if (shiftOn) {
                keypress = "F";
            }
            break;
        case Keys.G:
            keypress = "g";
            if (shiftOn) {
                keypress = "G";
            }
            break;
        case Keys.H:
            keypress = "h";
            if (shiftOn) {
                keypress = "H";
            }
            break;
        case Keys.I:
            keypress = "i";
            if (shiftOn) {
                keypress = "I";
            }
            break;
        case Keys.J:
            keypress = "j";
            if (shiftOn) {
                keypress = "J";
            }
            break;
        case Keys.K:
            keypress = "k";
            if (shiftOn) {
                keypress = "K";
            }
            break;
        case Keys.L:
            keypress = "l";
            if (shiftOn) {
                keypress = "L";
            }
            break;
        case Keys.M:
            keypress = "m";
            if (shiftOn) {
                keypress = "M";
            }
            break;
        case Keys.N:
            keypress = "n";
            if (shiftOn) {
                keypress = "N";
            }
            break;
        case Keys.O:
            keypress = "o";
            if (shiftOn) {
                keypress = "O";
            }
            break;
        case Keys.P:
            keypress = "p";
            if (shiftOn) {
                keypress = "P";
            }
            break;
        case Keys.Q:
            keypress = "q";
            if (shiftOn) {
                keypress = "Q";
            }
            break;
        case Keys.R:
            keypress = "r";
            if (shiftOn) {
                keypress = "R";
            }
            break;
        case Keys.S:
            keypress = "s";
            if (shiftOn) {
                keypress = "S";
            }
            break;
        case Keys.T:
            keypress = "t";
            if (shiftOn) {
                keypress = "T";
            }
            break;
        case Keys.U:
            keypress = "u";
            if (shiftOn) {
                keypress = "U";
            }
            break;
        case Keys.V:
            keypress = "v";
            if (shiftOn) {
                keypress = "V";
            }
            break;
        case Keys.W:
            keypress = "w";
            if (shiftOn) {
                keypress = "W";
            }
            break;
        case Keys.X:
            keypress = "x";
            if (shiftOn) {
                keypress = "X";
            }
            break;
        case Keys.Y:
            keypress = "y";
            if (shiftOn) {
                keypress = "Y";
            }
            break;
        case Keys.Z:
            keypress = "z";
            if (shiftOn) {
                keypress = "Z";
            }
            break;
        case Keys.D0:
            keypress = "0";
            if (shiftOn) {
                keypress = ")";
            }
            break;
        case Keys.D1:
            keypress = "1";
            if (shiftOn) {
                keypress = "!";
            }
            break;
        case Keys.D2:
            keypress = "2";
            if (shiftOn) {
                keypress = "@";
            }
            break;
        case Keys.D3:
            keypress = "3";
            if (shiftOn) {
                keypress = "#";
            }
            break;
        case Keys.D4:
            keypress = "4";
            if (shiftOn) {
                keypress = "$";
            }
            break;
        case Keys.D5:
            keypress = "5";
            if (shiftOn) {
                keypress = "%";
            }
            break;
        case Keys.D6:
            keypress = "6";
            if (shiftOn) {
                keypress = "^";
            }
            break;
        case Keys.D7:
            keypress = "7";
            if (shiftOn) {
                keypress = "&";
            }
            break;
        case Keys.D8:
            keypress = "8";
            if (shiftOn) {
                keypress = "*";
            }
            break;
        case Keys.D9:
            keypress = "9";
            if (shiftOn) {
                keypress = "(";
            }
            break;
        case Keys.OemMinus:
            keypress = "-";
            if (shiftOn) {
                keypress = "_";
            }
            break;
        case Keys.Oemplus:
            keypress = "=";
            if (shiftOn) {
                keypress = "+";
            }
            break;
        case Keys.OemQuestion:
            keypress = "/";
            if (shiftOn) {
                keypress = "?";
            }
            break;
        case Keys.Oemcomma:
            keypress = ",";
            if (shiftOn) {
                keypress = "<";
            }
            break;
        case Keys.OemPeriod:
            keypress = ".";
            if (shiftOn) {
                keypress = ">";
            }
            break;
        case Keys.OemSemicolon:
            keypress = ";";
            if (shiftOn) {
                keypress = ":";
            }
            break;
        case Keys.OemOpenBrackets:
            keypress = "[";
            if (shiftOn) {
                keypress = "{";
            }
            break;
        case Keys.OemCloseBrackets:
            keypress = "]";
            if (shiftOn) {
                keypress = "}";
            }
            break;
        case Keys.NumPad0:
            keypress = "0";
            break;
        case Keys.NumPad1:
            keypress = "1";
            break;
        case Keys.NumPad2:
            keypress = "2";
            break;
        case Keys.NumPad3:
            keypress = "3";
            break;
        case Keys.NumPad4:
            keypress = "4";
            break;
        case Keys.NumPad5:
            keypress = "5";
            break;
        case Keys.NumPad6:
            keypress = "6";
            break;
        case Keys.NumPad7:
            keypress = "7";
            break;
        case Keys.NumPad8:
            keypress = "8";
            break;
        case Keys.NumPad9:
            keypress = "9";
            break;
        }
    } else if (language === 1) {
        switch (e.KeyCode) {
        case Keys.Enter:
            keypress = "\n";
            break;
        case Keys.Space:
            keypress = " ";
            break;
        case Keys.A:
            keypress = "ф";
            if (shiftOn) {
                keypress = "Ф";
            }
            break;
        case Keys.B:
            keypress = "и";
            if (shiftOn) {
                keypress = "И";
            }
            break;
        case Keys.C:
            keypress = "с";
            if (shiftOn) {
                keypress = "С";
            }
            break;
        case Keys.D:
            keypress = "в";
            if (shiftOn) {
                keypress = "В";
            }
            break;
        case Keys.E:
            keypress = "у";
            if (shiftOn) {
                keypress = "У";
            }
            break;
        case Keys.F:
            keypress = "а";
            if (shiftOn) {
                keypress = "А";
            }
            break;
        case Keys.G:
            keypress = "п";
            if (shiftOn) {
                keypress = "П";
            }
            break;
        case Keys.H:
            keypress = "р";
            if (shiftOn) {
                keypress = "Р";
            }
            break;
        case Keys.I:
            keypress = "ш";
            if (shiftOn) {
                keypress = "Ш";
            }
            break;
        case Keys.J:
            keypress = "о";
            if (shiftOn) {
                keypress = "О";
            }
            break;
        case Keys.K:
            keypress = "л";
            if (shiftOn) {
                keypress = "Л";
            }
            break;
        case Keys.L:
            keypress = "д";
            if (shiftOn) {
                keypress = "Д";
            }
            break;
        case Keys.M:
            keypress = "ь";
            if (shiftOn) {
                keypress = "Ь";
            }
            break;
        case Keys.N:
            keypress = "т";
            if (shiftOn) {
                keypress = "Т";
            }
            break;
        case Keys.O:
            keypress = "щ";
            if (shiftOn) {
                keypress = "Щ";
            }
            break;
        case Keys.P:
            keypress = "з";
            if (shiftOn) {
                keypress = "З";
            }
            break;
        case Keys.Q:
            keypress = "й";
            if (shiftOn) {
                keypress = "Й";
            }
            break;
        case Keys.R:
            keypress = "к";
            if (shiftOn) {
                keypress = "К";
            }
            break;
        case Keys.S:
            keypress = "ы";
            if (shiftOn) {
                keypress = "Ы";
            }
            break;
        case Keys.T:
            keypress = "е";
            if (shiftOn) {
                keypress = "Е";
            }
            break;
        case Keys.U:
            keypress = "г";
            if (shiftOn) {
                keypress = "Г";
            }
            break;
        case Keys.V:
            keypress = "м";
            if (shiftOn) {
                keypress = "М";
            }
            break;
        case Keys.W:
            keypress = "ц";
            if (shiftOn) {
                keypress = "Ц";
            }
            break;
        case Keys.X:
            keypress = "ч";
            if (shiftOn) {
                keypress = "Ч";
            }
            break;
        case Keys.Y:
            keypress = "н";
            if (shiftOn) {
                keypress = "Н";
            }
            break;
        case Keys.Z:
            keypress = "я";
            if (shiftOn) {
                keypress = "Я";
            }
            break;
        case Keys.D0:
            keypress = "0";
            if (shiftOn) {
                keypress = ")";
            }
            break;
        case Keys.D1:
            keypress = "1";
            if (shiftOn) {
                keypress = "!";
            }
            break;
        case Keys.D2:
            keypress = "2";
            if (altOn) {
                keypress = "~";
            }
            break;
        case Keys.D3:
            keypress = '3';
            if (shiftOn) {
                keypress = "№";
            }
            else if (altOn) {
                keypress = "#";
            }
            break;
        case Keys.D4:
            keypress = "4";
            if (shiftOn) {
                keypress = ";";
            }
            break;
        case Keys.D5:
            keypress = "5";
            if (shiftOn) {
                keypress = "%";
            }
            break;
        case Keys.D6:
            keypress = "6";
            if (shiftOn) {
                keypress = ":";
            }
            else if (altOn) {
                keypress = "^";
            }
            break;
        case Keys.D7:
            keypress = "7";
            if (shiftOn) {
                keypress = "?";
            }
            else if (altOn) {
                keypress = "&";
            }
            break;
        case Keys.D8:
            keypress = "8";
            if (shiftOn) {
                keypress = "*";
            }
            break;
        case Keys.D9:
            keypress = "9";
            if (shiftOn) {
                keypress = "(";
            }
            break;
        case Keys.OemMinus:
            keypress = "-";
            if (shiftOn) {
                keypress = "_";
            }
            break;
        case Keys.Oemplus:
            keypress = "=";
            if (shiftOn) {
                keypress = "+";
            }
            break;
        case Keys.OemQuestion:
            keypress = ".";
            if (shiftOn) {
                keypress = ",";
            }
            break;
        case Keys.Oemcomma:
            keypress = "б";
            if (shiftOn) {
                keypress = "Б";
            }
            break;
        case Keys.OemPeriod:
            keypress = "ю";
            if (shiftOn) {
                keypress = "Ю";
            }
            break;
        case Keys.OemSemicolon:
            keypress = "ж";
            if (shiftOn) {
                keypress = "Ж";
            }
            break;
        case Keys.OemOpenBrackets:
            keypress = "х";
            if (shiftOn) {
                keypress = "Х";
            }
            break;
        case Keys.OemCloseBrackets:
            keypress = "ъ";
            if (shiftOn) {
                keypress = "Ъ";
            }
            if (shiftOn) {
                keypress = "Ъ";
            }
            break;
        case Keys.Oem3:
            keypress = "ё";
            if (shiftOn) {
                keypress = "Ё";
            }
            break;
        case Keys.Oem5:
            keypress = "*";
            if (shiftOn) {
                keypress = "µ";
            }
            break;
        case Keys.Oem7:
            keypress = "э";
            if (shiftOn) {
                keypress = "Э";
            }
            break;
        case Keys.Oem8:
            keypress = "!";
            if (shiftOn) {
                keypress = "§";
            }
            break;
        case Keys.Oem102:
            keypress = "<";
            if (shiftOn) {
                keypress = ">";
            }
            break;
        case Keys.NumPad0:
            keypress = "0";
            break;
        case Keys.NumPad1:
            keypress = "1";
            break;
        case Keys.NumPad2:
            keypress = "2";
            break;
        case Keys.NumPad3:
            keypress = "3";
            break;
        case Keys.NumPad4:
            keypress = "4";
            break;
        case Keys.NumPad5:
            keypress = "5";
            break;
        case Keys.NumPad6:
            keypress = "6";
            break;
        case Keys.NumPad7:
            keypress = "7";
            break;
        case Keys.NumPad8:
            keypress = "8";
            break;
        case Keys.NumPad9:
            keypress = "9";
            break;
        }
    }

    if (keypress === "") {
        return;
    }

    if (keypress.length > 0) {
        if (selectedInput === null) {
            return;
        }
        selectedInput.addToInput(keypress);
    } else {
        return;
    }
});