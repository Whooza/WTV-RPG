//**************************************
//************[Lib Var`s]***************
//**************************************
var resolution = API.getScreenResolutionMaintainRatio();
//************[for MenuDraw]************ START
var resX = resolution.Width;
var resY = resolution.Height;
var panelMinX = Math.round(resX / 32);
var panelMinY = Math.round(resY / 18);
var button = null;
var panel = null;
var image = null;
var notification = null;
var notifications = [];
var textnotification = null;
var textnotifications = [];
var padding = 10;
var selectedInput = null;
// Menu Properties
var tabIndex = [];
var tab = 0;
var menuElements = [];
var isReady = false;
var currentPage = 0;
var clickDelay = new Date().getTime();
var language = 0;
// Current Menu
var drawMenuCurrentMenu = null;
//************[for MenuDraw]************ END 
