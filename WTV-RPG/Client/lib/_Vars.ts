//**************************************
//************[Lib Var`s]***************
//**************************************
let resolution = API.getScreenResolutionMaintainRatio();

//************[for MenuDraw]************ START
let resX = resolution.Width;
let resY = resolution.Height;
let panelMinX = Math.round(resX / 32);
let panelMinY = Math.round(resY / 18);
let button = null;
let panel = null;
let image = null;
let notification = null;
let notifications = [];
let textnotification = null;
let textnotifications = [];
let padding = 10;
let selectedInput: InputPanel = null;

// Menu Properties
let tabIndex = [];
let tab: number = 0;
let menuElements = [];
let isReady = false;
let currentPage = 0;
let clickDelay = new Date().getTime();
let language = 0;

// Current Menu
let drawMenuCurrentMenu: MenuDraw = null;
//************[for MenuDraw]************ END