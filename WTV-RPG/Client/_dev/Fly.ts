/*
	By Xenius | 2016.12.27.
 */
let flyEnabled = false;
let currentPos: Vector3;
let forward: boolean;
let backward: boolean;
let left: boolean;
let right: boolean;
let down: boolean;
let up: boolean;
let shift: boolean;
let maxSpeed: boolean;

API.onUpdate.connect(() => {  
	if (!flyEnabled)
		return;
	let multiplier = 1;
	if (shift)
		multiplier = 3;
		
	if (maxSpeed)
		multiplier = 10;
		
	const camRotation = API.getGameplayCamRot();
	const camDirection = API.getGameplayCamDir();
		
	if (forward){
		currentPos.X = currentPos.X + camDirection.X*multiplier;
		currentPos.Y = currentPos.Y + camDirection.Y*multiplier;
	}
		
	if (backward){
		currentPos.X = currentPos.X - camDirection.X*multiplier;
		currentPos.Y = currentPos.Y - camDirection.Y*multiplier;
	}
		
	//BUG
	if (left){
		currentPos.X = currentPos.X + camDirection.X;
		currentPos.Y = currentPos.Y - camDirection.Y;
	}
		
	//BUG
	if (right){
		currentPos.X = currentPos.X - camDirection.X;
		currentPos.Y = currentPos.Y + camDirection.Y;
	}
		
	if (down){
		currentPos.Z = currentPos.Z - multiplier;
	}
		
	if (up){
		currentPos.Z = currentPos.Z + multiplier;
	}
		
	const newRotation = new Vector3(0, 0, camRotation.Z-180);
	API.setEntityRotation(API.getLocalPlayer(), newRotation);
	API.setEntityPosition(API.getLocalPlayer(), currentPos);
});

API.onChatCommand.connect(msg => {
	if (msg === "/fly") {
		if (flyEnabled) {
			flyEnabled = false;
			API.sendChatMessage("[Fly] ~r~Disabled");
		} else {
			currentPos = API.getEntityPosition(API.getLocalPlayer());
			flyEnabled = true;
			API.sendChatMessage("[Fly] ~g~Enabled");
		}
	}
});

API.onKeyUp.connect((sender, key) => {
	if (flyEnabled) {
		if (key.KeyCode === Keys.W) {
			forward = false;
		}else if (key.KeyCode === Keys.D) {
			right = false;
		}else if (key.KeyCode === Keys.A) {
			left = false;
		}else if (key.KeyCode === Keys.S) {
			backward = false;
		}else if (key.KeyCode === Keys.ControlKey) {
			down = false;
		}else if (key.KeyCode === Keys.ShiftKey) {
			shift = false;
		}else if (key.KeyCode === Keys.Space) {
			up = false;
		}else if (key.KeyCode === Keys.E) {
			maxSpeed = false;
		}
	}
});

API.onKeyDown.connect((sender, key) => {
	if (flyEnabled) {
		if (key.KeyCode === Keys.W) {
			forward = true;
		}else if (key.KeyCode === Keys.D) {
			right = true;
		}else if (key.KeyCode === Keys.A) {
			left = true;
		}else if (key.KeyCode === Keys.S) {
			backward = true;
		}else if (key.KeyCode === Keys.ControlKey) {
			down = true;
		}else if (key.KeyCode === Keys.ShiftKey) {
			shift = true;
		}else if (key.KeyCode === Keys.Space) {
			up = true;
		}else if (key.KeyCode === Keys.E) {
			maxSpeed = true;
		}
	}
});