/// <reference path="../types-gt-mp/Definitions/index.d.ts" />

var cameras = [];
var interpolating = false;



API.onServerEventTrigger.connect(function (name, args) {
    if (name === "createCameraActive") {
        createCameraActive(args);
    }
    else if (name === "createCameraAtGamecam") {
        createCameraAtGamecam();
    }
    else if (name === "createCameraInactive") {
        createCameraInactive(args);
    }
    else if (name === "setCameraFov") {
        API.setCameraFov(cameras[args[0]], args[1]);
    }
    else if (name === "pointCameraAtLocalPlayer") {
        pointCameraAtLocalPlayer(args);
    }
    else if (name === "pointCameraAtPosition") {
        pointCameraAtPosition(args);
    }
    else if (name === "setActiveCamera") {
        API.setActiveCamera(cameras[args[0]]);
    }
    else if (name === "interpolateCamera") {
        interpolateCamera(args);
    }
    else if (name === "clearCameras") {
        API.setActiveCamera(null);
        cameras = [];
    }
    else if (name === "setActiveCamera") {
        API.setActiveCamera(args[0]);
    }
    else if (name === "backToGamecam") {
        API.setActiveCamera(null);
    }
    else if (name === "setCameraHeading") {
        setCameraHeading(args);
    }
    else if (name === "drawNotification") {
        createNotification(args[0], args[1]);
    }
    else if (name === "killDrawMenus") {
        drawMenu.killMenu();
    }
});

var resolution = API.getScreenResolutionMaintainRatio();
var resX = resolution.Width;
var resY = resolution.Height;
var panelMinX = Math.round(resX / 32);
var panelMinY = Math.round(resY / 18);
var button = null;
var panel = null;
var image = null;
var notification = null;
var notifications = [];
var textnotification = null;
var textnotifications = [];
var padding = 10;
var selectedInput = null;
var tabIndex = [];
var tab = 0;
var menuElements = [];
var isReady = false;
var currentPage = 0;
var clickDelay = new Date().getTime();
var language = 0;
var drawMenuCurrentMenu = null;

function createMenu(pages) {
    if (drawMenuCurrentMenu !== null) {
        isReady = false;
        currentPage = 0;
        selectedInput = null;
        tabIndex = [];
        menuElements = [];
    }
    drawMenuCurrentMenu = new MenuDraw(pages);
    return drawMenuCurrentMenu;
}
var MenuDraw = (function () {
    function MenuDraw(pages) {
        if (Array.isArray(menuElements[0])) {
            return;
        }
        for (var i = 0; i < pages; i++) {
            var emptyArray = [];
            menuElements.push(emptyArray);
        }
        this._blur = false;
    }
    Object.defineProperty(MenuDraw.prototype, "Ready", {
        get: function () {
            return isReady;
        },
        set: function (value) {
            isReady = value;
            currentPage = 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuDraw.prototype, "Blur", {
        get: function () {
            return this._blur;
        },
        set: function (value) {
            this._blur = value;
            if (value) {
                API.callNative("_TRANSITION_TO_BLURRED", 3000);
            }
            else {
                API.callNative("_TRANSITION_FROM_BLURRED", 3000);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuDraw.prototype, "Page", {
        get: function () {
            return currentPage;
        },
        set: function (value) {
            currentPage = value;
        },
        enumerable: true,
        configurable: true
    });
    MenuDraw.prototype.DisableOverlays = function (value) {
        this._overlays = value;
        if (value) {
            API.setHudVisible(false);
            API.setChatVisible(false);
            API.setCanOpenChat(false);
            API.showCursor(true);
            return;
        }
        if (!value) {
            API.setHudVisible(true);
            API.setChatVisible(true);
            API.setCanOpenChat(true);
            API.showCursor(false);
            return;
        }
    };
    MenuDraw.prototype.DisableOverlaysWithHud = function (value) {
        this._overlays = value;
        if (value) {
            API.setCanOpenChat(false);
            API.showCursor(true);
            return;
        }
        if (!value) {
            API.setCanOpenChat(true);
            API.showCursor(false);
            return;
        }
    };
    MenuDraw.prototype.killMenu = function () {
        isReady = false;
        API.showCursor(false);
        API.setHudVisible(true);
        API.setChatVisible(true);
        API.setCanOpenChat(true);
        API.callNative("_TRANSITION_FROM_BLURRED", 3000);
    };
    MenuDraw.prototype.setLanguage = function (value) {
        language = value;
    };
    MenuDraw.prototype.nextPage = function () {
        if (currentPage + 1 > menuElements.length - 1) {
            currentPage = 0;
            return;
        }
        currentPage++;
    };
    MenuDraw.prototype.prevPage = function () {
        if (currentPage - 1 < 0) {
            currentPage = menuElements.length - 1;
            return;
        }
        currentPage--;
    };
    MenuDraw.prototype.createPanel = function (page, xStart, yStart, xGridWidth, yGridHeight) {
        var newPanel = new Panel(page, xStart, yStart, xGridWidth, yGridHeight);
        menuElements[page].push(newPanel);
        return newPanel;
    };
    return MenuDraw;
}());
var ProgressBar = (function () {
    function ProgressBar(x, y, width, height, currentProgress) {
        this._xPos = x * panelMinX;
        this._yPos = y * panelMinY;
        this._width = width * panelMinX - 10;
        this._height = height * panelMinY - 10;
        this._currentProgress = currentProgress;
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._alpha = 255;
        this._drawText = true;
    }
    ProgressBar.prototype.draw = function () {
        API.drawRectangle(this._xPos + 5, this._yPos + 5, ((this._width / 100) * this._currentProgress), this._height, this._r, this._g, this._b, this._alpha);
        if (this._drawText) {
            API.drawText("" + Math.round(this._currentProgress), this._xPos + (((this._width / 100) * this._currentProgress) / 2), this._yPos, 0.5, 255, 255, 255, 255, 4, 1, false, true, 100);
        }
    };
    ProgressBar.prototype.setColor = function (r, g, b) {
        this._r = r;
        this._g = g;
        this._b = b;
    };
    Object.defineProperty(ProgressBar.prototype, "Alpha", {
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressBar.prototype, "DrawText", {
        set: function (value) {
            this._drawText = value;
        },
        enumerable: true,
        configurable: true
    });
    ProgressBar.prototype.addProgress = function (value) {
        if (this._currentProgress + value > 100) {
            this._currentProgress = 100;
            return;
        }
        this._currentProgress += value;
    };
    ProgressBar.prototype.subtractProgress = function (value) {
        if (this._currentProgress - value < 0) {
            this._currentProgress = 0;
            return;
        }
        this._currentProgress -= value;
    };
    ProgressBar.prototype.setProgressAmount = function (value) {
        if (value >= 100) {
            this._currentProgress = 100;
            return;
        }
        if (value <= 0) {
            this._currentProgress = 0;
            return;
        }
        this._currentProgress = value;
        return;
    };
    ProgressBar.prototype.returnProgressAmount = function () {
        return this._currentProgress;
    };
    ProgressBar.prototype.returnType = function () {
        return "ProgressBar";
    };
    return ProgressBar;
}());
var DrawNotification = (function () {
    function DrawNotification(text, displayTime) {
        this._currentPosX = 26 * panelMinX;
        this._currentPosY = resY;
        this._targetX = 26 * panelMinX;
        this._targetY = 15 * panelMinY;
        this._width = panelMinX * 5;
        this._height = panelMinY * 3;
        this._text = text;
        this._r = 255;
        this._g = 165;
        this._b = 0;
        this._offset = 0;
        this._textScale = 0.5;
        this._lastUpdateTime = new Date().getTime();
        this._alpha = 255;
        this._displayTime = displayTime;
        this._incrementer = 0;
        this._sound = true;
    }
    DrawNotification.prototype.draw = function () {
        if (notification !== this) {
            return;
        }
        if (this._sound) {
            this._sound = false;
            API.playSoundFrontEnd("GOLF_NEW_RECORD", "HUD_AWARDS");
        }
        API.drawRectangle(this._currentPosX, this._currentPosY - 5, this._width, 5, this._r, this._g, this._b, this._alpha - 30);
        API.drawRectangle(this._currentPosX, this._currentPosY, this._width, this._height, 0, 0, 0, this._alpha - 30);
        API.drawText(this._text, this._offset + this._currentPosX + (this._width / 2), this._currentPosY + (this._height / 4), this._textScale, 255, 255, 255, this._alpha, 4, 1, false, false, this._width - padding);
        this.animate();
    };
    DrawNotification.prototype.animate = function () {
        if (this._currentPosY <= this._targetY) {
            this._currentPosY = this._targetY;
            if (new Date().getTime() > this._lastUpdateTime + this._displayTime) {
                this.fade();
                return;
            }
            return;
        }
        this._lastUpdateTime = new Date().getTime();
        if (this._currentPosY <= this._targetY + (this._height / 6)) {
            this._currentPosY -= 3;
            return;
        }
        else {
            this._currentPosY -= 5;
            return;
        }
    };
    DrawNotification.prototype.fade = function () {
        if (this._alpha <= 0) {
            this.cleanUpNotification();
            return;
        }
        this._alpha -= 5;
        return;
    };
    DrawNotification.prototype.cleanUpNotification = function () {
        notification = null;
    };
    DrawNotification.prototype.setText = function (value) {
        this._text = value;
    };
    DrawNotification.prototype.setColor = function (r, g, b) {
        this._r = r;
        this._g = g;
        this._b = b;
    };
    DrawNotification.prototype.setTextScale = function (value) {
        this._textScale = value;
    };
    DrawNotification.prototype.isHovered = function () {
        return;
    };
    DrawNotification.prototype.isClicked = function () {
        return;
    };
    DrawNotification.prototype.returnType = function () {
        return "DrawNotification";
    };
    return DrawNotification;
}());
var TextElement = (function () {
    function TextElement(text, x, y, width, height, line) {
        this._xPos = x;
        this._yPos = y + (panelMinY * line);
        this._width = width;
        this._height = height;
        this._text = text;
        this._fontScale = 0.6;
        this._centered = false;
        this._centeredVertically = false;
        this._font = 4;
        this._fontR = 255;
        this._fontG = 255;
        this._fontB = 255;
        this._fontAlpha = 255;
        this._hoverTextAlpha = 255;
        this._hoverTextR = 255;
        this._hoverTextG = 255;
        this._hoverTextB = 255;
        this._offset = 0;
        this._padding = 10;
        this._hovered = false;
        this._shadow = false;
        this._outline = false;
    }
    TextElement.prototype.draw = function () {
        if (this._centered && this._centeredVertically) {
            this.drawAsCenteredAll();
            return;
        }
        if (this._centered) {
            this.drawAsCentered();
            return;
        }
        if (this._centeredVertically) {
            this.drawAsCenteredVertically();
            return;
        }
        this.drawAsNormal();
    };
    Object.defineProperty(TextElement.prototype, "Text", {
        set: function (value) {
            this._text = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Hovered", {
        get: function () {
            return this._hovered;
        },
        set: function (value) {
            this._hovered = value;
        },
        enumerable: true,
        configurable: true
    });
    TextElement.prototype.Color = function (r, g, b, a) {
        this._fontR = r;
        this._fontG = g;
        this._fontB = b;
        this._fontAlpha = a;
    };
    TextElement.prototype.HoverColor = function (r, g, b, a) {
        this._hoverTextR = r;
        this._hoverTextG = g;
        this._hoverTextB = b;
        this._hoverTextAlpha = a;
    };
    Object.defineProperty(TextElement.prototype, "R", {
        get: function () {
            return this._fontR;
        },
        set: function (value) {
            this._fontR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "G", {
        get: function () {
            return this._fontG;
        },
        set: function (value) {
            this._fontG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "B", {
        get: function () {
            return this._fontB;
        },
        set: function (value) {
            this._fontB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Alpha", {
        get: function () {
            return this._fontAlpha;
        },
        set: function (value) {
            this._fontAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverTextAlpha;
        },
        set: function (value) {
            this._hoverTextAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverR", {
        get: function () {
            return this._hoverTextR;
        },
        set: function (value) {
            this._hoverTextR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverG", {
        get: function () {
            return this._hoverTextG;
        },
        set: function (value) {
            this._hoverTextG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "HoverB", {
        get: function () {
            return this._hoverTextB;
        },
        set: function (value) {
            this._hoverTextB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Font", {
        get: function () {
            return this._font;
        },
        set: function (value) {
            this._font = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "FontScale", {
        get: function () {
            return this._fontScale;
        },
        set: function (value) {
            this._fontScale = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "VerticalCentered", {
        get: function () {
            return this._centeredVertically;
        },
        set: function (value) {
            this._centeredVertically = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Centered", {
        get: function () {
            return this._centered;
        },
        set: function (value) {
            this._centered = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TextElement.prototype, "Offset", {
        set: function (value) {
            this._offset = value;
        },
        enumerable: true,
        configurable: true
    });
    TextElement.prototype.drawAsCenteredAll = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 1, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 1, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsCenteredVertically = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos, this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 0, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos, this._yPos + (this._height / 2) - ((this._fontScale * 80) / 2), this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 0, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsCentered = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._padding + this._yPos, this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 1, this._shadow, this._outline, this._width);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + (this._width / 2), this._padding + this._yPos, this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 1, this._shadow, this._outline, this._width);
    };
    TextElement.prototype.drawAsNormal = function () {
        if (this._hovered) {
            API.drawText(this._text, this._offset + this._xPos + this._padding, this._yPos + this._padding, this._fontScale, this._hoverTextR, this._hoverTextG, this._hoverTextB, this._hoverTextAlpha, this._font, 0, this._shadow, this._outline, this._width - this._padding);
            return;
        }
        API.drawText(this._text, this._offset + this._xPos + this._padding, this._yPos + this._padding, this._fontScale, this._fontR, this._fontG, this._fontB, this._fontAlpha, this._font, 0, this._shadow, this._outline, this._width - this._padding);
    };
    return TextElement;
}());
var Panel = (function () {
    function Panel(page, x, y, width, height) {
        this._page = page;
        this._padding = 10;
        this._xPos = x * panelMinX;
        this._yPos = y * panelMinY;
        this._width = width * panelMinX;
        this._height = height * panelMinY;
        this._alpha = 225;
        this._border = false;
        this._header = false;
        this._headerR = 0;
        this._headerG = 0;
        this._headerB = 0;
        this._headerAlpha = 0;
        this._offset = 0;
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._textLines = [];
        this._inputPanels = [];
        this._progressBars = [];
        this._currentLine = 0;
        this._shadow = false;
        this._outline = false;
        this._tooltip = null;
        this._hovered = false;
        this._hoverTime = 0;
        this._hoverR = 0;
        this._hoverG = 0;
        this._hoverB = 0;
        this._hoverAlpha = 200;
        this._backgroundImage = null;
        this._backgroundImagePadding = 0;
        this._function = null;
        this._functionArgs = [];
        this._functionAudioLib = "Click";
        this._functionAudioName = "DLC_HEIST_HACKING_SNAKE_SOUNDS";
        this._hoverAudioLib = "Cycle_Item";
        this._hoverAudioName = "DLC_Dmod_Prop_Editor_Sounds";
        this._hoverAudio = true;
        this._functionClickAudio = true;
        this._hoverable = false;
        this._line = 0;
    }
    Panel.prototype.draw = function () {
        if (this._page !== currentPage) {
            return;
        }
        this.drawRectangles();
        if (!isReady) {
            return;
        }
        if (this._textLines.length > 0) {
            for (var i = 0; i < this._textLines.length; i++) {
                this._textLines[i].draw();
            }
        }
        if (this._inputPanels.length > 0) {
            for (var i = 0; i < this._inputPanels.length; i++) {
                this._inputPanels[i].draw();
            }
        }
        if (this._progressBars.length > 0) {
            for (var i_1 = 0; i_1 < this._progressBars.length; i_1++) {
                this._progressBars[i_1].draw();
            }
        }
    };
    Panel.prototype.drawRectangles = function () {
        if (this._backgroundImage !== null) {
            this.drawBackgroundImage();
            return;
        }
        if (this._hovered) {
            if (this._border) {
                API.drawRectangle(this._xPos - 4, this._yPos - 4, this._width + 8, this._height + 8, 0, 0, 0, 255);
            }
            API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._hoverR, this._hoverG, this._hoverB, this._hoverAlpha);
            if (this._header) {
                API.drawRectangle(this._xPos, this._yPos + this._height - 5, this._width, 5, this._headerR, this._headerG, this._headerB, this._headerAlpha);
            }
            return;
        }
        if (this._border) {
            API.drawRectangle(this._xPos - 4, this._yPos - 4, this._width + 8, this._height + 8, 0, 0, 0, 255);
        }
        API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._r, this._g, this._b, this._alpha);
        if (this._header) {
            API.drawRectangle(this._xPos, this._yPos + this._height - 5, this._width, 5, this._headerR, this._headerG, this._headerB, this._headerAlpha);
        }
    };
    Panel.prototype.drawBackgroundImage = function () {
        if (this._backgroundImagePadding > 1) {
            API.dxDrawTexture(this._backgroundImage, new Point(this._xPos + this._backgroundImagePadding, this._yPos + this._backgroundImagePadding), new Size(this._width - (this._backgroundImagePadding * 2), this._height - (this._backgroundImagePadding * 2)), 0);
            API.drawRectangle(this._xPos, this._yPos, this._width, this._height, this._r, this._g, this._b, this._alpha);
            return;
        }
        API.dxDrawTexture(this._backgroundImage, new Point(this._xPos, this._yPos), new Size(this._width, this._height), 0);
    };
    Object.defineProperty(Panel.prototype, "Function", {
        set: function (value) {
            this._function = value;
        },
        enumerable: true,
        configurable: true
    });
    Panel.prototype.addFunctionArgs = function (value) {
        this._functionArgs = value;
    };
    Object.defineProperty(Panel.prototype, "HoverAudioLib", {
        get: function () {
            return this._hoverAudioLib;
        },
        set: function (value) {
            this._hoverAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverAudioName", {
        get: function () {
            return this._hoverAudioName;
        },
        set: function (value) {
            this._hoverAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudioLib", {
        get: function () {
            return this._functionAudioLib;
        },
        set: function (value) {
            this._functionAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudioName", {
        get: function () {
            return this._functionAudioName;
        },
        set: function (value) {
            this._functionAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FunctionAudio", {
        get: function () {
            return this._functionClickAudio;
        },
        set: function (value) {
            this._functionClickAudio = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainAlpha", {
        get: function () {
            return this._alpha;
        },
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainBackgroundImagePadding", {
        get: function () {
            return this._backgroundImagePadding;
        },
        set: function (value) {
            this._backgroundImagePadding = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainBackgroundImage", {
        get: function () {
            return this._backgroundImage;
        },
        set: function (value) {
            this._backgroundImage = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorR", {
        get: function () {
            return this._r;
        },
        set: function (value) {
            this._r = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorG", {
        get: function () {
            return this._g;
        },
        set: function (value) {
            this._g = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "MainColorB", {
        get: function () {
            return this._b;
        },
        set: function (value) {
            this._b = value;
        },
        enumerable: true,
        configurable: true
    });
    Panel.prototype.MainBackgroundColor = function (r, g, b, alpha) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._alpha = alpha;
    };
    Panel.prototype.HoverBackgroundColor = function (r, g, b, alpha) {
        this._hoverR = r;
        this._hoverG = g;
        this._hoverB = b;
        this._hoverAlpha = alpha;
    };
    Panel.prototype.HeaderColor = function (r, g, b, alpha) {
        this._headerR = r;
        this._headerG = g;
        this._headerB = b;
        this._headerAlpha = alpha;
    };
    Object.defineProperty(Panel.prototype, "Hoverable", {
        get: function () {
            return this._hoverable;
        },
        set: function (value) {
            this._hoverable = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverAlpha;
        },
        set: function (value) {
            this._hoverAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverR", {
        get: function () {
            return this._hoverR;
        },
        set: function (value) {
            this._hoverR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverG", {
        get: function () {
            return this._hoverG;
        },
        set: function (value) {
            this._hoverG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "HoverB", {
        get: function () {
            return this._hoverB;
        },
        set: function (value) {
            this._hoverB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FontOutline", {
        get: function () {
            return this._outline;
        },
        set: function (value) {
            this._outline = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "FontShadow", {
        get: function () {
            return this._shadow;
        },
        set: function (value) {
            this._shadow = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Tooltip", {
        get: function () {
            return this._tooltip;
        },
        set: function (value) {
            this._tooltip = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Border", {
        get: function () {
            return this._border;
        },
        set: function (value) {
            this._border = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Header", {
        get: function () {
            return this._header;
        },
        set: function (value) {
            this._header = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Panel.prototype, "Offset", {
        get: function () {
            return this._offset;
        },
        set: function (value) {
            this._offset = value;
        },
        enumerable: true,
        configurable: true
    });
    Panel.prototype.addText = function (value) {
        var textElement = new TextElement(value, this._xPos, this._yPos, this._width, this._height, this._line);
        this._textLines.push(textElement);
        this._line += 1;
        return textElement;
    };
    Panel.prototype.addInput = function (x, y, width, height) {
        var inputPanel = new InputPanel(this._page, (x * panelMinX) + this._xPos, (y * panelMinY) + this._yPos, width, height);
        this._inputPanels.push(inputPanel);
        return inputPanel;
    };
    Panel.prototype.addProgressBar = function (x, y, width, height, currentProgress) {
        var progressBar = new ProgressBar((x * panelMinX) + this._xPos, (y * panelMinY) + this._yPos, width, height, currentProgress);
        this._progressBars.push(progressBar);
        return progressBar;
    };
    Panel.prototype.isHovered = function () {
        if (!API.isCursorShown()) {
            return;
        }
        if (!this._hoverable) {
            return;
        }
        var cursorPos = API.getCursorPositionMaintainRatio();
        if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < this._yPos + this._height) {
            if (!this._hovered) {
                this._hovered = true;
                this.setTextHoverState(true);
                if (this._hoverAudio) {
                    API.playSoundFrontEnd(this._hoverAudioLib, this._hoverAudioName);
                }
            }
            this._hoverTime += 1;
            if (this._hoverTime > 50) {
                if (this._tooltip === null) {
                    return;
                }
                if (this._tooltip.length > 1) {
                    API.drawText(this._tooltip, cursorPos.X + 25, cursorPos.Y, 0.4, 255, 255, 255, 255, 4, 0, true, true, 200);
                }
            }
            return;
        }
        this._hovered = false;
        this._hoverTime = 0;
        this.setTextHoverState(false);
    };
    Panel.prototype.isClicked = function () {
        if (!API.isCursorShown()) {
            return;
        }
        if (this._function === null) {
            return;
        }
        if (API.isControlJustReleased(237)) {
            var cursorPos = API.getCursorPositionMaintainRatio();
            if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < this._yPos + this._height) {
                if (new Date().getTime() < clickDelay + 200) {
                    return;
                }
                clickDelay = new Date().getTime();
                if (this._functionClickAudio) {
                    API.playSoundFrontEnd(this._functionAudioLib, this._functionAudioName);
                }
                if (this._functionArgs !== null || this._functionArgs.length > 1) {
                    this._function(this._functionArgs);
                }
                else {
                    this._function();
                }
                return;
            }
        }
    };
    Panel.prototype.setTextHoverState = function (value) {
        for (var i = 0; i < this._textLines.length; i++) {
            this._textLines[i].Hovered = value;
        }
    };
    Panel.prototype.returnType = function () {
        return "Panel";
    };
    return Panel;
}());
var InputPanel = (function () {
    function InputPanel(page, x, y, width, height) {
        this._xPos = x;
        this._yPos = y;
        this._width = width * panelMinX;
        this._height = height * panelMinY;
        this._protected = false;
        this._input = "";
        this._hovered = false;
        this._selected = false;
        this._maxLength = 397;
        this._isLongText = false;
        this._font = 4;
        this._numeric = false;
        this._isError = false;
        this._isTransparent = false;
        this._r = 255;
        this._g = 255;
        this._b = 255;
        this._alpha = 100;
        this._hoverR = 255;
        this._hoverG = 255;
        this._hoverB = 255;
        this._hoverAlpha = 125;
        this._selectR = 255;
        this._selectG = 255;
        this._selectB = 255;
        this._inputTextR = 0;
        this._inputTextG = 0;
        this._inputTextB = 0;
        this._inputTextAlpha = 255;
        this._selectAlpha = 125;
        this._inputAudioLib = "Click";
        this._inputAudioName = "DLC_HEIST_HACKING_SNAKE_SOUNDS";
        this._page = page;
        this._inputTextScale = 0.45;
        tabIndex.push(this);
    }
    Object.defineProperty(InputPanel.prototype, "isError", {
        set: function (value) {
            this._isError = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Selected", {
        get: function () {
            return this._selected;
        },
        set: function (value) {
            this._selected = value;
            if (value) {
                selectedInput = this;
            }
            else {
                selectedInput = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "AudioLib", {
        get: function () {
            return this._inputAudioLib;
        },
        set: function (value) {
            this._inputAudioLib = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "AudioName", {
        get: function () {
            return this._inputAudioName;
        },
        set: function (value) {
            this._inputAudioName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Font", {
        get: function () {
            return this._font;
        },
        set: function (value) {
            this._font = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverR", {
        get: function () {
            return this._hoverR;
        },
        set: function (value) {
            this._hoverR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverG", {
        get: function () {
            return this._hoverG;
        },
        set: function (value) {
            this._hoverG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverB", {
        get: function () {
            return this._hoverB;
        },
        set: function (value) {
            this._hoverB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "HoverAlpha", {
        get: function () {
            return this._hoverAlpha;
        },
        set: function (value) {
            this._hoverAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    InputPanel.prototype.MainBackgroundColor = function (r, g, b, alpha) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._alpha = alpha;
    };
    InputPanel.prototype.HoverBackgroundColor = function (r, g, b, alpha) {
        this._hoverR = r;
        this._hoverG = g;
        this._hoverB = b;
        this._hoverAlpha = alpha;
    };
    InputPanel.prototype.SelectBackgroundColor = function (r, g, b, alpha) {
        this._selectR = r;
        this._selectG = g;
        this._selectB = b;
        this._selectAlpha = alpha;
    };
    Object.defineProperty(InputPanel.prototype, "R", {
        get: function () {
            return this._r;
        },
        set: function (value) {
            this._r = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "G", {
        get: function () {
            return this._g;
        },
        set: function (value) {
            this._g = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "B", {
        get: function () {
            return this._b;
        },
        set: function (value) {
            this._b = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Alpha", {
        get: function () {
            return this._alpha;
        },
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectR", {
        get: function () {
            return this._selectR;
        },
        set: function (value) {
            this._selectR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectG", {
        get: function () {
            return this._selectG;
        },
        set: function (value) {
            this._selectG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectB", {
        get: function () {
            return this._selectB;
        },
        set: function (value) {
            this._selectB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "SelectAlpha", {
        get: function () {
            return this._selectAlpha;
        },
        set: function (value) {
            this._selectAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    InputPanel.prototype.InputTextColor = function (r, g, b, alpha) {
        this._inputTextR = r;
        this._inputTextG = g;
        this._inputTextB = b;
        this._inputTextAlpha = alpha;
    };
    Object.defineProperty(InputPanel.prototype, "TextR", {
        get: function () {
            return this._inputTextR;
        },
        set: function (value) {
            this._inputTextR = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "TextG", {
        get: function () {
            return this._inputTextG;
        },
        set: function (value) {
            this._inputTextG = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "TextB", {
        set: function (value) {
            this._inputTextB = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputB", {
        get: function () {
            return this._inputTextB;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputAlpha", {
        get: function () {
            return this._inputTextAlpha;
        },
        set: function (value) {
            this._inputTextAlpha = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "InputTextScale", {
        get: function () {
            return this._inputTextScale;
        },
        set: function (value) {
            this._inputTextScale = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Input", {
        get: function () {
            return this._input;
        },
        set: function (value) {
            this._input = value;
        },
        enumerable: true,
        configurable: true
    });
    InputPanel.prototype.removeFromInput = function () {
        this._input = this._input.substring(0, this._input.length - 1);
    };
    Object.defineProperty(InputPanel.prototype, "NumericOnly", {
        get: function () {
            return this._numeric;
        },
        set: function (value) {
            this._numeric = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "MaxLength", {
        get: function () {
            return this._maxLength;
        },
        set: function (value) {
            this._maxLength = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "LongText", {
        get: function () {
            return this._isLongText;
        },
        set: function (value) {
            this._isLongText = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputPanel.prototype, "Protected", {
        set: function (value) {
            this._protected = value;
        },
        enumerable: true,
        configurable: true
    });
    InputPanel.prototype.draw = function () {
        if (this._selected) {
            this.selectedDraw();
        }
        if (this._hovered) {
            this.hoveredDraw();
        }
        if (!this._hovered && !this._selected) {
            this.normalDraw();
        }
        this.isHovered();
        this.isClicked();
    };
    InputPanel.prototype.normalDraw = function () {
        if (this._isError) {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, 255, 0, 0, 100);
        }
        else {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._r, this._g, this._b, this._alpha);
        }
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText === true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
    };
    InputPanel.prototype.selectedDraw = function () {
        API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._selectR, this._selectG, this._selectB, this._selectAlpha);
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText === true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
        return;
    };
    InputPanel.prototype.hoveredDraw = function () {
        if (this._isError) {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, 255, 0, 0, 100);
        }
        else {
            API.drawRectangle(this._xPos + 10, this._yPos + 10, this._width - 20, this._height - 20, this._hoverR, this._hoverG, this._hoverB, this._hoverAlpha);
        }
        if (this._protected) {
            if (this._input.length < 1) {
                return;
            }
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            API.drawText("*".repeat(this._input.length), this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, 4, 1, false, false, (panelMinX * this._width));
        }
        else {
            if (this._input.length > this._maxLength) {
                selectedInput.removeFromInput();
            }
            if (this._isLongText == true) {
                API.drawText(this._input, this._xPos + 15, this._yPos + 10, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 0, false, false, this._width - 20);
            }
            else {
                API.drawText(this._input, this._xPos + (this._width / 2), this._yPos + (this._height / 2) - 14, this._inputTextScale, this._inputTextR, this._inputTextG, this._inputTextB, this._inputTextAlpha, this._font, 1, false, false, (panelMinX * this._width));
            }
        }
    };
    InputPanel.prototype.isHovered = function () {
        if (!API.isCursorShown()) {
            return;
        }
        var cursorPos = API.getCursorPositionMaintainRatio();
        if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < (this._yPos + this._height)) {
            if (this._selected) {
                this._hovered = false;
                return;
            }
            this._hovered = true;
        }
        else {
            this._hovered = false;
        }
    };
    InputPanel.prototype.isClicked = function () {
        if (API.isControlJustPressed(237)) {
            if (new Date().getTime() < clickDelay + 200) {
                return;
            }
            var cursorPos = API.getCursorPositionMaintainRatio();
            if (cursorPos.X > this._xPos && cursorPos.X < (this._xPos + this._width) && cursorPos.Y > this._yPos && cursorPos.Y < (this._yPos + this._height)) {
                if (!this._selected) {
                    API.playSoundFrontEnd(this._inputAudioLib, this._inputAudioName);
                    this._selected = true;
                }
                selectedInput = this;
            }
            else {
                this._selected = false;
            }
        }
    };
    InputPanel.prototype.addToInput = function (text) {
        if (!this._numeric) {
            this._input += text;
            return this._input;
        }
        else {
            if (Number.isInteger(+text)) {
                this._input += text;
                return this._input;
            }
        }
    };
    InputPanel.prototype.returnType = function () {
        return "InputPanel";
    };
    return InputPanel;
}());
function drawTextNotification() {
    if (textnotification !== null) {
        textnotification.draw();
        return;
    }
    if (textnotifications.length <= 0) {
        return;
    }
    textnotification = textnotifications.shift();
    return;
}
function drawNotification() {
    if (notification !== null) {
        notification.draw();
        return;
    }
    if (notifications.length <= 0) {
        return;
    }
    notification = notifications.shift();
    return;
}
function createNotification(text, displayTime) {
    var notify = new DrawNotification(text, displayTime);
    notifications.push(notify);
    return notify;
}
function setPage(value) {
    currentPage = value;
}
function killDrawMenu() {
    isReady = false;
    currentPage = -1;
    selectedInput = null;
    tabIndex = [];
    API.showCursor(false);
    API.setHudVisible(true);
    API.setChatVisible(true);
    API.setCanOpenChat(true);
    API.callNative("_TRANSITION_FROM_BLURRED", 3000);
    menuElements = [];
}
var drawMenu;

API.onKeyDown.connect(function (sender, e) {
    if (e.Control && e.KeyCode === Keys.Z) {
        menuMainPanel();
    }
    if (e.Control && e.KeyCode === Keys.X) {
        drawMenu.killMenu();
    }
    if (e.Control && e.KeyCode === Keys.G) {
        var currentWeapon = API.getPlayerCurrentWeapon();
        API.removePlayerWeapon(currentWeapon);
        API.sendNotification("Dropped weapon: " + API.getWeaponName(currentWeapon));
    }
    if (e.Control && e.KeyCode === Keys.D1) {
        API.setEntitySyncedData(API.getLocalPlayer(), "Speedometr", 1);
    }
    if (e.Control && e.KeyCode === Keys.D2) {
        API.setEntitySyncedData(API.getLocalPlayer(), "Speedometr", 2);
    }
    if (e.Control && e.KeyCode === Keys.D3) {
        drawMenu = createMenu(2);
        var panel_1;
        var inputPanel = void 0;
        var textElement = void 0;
        for (var i = 0; i < 2; i++) {
            panel_1 = drawMenu.createPanel(i, 12, 1, 8, 2);
            panel_1.MainBackgroundColor(92, 49, 4, 255);
            panel_1.Header = true;
            textElement = panel_1.addText("Menu Builder");
            textElement.Centered = true;
            textElement.Font = 1;
            textElement.Color(206, 213, 206, 255);
            textElement = panel_1.addText("Page ( " + i + " / 1 )");
            textElement.Font = 4;
            textElement.Color(206, 213, 206, 255);
            textElement.Centered = true;
            textElement.FontScale = 0.45;
            panel_1 = drawMenu.createPanel(i, 11, 1, 1, 2);
            panel_1.MainBackgroundColor(92, 49, 4, 255);
            panel_1.Function = drawMenu.prevPage;
            panel_1.Header = true;
            panel_1.Hoverable = true;
            panel_1.HoverBackgroundColor(154, 110, 64, 255);
            panel_1.Tooltip = "Previous Page";
            textElement = panel_1.addText("<");
            textElement.Centered = true;
            textElement.VerticalCentered = true;
            textElement.Color(206, 213, 206, 255);
            panel_1 = drawMenu.createPanel(i, 20, 1, 1, 2);
            panel_1.MainBackgroundColor(92, 49, 4, 255);
            panel_1.Function = drawMenu.nextPage;
            panel_1.Header = true;
            panel_1.Hoverable = true;
            panel_1.HoverBackgroundColor(154, 110, 64, 255);
            panel_1.Tooltip = "Next Page";
            textElement = panel_1.addText(">");
            textElement.VerticalCentered = true;
            textElement.Centered = true;
            textElement.Color(206, 213, 206, 255);
        }
        panel_1 = drawMenu.createPanel(0, 11, 3, 10, 10);
        panel_1.MainBackgroundColor(206, 213, 206, 255);
        textElement = panel_1.addText("Line 0");
        textElement.Centered = true;
        textElement.Font = 4;
        textElement.FontScale = 0.4;
        textElement.Color(83, 117, 40, 255);
        textElement = panel_1.addText("Line 1");
        textElement.Centered = true;
        textElement.Font = 4;
        textElement.FontScale = 0.4;
        textElement.Color(83, 117, 40, 255);
        textElement = panel_1.addText("Line 2");
        textElement.Centered = true;
        textElement.Font = 4;
        textElement.FontScale = 0.4;
        textElement.Color(83, 117, 40, 255);
        panel_1 = drawMenu.createPanel(0, 11, 13, 10, 1);
        panel_1.MainBackgroundColor(47, 76, 22, 255);
        panel_1.Hoverable = true;
        panel_1.Header = true;
        panel_1.Function = drawMenu.killMenu;
        panel_1.HoverBackgroundColor(83, 117, 40, 255);
        textElement = panel_1.addText("Fire Event");
        textElement.Centered = true;
        textElement.VerticalCentered = true;
        textElement.Font = 4;
        textElement.FontScale = 0.5;
        textElement.Color(206, 213, 206, 255);
        API.showCursor(true);
        panel_1 = drawMenu.createPanel(1, 11, 3, 10, 5);
        panel_1.MainBackgroundColor(206, 213, 206, 255);
        panel_1.MainBackgroundImagePadding = 10;
        panel_1.MainBackgroundImage = "Files/images/fill.png";
        panel_1 = drawMenu.createPanel(1, 11, 8, 10, 5);
        panel_1.MainBackgroundColor(206, 213, 206, 255);
        textElement = panel_1.addText("Regular Input");
        textElement.Color(83, 117, 40, 255);
        panel_1.addText(" ");
        inputPanel = panel_1.addInput(0, 1, 10, 1);
        inputPanel.MainBackgroundColor(0, 0, 0, 100);
        inputPanel.HoverBackgroundColor(0, 0, 0, 75);
        inputPanel.SelectBackgroundColor(0, 0, 0, 125);
        textElement = panel_1.addText("Numeric Input");
        textElement.Color(83, 117, 40, 255);
        inputPanel = panel_1.addInput(0, 3, 10, 1);
        inputPanel.NumericOnly = true;
        inputPanel.MainBackgroundColor(0, 0, 0, 100);
        inputPanel.HoverBackgroundColor(0, 0, 0, 75);
        inputPanel.SelectBackgroundColor(0, 0, 0, 125);
        drawMenu.Ready = true;
    }
});
API.onUpdate.connect(function () {
    //TODO: Probably change this

    var localPlayer = API.getLocalPlayer();
    if (!API.isPlayerInAnyVehicle(localPlayer)) {
        return;
    }
    var vehicle = API.getPlayerVehicle(localPlayer);
    if (API.getVehicleClass(API.getEntityModel(vehicle)) === 13) {
        return;
    }
    var isDriver = API.getPlayerVehicleSeat(localPlayer);
    if (isDriver === -1) {
        if (API.getEntitySyncedData(localPlayer, "Speedometr") === 1 && API.isPlayerInAnyVehicle(localPlayer)) {
            var pointX = new Point(parseInt(resX - 350 + ""), parseInt(resY - 350 + ""));
            var pointSize = new Size(300, 300);
            var veh = API.getPlayerVehicle(localPlayer);
            var velocity = API.getEntityVelocity(veh);
            var speed = Math.sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y + velocity.Z * velocity.Z) * 2.7;
            if (speed > 250) {
                speed = 250;
            }
            API.dxDrawTexture("Files/Images/speedo.png", pointX, pointSize);
            API.dxDrawTexture("Files/Images/line.png", pointX, pointSize, speed);
            var point = new Point(parseInt(resX - 350 + ""), parseInt(resY - 75 + ""));
            var fillSize = 0;
            var health = API.getVehicleHealth(veh);
            var maxhealth = API.returnNative("GET_ENTITY_MAX_HEALTH", 0, veh);
            var healthpercent = Math.floor((health / maxhealth) * 100);
            if (healthpercent > 0) {
                fillSize = parseInt(healthpercent * 3 + "");
            }
            API.dxDrawTexture("Files/Images/fill.png", point, new Size(fillSize, 28));
            API.dxDrawTexture("Files/Images/fuel.png", point, new Size(300, 28));
        }
        else {
            var veh = API.getPlayerVehicle(localPlayer);
            var velocity = API.getEntityVelocity(veh);
            var speed = Math.round(Math.sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y + velocity.Z * velocity.Z) * 2.25);
            var health = API.getVehicleHealth(veh);
            var maxhealth = API.returnNative("GET_ENTITY_MAX_HEALTH", 0, veh);
            var healthpercent = Math.floor((health / maxhealth) * 100);
            API.drawText("SPEED ~y~" + speed + "MPH\n~w~HP ~y~" + healthpercent + "%", resX - 25, resY - 100, 0.7, 255, 255, 255, 255, 4, 2, false, true, 0);
        }
    }
});

/*
API.onUpdate.connect(function () {
    var localPlayer = API.getLocalPlayer();
    var pos = API.getEntityPosition(localPlayer);
    var angel = API.getEntityRotation(localPlayer);
    var text = "~s~X:~y~" + pos.X.toFixed(6) + "~s~; Y:~y~" + pos.Y.toFixed(6) + "~s~; Z:~y~" + pos.Z.toFixed(6) + "~s~; A:~y~" + angel.Z.toFixed(6);
    API.drawText(text, resX - 800, resY - 80, 0.6, 255, 255, 255, 255, 4, 2, false, true, 0);
});*/

var activeCameras = [];
var lastCamera = null;
var activeCameraBlips = [];
var activeCameraMarkers = [];
var directorMode = false;
var defaultSpeed = 5000;
var resolutionSCriptY = API.getScreenResolutionMaintainRatio().Height;
var currentCamera = null;
var markersHidden = false;
API.onKeyDown.connect(function (player, e) {
    if (API.isChatOpen()) {
        return;
    }
    if (e.KeyCode === Keys.F12) {
        if (directorMode === false) {
            directorMode = true;
            API.playSoundFrontEnd("Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS");
            return;
        }
        else {
            directorMode = false;
            API.playSoundFrontEnd("CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET");
            cameraKillActive();
            clearCameraBlips();
            return;
        }
    }
    if (directorMode === false) {
        return;
    }
    if (e.KeyCode === Keys.D0) {
        cameraGoto(0);
        return;
    }
    if (e.KeyCode === Keys.D1) {
        cameraGoto(1);
        return;
    }
    if (e.KeyCode === Keys.D2) {
        cameraGoto(2);
        return;
    }
    if (e.KeyCode === Keys.D3) {
        cameraGoto(3);
        return;
    }
    if (e.KeyCode === Keys.D4) {
        cameraGoto(4);
        return;
    }
    if (e.KeyCode === Keys.D5) {
        cameraGoto(5);
        return;
    }
    if (e.KeyCode === Keys.D6) {
        cameraGoto(7);
        return;
    }
    if (e.KeyCode === Keys.D7) {
        cameraGoto(7);
        return;
    }
    if (e.KeyCode === Keys.D8) {
        cameraGoto(8);
        return;
    }
    if (e.KeyCode === Keys.D9) {
        cameraGoto(9);
        return;
    }
    if (e.KeyCode === Keys.Tab) {
        cameraSetup(API.getEntityPosition(API.getLocalPlayer()), API.getEntityRotation(API.getLocalPlayer()));
        return;
    }
    if (e.KeyCode === Keys.Back) {
        cameraKillActive();
        return;
    }
    if (e.KeyCode === Keys.Oemtilde) {
        cameraLastCamera();
        return;
    }
    if (e.KeyCode === Keys.Oemcomma) {
        defaultSpeed -= 500;
        return;
    }
    if (e.KeyCode === Keys.OemPeriod) {
        defaultSpeed += 500;
        return;
    }
    if (e.KeyCode === Keys.F11) {
        cameraAnimateAllPoints();
        return;
    }
    if (e.KeyCode === Keys.Delete) {
        var number = activeCameraBlips.length - 1;
        var numberMarker = activeCameraMarkers.length - 1;
        if (numberMarker === -1) {
            return;
        }
        if (number === -1) {
            return;
        }
        API.sendNotification("~b~Removing Camera at: ~w~" + number);
        API.deleteEntity(activeCameraBlips[number]);
        API.deleteEntity(activeCameraMarkers[numberMarker]);
        activeCameraBlips.pop();
        activeCameraMarkers.pop();
        activeCameras.pop();
        API.setActiveCamera(null);
        API.playSoundFrontEnd("CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET");
        return;
    }
    if (e.KeyCode === Keys.F9) {
        if (markersHidden) {
            for (var i = 0; i < activeCameraMarkers.length; i++) {
                API.setMarkerColor(activeCameraMarkers[i], 50, 0, 0, 255);
            }
            markersHidden = false;
            return;
        }
        else {
            for (var i = 0; i < activeCameraMarkers.length; i++) {
                API.setMarkerColor(activeCameraMarkers[i], 0, 0, 0, 255);
            }
            markersHidden = true;
        }
    }
    if (API.getActiveCamera() != null) {
        if (e.KeyCode === Keys.Oemplus && e.Shift) {
            var fov = API.getCameraFov(API.getActiveCamera());
            fov += 5;
            API.setCameraFov(API.getActiveCamera(), fov);
            return;
        }
        if (e.KeyCode === Keys.OemMinus && e.Shift) {
            var fov = API.getCameraFov(API.getActiveCamera());
            fov -= 5;
            API.setCameraFov(API.getActiveCamera(), fov);
            return;
        }
        if (e.KeyCode === Keys.Oemplus) {
            var activePos = API.getCameraPosition(API.getActiveCamera());
            API.setCameraPosition(API.getActiveCamera(), new Vector3(activePos.X, activePos.Y, activePos.Z + 0.5));
            return;
        }
        if (e.KeyCode === Keys.OemMinus) {
            var activePos = API.getCameraPosition(API.getActiveCamera());
            API.setCameraPosition(API.getActiveCamera(), new Vector3(activePos.X, activePos.Y, activePos.Z - 0.5));
            return;
        }
        if (e.KeyCode === Keys.P) {
            API.pointCameraAtEntity(API.getActiveCamera(), API.getLocalPlayer(), new Vector3());
            return;
        }
        if (e.KeyCode === Keys.OemOpenBrackets) {
            player = API.getLocalPlayer();
            API.pointCameraAtPosition(API.getActiveCamera(), API.getEntityPosition(player));
            return;
        }
        if (e.KeyCode === Keys.OemCloseBrackets) {
            player = API.getLocalPlayer();
            if (API.isPlayerInAnyVehicle(player) === false) {
                return;
            }
            API.pointCameraAtEntity(API.getActiveCamera(), API.getPlayerVehicle(player), new Vector3());
            return;
        }
    }
});
API.onUpdate.connect(function () {
    if (directorMode === false) {
        return;
    }
    API.drawText("~g~Director Mode", 27, resolutionSCriptY - 465, 0.6, 50, 211, 82, 255, 1, 0, true, true, 0);
    API.drawText("~y~F11 - Animate Cameras", 27, resolutionSCriptY - 425, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
    API.drawText("~b~Markers Hidden: ~w~" + markersHidden, 27, resolutionSCriptY - 390, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
    if (API.getActiveCamera() != null) {
        API.drawText("~y~FOV: ~w~" + API.getCameraFov(API.getActiveCamera()), 27, resolutionSCriptY - 355, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
    }
    API.drawText("~o~Speed: ~w~" + defaultSpeed, 27, resolutionSCriptY - 320, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
    API.drawText("~g~Current: ~w~" + currentCamera, 27, resolutionSCriptY - 285, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
    API.drawText("~b~Cameras: ~w~" + activeCameras.length, 27, resolutionSCriptY - 250, 0.5, 50, 211, 82, 255, 4, 0, true, true, 0);
});
function clearCameraBlips() {
    for (var i = 0; i < activeCameraBlips.length; i++) {
        API.deleteEntity(activeCameraBlips[i]);
    }
    activeCameras = [];
    activeCameraBlips = [];
    API.playSoundFrontEnd("Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS");
}
function cameraSetup(position, rotation) {
    var camera = API.createCamera(position, rotation);
    API.setActiveCamera(camera);
    activeCameras.push(camera);
    var blip = API.createBlip(position);
    API.setBlipSprite(blip, 135);
    var marker = API.createMarker(28, position, new Vector3(), rotation, new Vector3(0.2, 0.2, 0.2), 0, 0, 255, 50);
    activeCameraMarkers.push(marker);
    activeCameraBlips.push(blip);
    currentCamera = activeCameras.length - 1;
    API.playSoundFrontEnd("Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS");
}
function cameraGoto(camera) {
    if (activeCameras[camera] == null) {
        return;
    }
    currentCamera = camera;
    lastCamera = API.getActiveCamera();
    API.setActiveCamera(activeCameras[camera]);
    API.playSoundFrontEnd("Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS");
}
function cameraKillActive() {
    lastCamera = API.getActiveCamera();
    API.setActiveCamera(null);
    currentCamera = null;
    API.playSoundFrontEnd("CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET");
}
function cameraLastCamera() {
    if (lastCamera === null) {
        return;
    }
    API.setActiveCamera(lastCamera);
    API.playSoundFrontEnd("Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS");
    for (var i = 0; i < activeCameras.length; i++) {
        if (activeCameras[i] === lastCamera) {
            currentCamera = i;
            break;
        }
    }
    return;
}
function cameraAnimateAllPoints() {
    API.playSoundFrontEnd("5s", "MP_MISSION_COUNTDOWN_SOUNDSET");
    for (var i_2 = 0; i_2 < activeCameraMarkers.length; i_2++) {
        API.setMarkerColor(activeCameraMarkers[i_2], 0, 0, 0, 255);
    }
    API.sleep(5000);
    var i = 0;
    while (i !== activeCameras.length) {
        var currentCamera_1 = activeCameras[i];
        var nextCamera = activeCameras[i + 1];
        if (currentCamera_1 == null || nextCamera == null) {
            break;
        }
        API.interpolateCameras(currentCamera_1, nextCamera, defaultSpeed, true, true);
        API.sleep(defaultSpeed);
        i++;
    }
    API.sendNotification("Route complete");
    for (i = 0; i < activeCameraMarkers.length; i++) {
        API.setMarkerColor(activeCameraMarkers[i], 50, 0, 0, 255);
    }
}
var flyEnabled = false;
var currentPos;
var forward;
var backward;
var left;
var right;
var down;
var up;
var shift;
var maxSpeed;
API.onUpdate.connect(function () {
    if (!flyEnabled)
        return;
    var multiplier = 1;
    if (shift)
        multiplier = 3;
    if (maxSpeed)
        multiplier = 10;
    var camRotation = API.getGameplayCamRot();
    var camDirection = API.getGameplayCamDir();
    if (forward) {
        currentPos.X = currentPos.X + camDirection.X * multiplier;
        currentPos.Y = currentPos.Y + camDirection.Y * multiplier;
    }
    if (backward) {
        currentPos.X = currentPos.X - camDirection.X * multiplier;
        currentPos.Y = currentPos.Y - camDirection.Y * multiplier;
    }
    if (left) {
        currentPos.X = currentPos.X + camDirection.X;
        currentPos.Y = currentPos.Y - camDirection.Y;
    }
    if (right) {
        currentPos.X = currentPos.X - camDirection.X;
        currentPos.Y = currentPos.Y + camDirection.Y;
    }
    if (down) {
        currentPos.Z = currentPos.Z - multiplier;
    }
    if (up) {
        currentPos.Z = currentPos.Z + multiplier;
    }
    var newRotation = new Vector3(0, 0, camRotation.Z - 180);
    API.setEntityRotation(API.getLocalPlayer(), newRotation);
    API.setEntityPosition(API.getLocalPlayer(), currentPos);
});
API.onChatCommand.connect(function (msg) {
    if (msg === "/fly") {
        if (flyEnabled) {
            flyEnabled = false;
            API.sendChatMessage("[Fly] ~r~Disabled");
        }
        else {
            currentPos = API.getEntityPosition(API.getLocalPlayer());
            flyEnabled = true;
            API.sendChatMessage("[Fly] ~g~Enabled");
        }
    }
});
API.onKeyUp.connect(function (sender, key) {
    if (flyEnabled) {
        if (key.KeyCode === Keys.W) {
            forward = false;
        }
        else if (key.KeyCode === Keys.D) {
            right = false;
        }
        else if (key.KeyCode === Keys.A) {
            left = false;
        }
        else if (key.KeyCode === Keys.S) {
            backward = false;
        }
        else if (key.KeyCode === Keys.ControlKey) {
            down = false;
        }
        else if (key.KeyCode === Keys.ShiftKey) {
            shift = false;
        }
        else if (key.KeyCode === Keys.Space) {
            up = false;
        }
        else if (key.KeyCode === Keys.E) {
            maxSpeed = false;
        }
    }
});
API.onKeyDown.connect(function (sender, key) {
    if (flyEnabled) {
        if (key.KeyCode === Keys.W) {
            forward = true;
        }
        else if (key.KeyCode === Keys.D) {
            right = true;
        }
        else if (key.KeyCode === Keys.A) {
            left = true;
        }
        else if (key.KeyCode === Keys.S) {
            backward = true;
        }
        else if (key.KeyCode === Keys.ControlKey) {
            down = true;
        }
        else if (key.KeyCode === Keys.ShiftKey) {
            shift = true;
        }
        else if (key.KeyCode === Keys.Space) {
            up = true;
        }
        else if (key.KeyCode === Keys.E) {
            maxSpeed = true;
        }
    }
});
function acceptDialog(infoText, callFunction, callFunctionParams) {
    if (callFunctionParams === void 0) { callFunctionParams = undefined; }
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Accept action");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText(infoText);
    textElement.Color(255, 255, 255, 255);
    panel = drawMenu.createPanel(0, 12, 10, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 10, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = callFunction;
    if (callFunction !== undefined)
        panel.addFunctionArgs(callFunctionParams);
    textElement = panel.addText("Accept");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 10, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
var clanName;
var clanTag;
var languageVariable;
function clanCreatePanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Clans");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 5);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText("Create a new clan:");
    textElement.Color(255, 255, 255, 255);
    textElement = panel.addText("Clan name: ~c~(max 20 simbols)");
    textElement.Color(255, 255, 255, 255);
    clanName = panel.addInput(0, 2, 10, 1);
    panel.addText(" ");
    textElement = panel.addText("Clan tag: ~c~(max 5 simbols)");
    textElement.Color(255, 255, 255, 255);
    clanTag = panel.addInput(0, 4, 10, 1);
    panel = drawMenu.createPanel(0, 12, 13, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 13, 10, 1);
    panel.MainBackgroundColor(249, 168, 37, 160);
    panel.HoverBackgroundColor(249, 168, 37, 90);
    panel.Hoverable = true;
    panel.Function = changeLang;
    languageVariable = panel.addText("Input Language " + (language === 0 ? "~r~English" : "~w~Ru~b~ss~r~ia") + "  ~c~(click here to change)");
    languageVariable.Color(255, 255, 255, 255);
    languageVariable.HoverColor(0, 180, 255, 255);
    languageVariable.Centered = true;
    languageVariable.VerticalCentered = true;
    languageVariable.FontScale = 0.6;
    panel = drawMenu.createPanel(0, 12, 12, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = tryToCreateClan;
    textElement = panel.addText("Create");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 12, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function tryToCreateClan() {
    clanName.isError = false;
    clanTag.isError = false;
    if (clanName.Input.length === 0 || clanTag.Input.length === 0) {
        if (clanName.Input.length === 0)
            clanName.isError = true;
        if (clanTag.Input.length === 0)
            clanTag.isError = true;
        createNotification("Can`t be empty", 1500);
        return;
    }
    if (clanName.Input.length > 20) {
        createNotification("In Name you can use max 20 symbols", 1500);
        clanName.isError = true;
        return;
    }
    if (clanTag.Input.length > 5) {
        createNotification("In Tag you can use max 5 symbols", 1500);
        clanTag.isError = true;
        return;
    }
    API.triggerServerEvent("clanCreate", clanName.Input, clanTag.Input);
}
function changeLang() {
    drawMenu.setLanguage(language === 0 ? (1) : (0));
    languageVariable.Text = "Input Language " + (language === 0 ? "~r~English" : "~w~Ru~b~ss~r~ia") + " ~c~(click here to change)";
}
function menuClanInfoPanel(clanId, playerClanState) {
    if (playerClanState === void 0) { playerClanState = 1; }
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    var localPlayer = API.getLocalPlayer();
    var localPlayerName = API.getPlayerName(localPlayer);
    panel = drawMenu.createPanel(0, 6, 4, 8, 1);
    panel.MainBackgroundColor(92, 49, 4, 255);
    panel.Header = true;
    textElement = panel.addText("Clan Menu ~r~EvaDM");
    textElement.Color(206, 213, 206, 255);
    textElement.Centered = false;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.7;
    textElement.Font = 4;
    panel = drawMenu.createPanel(0, 14, 4, 1, 12);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel.Header = true;
    panel = drawMenu.createPanel(0, 15, 4, 12, 1);
    panel.MainBackgroundColor(0, 0, 0, 255);
    panel.Header = true;
    textElement = panel.addText("Members");
    textElement.Color(206, 213, 206, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.6;
    textElement.Font = 1;
    panel = drawMenu.createPanel(0, 6, 5, 8, 12);
    panel.MainBackgroundColor(206, 213, 206, 255);
    textElement = panel.addText("Name: " + API.getWorldSyncedData("Clan" + clanId + "_Name"));
    textElement.Centered = false;
    textElement.Font = 4;
    textElement.FontScale = 0.6;
    textElement.Color(83, 117, 40, 255);
    textElement = panel.addText("Tag: " + API.getWorldSyncedData("Clan" + clanId + "_Tag"));
    textElement.Centered = false;
    textElement.Font = 4;
    textElement.FontScale = 0.6;
    textElement.Color(83, 117, 40, 255);
    textElement = panel.addText("Leader: " + API.getWorldSyncedData("Clan" + clanId + "_Leader"));
    textElement.Centered = false;
    textElement.Font = 4;
    textElement.FontScale = 0.6;
    textElement.Color(83, 117, 40, 255);
    textElement = panel.addText("Members " + API.getWorldSyncedData("Clan" + clanId + "_Members") + "/10");
    textElement.Centered = false;
    textElement.Font = 4;
    textElement.FontScale = 0.6;
    textElement.Color(83, 117, 40, 255);
    if (clanId === API.getEntitySyncedData(localPlayer, "Clan")) {
        if (playerClanState > 1) {
            if (API.getWorldSyncedData("Clan" + clanId + "_Members") < 10) {
                panel = drawMenu.createPanel(0, 6, 14, 8, 1);
                panel.MainBackgroundColor(0, 0, 0, 230);
                panel.HoverBackgroundColor(0, 0, 0, 200);
                panel.Hoverable = true;
                panel.addText("Invite player");
                panel.Function = clanInvitePlayerPanel;
            }
            panel = drawMenu.createPanel(0, 6, 15, 4, 1);
            panel.MainBackgroundColor(0, 0, 0, 230);
            panel.HoverBackgroundColor(0, 0, 0, 200);
            panel.Hoverable = true;
            panel.addText("Change Clan Name");
            panel.Function = clanChangeNamePanel;
            panel = drawMenu.createPanel(0, 10, 15, 4, 1);
            panel.MainBackgroundColor(0, 0, 0, 230);
            panel.HoverBackgroundColor(0, 0, 0, 200);
            panel.Hoverable = true;
            panel.addText("Change Clan Tag");
            panel.Function = clanChangeTagPanel;
        }
        panel = drawMenu.createPanel(0, 6, 16, 8, 1);
        panel.MainBackgroundColor(0, 0, 0, 230);
        panel.HoverBackgroundColor(0, 0, 0, 200);
        panel.addText("Leave from the clan");
        panel.Hoverable = true;
        panel.Function = leaveFromClan;
        panel.addFunctionArgs((localPlayerName));
    }
    panel = drawMenu.createPanel(0, 15, 5, 12, 12);
    panel.MainBackgroundColor(206, 213, 206, 255);
    for (var i = 1; i <= 10; i++) {
        var playerCurrentName = API.getWorldSyncedData("Clan" + clanId + "_Member" + i);
        if (playerCurrentName === "None")
            continue;
        panel = drawMenu.createPanel(0, 15, 5 - 1 + i, 7, 1);
        panel.MainBackgroundColor(206, 213, 206, 255);
        textElement = panel.addText("" + playerCurrentName);
        textElement.Centered = false;
        textElement.Font = 4;
        textElement.FontScale = 0.8;
        textElement.Color(83, 117, 40, 255);
        if (clanId === API.getEntitySyncedData(localPlayer, "Clan") && playerClanState > 1 && localPlayerName !== playerCurrentName) {
            if (playerClanState > 999) {
                panel = drawMenu.createPanel(0, 22, 5 - 1 + i, 3, 1);
                panel.MainBackgroundColor(47, 76, 22, 255);
                panel.HoverBackgroundColor(47, 76, 22, 200);
                panel.Hoverable = true;
                panel.Function = makeManager;
                panel.addFunctionArgs((i));
                textElement = panel.addText("Make manager");
                textElement.Centered = true;
                textElement.Font = 4;
                textElement.FontScale = 0.6;
            }
            panel = drawMenu.createPanel(0, 25, 5 - 1 + i, 2, 1);
            panel.MainBackgroundColor(216, 67, 21, 255);
            panel.HoverBackgroundColor(216, 67, 21, 200);
            panel.Hoverable = true;
            panel.Function = excludeMember;
            panel.addFunctionArgs((i));
            textElement = panel.addText("Exclude");
            textElement.Centered = true;
            textElement.Font = 4;
            textElement.FontScale = 0.6;
        }
    }
    panel = drawMenu.createPanel(0, 14, 16, 12, 1);
    panel.MainBackgroundColor(206, 213, 206, 255);
    panel = drawMenu.createPanel(0, 14, 16, 13, 1);
    panel.MainBackgroundColor(216, 67, 21, 255);
    panel.HoverBackgroundColor(216, 67, 21, 200);
    panel.Hoverable = true;
    panel.Tooltip = "Close this dialog";
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Close this dialog");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    drawMenu.Ready = true;
}
function makeManager(memberid) {
    createNotification("TODO: Make manager (" + memberid + ")", 1500);
}
function excludeMember(memberid) {
    API.triggerServerEvent("exludeMemberFromClan", memberid);
}
function leaveFromClan() {
    API.triggerServerEvent("leaveFromClan");
}
var clanNewName;
var clanNewTag;
function clanChangeNamePanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Clans");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText("Change Clan Name:");
    textElement.Color(255, 255, 255, 255);
    textElement = panel.addText("Clan name: ~c~(max 20 simbols)");
    textElement.Color(255, 255, 255, 255);
    clanNewName = panel.addInput(0, 2, 10, 1);
    panel = drawMenu.createPanel(0, 12, 10, 10, 2);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 11, 10, 1);
    panel.MainBackgroundColor(249, 168, 37, 160);
    panel.HoverBackgroundColor(249, 168, 37, 90);
    panel.Hoverable = true;
    panel.Function = changeLang;
    languageVariable = panel.addText("Input Language " + (language === 0 ? "~r~English" : "~w~Ru~b~ss~r~ia") + "  ~c~(click here to change)");
    languageVariable.Color(255, 255, 255, 255);
    languageVariable.HoverColor(0, 180, 255, 255);
    languageVariable.Centered = true;
    languageVariable.VerticalCentered = true;
    languageVariable.FontScale = 0.6;
    panel = drawMenu.createPanel(0, 12, 10, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = tryToChangeClanName;
    textElement = panel.addText("Change");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 10, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function clanChangeTagPanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Clans");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText("Change Clan Tag:");
    textElement.Color(255, 255, 255, 255);
    textElement = panel.addText("Clan tag: ~c~(max 5 simbols)");
    textElement.Color(255, 255, 255, 255);
    clanNewTag = panel.addInput(0, 2, 10, 1);
    panel = drawMenu.createPanel(0, 12, 10, 10, 2);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 11, 10, 1);
    panel.MainBackgroundColor(249, 168, 37, 160);
    panel.HoverBackgroundColor(249, 168, 37, 90);
    panel.Hoverable = true;
    panel.Function = changeLang;
    languageVariable = panel.addText("Input Language " + (language === 0 ? "~r~English" : "~w~Ru~b~ss~r~ia") + "  ~c~(click here to change)");
    languageVariable.Color(255, 255, 255, 255);
    languageVariable.HoverColor(0, 180, 255, 255);
    languageVariable.Centered = true;
    languageVariable.VerticalCentered = true;
    languageVariable.FontScale = 0.6;
    panel = drawMenu.createPanel(0, 12, 10, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = tryToChangeClanTag;
    textElement = panel.addText("Change");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 10, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function tryToChangeClanName() {
    clanNewName.isError = false;
    if (clanNewName.Input.length === 0) {
        clanNewName.isError = true;
        createNotification("Can`t be empty", 1500);
        return;
    }
    if (clanNewName.Input.length > 20) {
        createNotification("In Name you can use max 20 symbols", 1500);
        clanNewName.isError = true;
        return;
    }
    API.triggerServerEvent("updateClanNameFromSettings", clanNewName.Input);
}
function tryToChangeClanTag() {
    clanNewTag.isError = false;
    if (clanNewTag.Input.length === 0) {
        clanNewTag.isError = true;
        createNotification("Can`t be empty", 1500);
        return;
    }
    if (clanNewTag.Input.length > 5) {
        createNotification("In Name you can use max 5 symbols", 1500);
        clanNewTag.isError = true;
        return;
    }
    API.triggerServerEvent("updateClanTagFromSettings", clanNewTag.Input);
}
var invitedPlayer;
function clanInvitePlayerPanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Clans | Invite player");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText("Input player name or id:");
    textElement.Color(255, 255, 255, 255);
    textElement = panel.addText("Player: ");
    textElement.Color(255, 255, 255, 255);
    invitedPlayer = panel.addInput(0, 2, 10, 1);
    panel = drawMenu.createPanel(0, 12, 10, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 10, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = tryToInvitePlayerInClan;
    textElement = panel.addText("Invite");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 10, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function tryToInvitePlayerInClan() {
    invitedPlayer.isError = false;
    if (invitedPlayer.Input.length === 0) {
        invitedPlayer.isError = true;
        createNotification("Can`t be empty", 1500);
        return;
    }
    API.triggerServerEvent("inviteMemberInClan", invitedPlayer.Input);
}
function acceptClanInviteDialog(clainId, clanName) {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    var message = "You get invite to clan " + clanName + ". Accept this invite?";
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Accept action");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText(message);
    textElement.Color(255, 255, 255, 255);
    panel = drawMenu.createPanel(0, 12, 10, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 230);
    panel = drawMenu.createPanel(0, 12, 10, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = acceptClanInvite;
    panel.addFunctionArgs((clainId));
    textElement = panel.addText("Accept");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    panel = drawMenu.createPanel(0, 17, 10, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Cancel");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function acceptClanInvite(clanId) {
    API.triggerServerEvent("acceptClanInvite", clanId);
}
function menuCommandsPanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 2, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Commands ~r~EvaDM");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 3, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    panel.addText(" › /rules - Server rules");
    panel.addText(" › /car - Get the random car ");
    panel.addText(" › /car Name - Get the car by its name ");
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("Okay");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    drawMenu.Blur = true;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
var loginPassword;
function menuLoginPanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Welcome to ~r~EvaDM");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 7, 10, 3);
    panel.MainBackgroundColor(0, 0, 0, 160);
    textElement = panel.addText("You already registered on the server. Please login.");
    textElement.Color(255, 255, 255, 255);
    textElement = panel.addText("Password:");
    textElement.Color(255, 255, 255, 255);
    loginPassword = panel.addInput(0, 2, 10, 1);
    loginPassword.Protected = true;
    loginPassword.Selected = true;
    panel = drawMenu.createPanel(0, 12, 10, 10, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = attemptToLogin;
    textElement = panel.addText("Login");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 1;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
function attemptToLogin() {
    var pass = loginPassword.Input;
    loginPassword.isError = false;
    if (pass.length < 5) {
        loginPassword.isError = true;
        loginPassword.Input = "";
        loginPassword.Selected = true;
        return;
    }
    else {
        API.triggerServerEvent("clientLogin", pass);
        return;
    }
}
function menuMainPanel() {
    drawMenu = createMenu(2);
    var panel;
    var textElement;
    var localPlayer = API.getLocalPlayer();
    var coordY = 15;
    for (var i = 0; i < 2; i++) {
        panel = drawMenu.createPanel(i, 24, coordY, 8, 1);
        panel.MainBackgroundColor(255, 255, 255, 235);
        panel = drawMenu.createPanel(i, 24, coordY, 2, 1);
        textElement = panel.addText("Menu");
        textElement.Centered = true;
        textElement.VerticalCentered = true;
        textElement.Color(0, 0, 0, 255);
        if (i === 0) {
            panel.MainBackgroundColor(205, 0, 0, 190);
            textElement.Color(255, 255, 255, 255);
        }
        else {
            panel.Hoverable = true;
            panel.Function = setPage;
            panel.addFunctionArgs((0));
            panel.MainBackgroundColor(255, 255, 255, 240);
            textElement.Color(0, 0, 0, 255);
            textElement.HoverColor(255, 255, 255, 255);
        }
        panel = drawMenu.createPanel(i, 26, coordY, 2, 1);
        textElement = panel.addText("Stats");
        textElement.Centered = true;
        textElement.VerticalCentered = true;
        if (i === 1) {
            textElement.Color(205, 0, 0, 255);
            textElement.Color(255, 255, 255, 255);
        }
        else {
            panel.Hoverable = true;
            panel.Function = setPage;
            panel.addFunctionArgs((1));
            panel.MainBackgroundColor(255, 255, 255, 240);
            textElement.Color(0, 0, 0, 255);
            textElement.HoverColor(255, 255, 255, 255);
        }
        panel = drawMenu.createPanel(i, 31, coordY, 1, 1);
        panel.MainBackgroundColor(205, 0, 0, 190);
        panel.HoverBackgroundColor(205, 0, 0, 140);
        panel.Hoverable = true;
        panel.Function = drawMenu.killMenu;
        textElement = panel.addText("X");
        textElement.Color(255, 255, 255, 255);
        textElement.HoverColor(0, 180, 255, 255);
        textElement.Centered = true;
        textElement.VerticalCentered = true;
        textElement.FontScale = 1;
    }
    panel = drawMenu.createPanel(0, 24, 8, 8, 7);
    panel.MainBackgroundColor(0, 0, 0, 235);
    textElement = panel.addText("Main menu");
    textElement.Centered = true;
    textElement.VerticalCentered = false;
    textElement.Font = 1;
    panel = drawMenu.createPanel(0, 24, 9, 8, 1);
    panel.MainBackgroundColor(206, 213, 206, 190);
    panel.HoverBackgroundColor(206, 213, 206, 160);
    panel.Hoverable = true;
    panel.Function = openClanMenu;
    textElement = panel.addText("Clan");
    textElement.Font = 4;
    panel = drawMenu.createPanel(0, 24, 10, 8, 1);
    panel.MainBackgroundColor(206, 213, 206, 190);
    panel.HoverBackgroundColor(206, 213, 206, 160);
    panel.Hoverable = true;
    textElement = panel.addText("Cars");
    textElement.Font = 4;
    panel = drawMenu.createPanel(0, 24, 11, 8, 1);
    panel.MainBackgroundColor(206, 213, 206, 190);
    panel.HoverBackgroundColor(206, 213, 206, 160);
    panel.Hoverable = true;
    textElement = panel.addText("Teleports");
    textElement.Font = 4;
    panel = drawMenu.createPanel(0, 24, 12, 8, 1);
    panel.MainBackgroundColor(206, 213, 206, 190);
    panel.HoverBackgroundColor(206, 213, 206, 160);
    panel.Hoverable = true;
    textElement = panel.addText("Shop");
    textElement.Font = 4;
    var yStart = 8;
    panel = drawMenu.createPanel(1, 24, yStart, 8, 7);
    panel.MainBackgroundColor(0, 0, 0, 235);
    textElement = panel.addText("Statistic");
    textElement.Centered = true;
    textElement.VerticalCentered = false;
    textElement.Font = 1;
    yStart++;
    panel = drawMenu.createPanel(1, 24, yStart, 4, 1);
    textElement = panel.addText("Money: ");
    textElement.Font = 4;
    panel = drawMenu.createPanel(1, 28, yStart, 4, 1);
    textElement = panel.addText("5000$");
    textElement.Font = 4;
    yStart++;
    panel = drawMenu.createPanel(1, 24, yStart, 4, 1);
    textElement = panel.addText("Kills/Deaths: ");
    textElement.Font = 4;
    var kills = API.getEntitySyncedData(localPlayer, "Kills");
    var death = API.getEntitySyncedData(localPlayer, "Deaths");
    var rang = API.getEntitySyncedData(localPlayer, "Rang");
    panel = drawMenu.createPanel(1, 28, yStart, 4, 1);
    textElement = panel.addText(kills + "/" + death + " ~c~(" + rang + ")");
    textElement.Font = 4;
    yStart++;
    panel = drawMenu.createPanel(1, 24, yStart, 4, 1);
    textElement = panel.addText("Admin level: ");
    textElement.Font = 4;
    var adminLevel = API.getEntitySyncedData(localPlayer, "Admin");
    var adminRang = API.getEntitySyncedData(localPlayer, "AdmRang");
    panel = drawMenu.createPanel(1, 28, yStart, 4, 1);
    textElement = panel.addText(adminLevel + " | " + adminRang);
    textElement.Font = 4;
    yStart++;
    panel = drawMenu.createPanel(1, 24, yStart, 4, 1);
    textElement = panel.addText("Clan: ");
    textElement.Font = 4;
    panel = drawMenu.createPanel(1, 28, yStart, 4, 1);
    textElement = panel.addText("" + API.getEntitySyncedData(localPlayer, "ClanName"));
    textElement.Font = 4;
    yStart++;
    panel = drawMenu.createPanel(1, 24, yStart, 4, 1);
    textElement = panel.addText("On Server: ");
    textElement.Font = 4;
    var timeOnServer = API.getEntitySyncedData(localPlayer, "OnServer");
    var hours = Math.floor(timeOnServer / 3600);
    var minutes = Math.floor(timeOnServer % 3600 / 60);
    var seconds = Math.floor(timeOnServer % 3600 % 60);
    if (hours < 10)
        hours = ("0" + hours);
    if (minutes < 10)
        minutes = ("0" + minutes);
    if (seconds < 10)
        seconds = ("0" + seconds);
    var timeOnServerFormat = hours + ":" + minutes + ":" + seconds;
    panel = drawMenu.createPanel(1, 28, yStart, 4, 1);
    textElement = panel.addText("" + timeOnServerFormat);
    textElement.Font = 4;
    drawMenu.DisableOverlaysWithHud(true);
    drawMenu.Ready = true;
}
function openClanMenu() {
    var localPlayer = API.getLocalPlayer();
    var playerClan = API.getEntitySyncedData(localPlayer, "Clan");
    var playerClanState = API.getEntitySyncedData(localPlayer, "ClanState");
    if (playerClan !== -1) {
        menuClanInfoPanel(playerClan, playerClanState);
    }
    else {
        clanCreatePanel();
    }
}
var regPassword;
var regConfirmPassword;
function menuRegisterPanel() {
    /*   drawMenu = createMenu(1);
       var panel;
       var textElement;
       panel = drawMenu.createPanel(0, 12, 6, 10, 1);
       panel.MainBackgroundColor(0, 0, 0, 200);
       panel.Header = true;
       textElement = panel.addText("Welcome to ~r~EvaDM");
       textElement.Color(255, 255, 255, 255);
       textElement.Centered = true;
       textElement.VerticalCentered = true;
       textElement.FontScale = 0.8;
       textElement.Offset = 18;
       panel = drawMenu.createPanel(0, 12, 7, 10, 5);
       panel.MainBackgroundColor(0, 0, 0, 160);
       textElement = panel.addText("You are not registered yet. Please ~r~register.");
       textElement.Color(255, 255, 255, 255);
       textElement = panel.addText("Password:");
       textElement.Color(255, 255, 255, 255);
       regPassword = panel.addInput(0, 2, 10, 1);
       regPassword.Protected = true;
       regPassword.Selected = true;
       panel.addText("");
       textElement = panel.addText("Password Again:");
       textElement.Color(255, 255, 255, 255);
       regConfirmPassword = panel.addInput(0, 4, 10, 1);
       regConfirmPassword.Protected = true;
       panel = drawMenu.createPanel(0, 12, 12, 10, 1);
       panel.MainBackgroundColor(46, 125, 50, 160);
       panel.HoverBackgroundColor(46, 125, 50, 90);
       panel.Hoverable = true;
       panel.Function = attemptToRegister;
       textElement = panel.addText("Register");
       textElement.Color(255, 255, 255, 255);
       textElement.HoverColor(0, 180, 255, 255);
       textElement.Centered = true;
       textElement.VerticalCentered = true;
       textElement.FontScale = 1;
       drawMenu.Blur = false;
       drawMenu.DisableOverlays(true);
       drawMenu.Ready = true;*/
}
function attemptToRegister() {
    var pass = regPassword.Input;
    var passVerify = regConfirmPassword.Input;
    if (pass.length < 5 && passVerify.length < 5) {
        regPassword.isError = true;
        regConfirmPassword.isError = true;
        regPassword.Input = "";
        regConfirmPassword.Input = "";
        regPassword.Selected = true;
        return;
    }
    regPassword.isError = false;
    regConfirmPassword.isError = false;
    if (pass === passVerify) {
        API.triggerServerEvent("clientRegistration", pass);
        drawMenu.killMenu();
        createNotification("Succesful registered. ~n~Main Menu: ~g~CTRL ~y~+ ~g~Z", 3500);
        return;
    }
    else {
        regPassword.isError = true;
        regConfirmPassword.isError = true;
        regPassword.Input = "";
        regConfirmPassword.Input = "";
        regPassword.Selected = true;
        return;
    }
}
function menuRulesPanel() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 2, 10, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Rules ~r~EvaDM");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 12, 3, 10, 10);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel = drawMenu.createPanel(0, 12, 3, 10, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.addText("Allowed: ");
    panel = drawMenu.createPanel(0, 12, 4, 10, 2);
    panel.MainBackgroundColor(255, 255, 255, 160);
    panel.addText(" • First ");
    panel.addText(" • Second ");
    panel = drawMenu.createPanel(0, 12, 6, 10, 1);
    panel.MainBackgroundColor(249, 168, 37, 160);
    panel.addText("Not recommended: ");
    panel = drawMenu.createPanel(0, 12, 7, 10, 2);
    panel.MainBackgroundColor(255, 255, 255, 160);
    panel.addText(" • First ");
    panel.addText(" • Second ");
    panel = drawMenu.createPanel(0, 12, 9, 10, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.addText("Not allowed: ");
    panel = drawMenu.createPanel(0, 12, 10, 10, 2);
    panel.MainBackgroundColor(255, 255, 255, 160);
    panel.addText(" • First ");
    panel.addText(" • Second ");
    panel = drawMenu.createPanel(0, 12, 12, 5, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.nextPage;
    textElement = panel.addText("Yes");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    panel = drawMenu.createPanel(0, 17, 12, 5, 1);
    panel.MainBackgroundColor(216, 67, 21, 160);
    panel.HoverBackgroundColor(216, 67, 21, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("No");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    drawMenu.Blur = true;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
API.onKeyDown.connect(function (sender, e) {
    if (e.Control && e.KeyCode === Keys.C) {
        menuSelectTeleportCategory();
    }
});
function menuSelectTeleportCategory() {
    drawMenu = createMenu(1);
    var panel;
    var textElement;
    panel = drawMenu.createPanel(0, 12, 6, 9, 1);
    panel.MainBackgroundColor(0, 0, 0, 200);
    panel.Header = true;
    textElement = panel.addText("Select TP Category");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = false;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    textElement.Offset = 18;
    panel = drawMenu.createPanel(0, 21, 6, 1, 1);
    panel.MainBackgroundColor(216, 67, 21, 200);
    panel.HoverBackgroundColor(216, 67, 21, 180);
    panel.Header = true;
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("X");
    textElement.Color(255, 255, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.8;
    panel = drawMenu.createPanel(0, 12, 7, 10, 6);
    panel.MainBackgroundColor(0, 0, 0, 160);
    var xStart = 12;
    var yStart = 7;
    for (var i = 0; i < 10; i++) {
        panel = drawMenu.createPanel(0, xStart, yStart, 5, 1);
        panel.MainBackgroundColor(0, 0, 0, 160);
        panel.Hoverable = true;
        panel.addText("" + i);
        yStart++;
        if (i !== 4)
            continue;
        xStart = 12 + 5;
        yStart = 7;
    }
    panel = drawMenu.createPanel(0, 12, 12, 10, 1);
    panel.MainBackgroundColor(46, 125, 50, 160);
    panel.HoverBackgroundColor(46, 125, 50, 90);
    panel.Hoverable = true;
    panel.Function = drawMenu.killMenu;
    textElement = panel.addText("+ Add new category");
    textElement.Color(255, 255, 255, 255);
    textElement.HoverColor(0, 180, 255, 255);
    textElement.Centered = true;
    textElement.VerticalCentered = true;
    textElement.FontScale = 0.7;
    drawMenu.Blur = false;
    drawMenu.DisableOverlays(true);
    drawMenu.Ready = true;
}
API.onKeyDown.connect(function (sender, e) {
    if (!isReady) {
        return;
    }
    if (e.KeyCode === Keys.Tab) {
        if (tabIndex[0].Selected) {
            tabIndex[0].Selected = false;
        }
        else {
            tabIndex[0].Selected = true;
            return;
        }
        var removeItem = tabIndex.shift();
        tabIndex.push(removeItem);
        tabIndex[0].Selected = true;
    }
    if (selectedInput === null) {
        return;
    }
    if (e.KeyCode === Keys.Back) {
        selectedInput.removeFromInput();
        return;
    }
    var keypress = "";
    var shiftOn = false;
    var altOn = false;
    if (e.Shift) {
        shiftOn = true;
    }
    if (e.Alt) {
        altOn = true;
    }
    if (language === 0) {
        switch (e.KeyCode) {
            case Keys.Enter:
                keypress = "\n";
                break;
            case Keys.Space:
                keypress = " ";
                break;
            case Keys.A:
                keypress = "a";
                if (shiftOn) {
                    keypress = "A";
                }
                break;
            case Keys.B:
                keypress = "b";
                if (shiftOn) {
                    keypress = "B";
                }
                break;
            case Keys.C:
                keypress = "c";
                if (shiftOn) {
                    keypress = "C";
                }
                break;
            case Keys.D:
                keypress = "d";
                if (shiftOn) {
                    keypress = "D";
                }
                break;
            case Keys.E:
                keypress = "e";
                if (shiftOn) {
                    keypress = "E";
                }
                break;
            case Keys.F:
                keypress = "f";
                if (shiftOn) {
                    keypress = "F";
                }
                break;
            case Keys.G:
                keypress = "g";
                if (shiftOn) {
                    keypress = "G";
                }
                break;
            case Keys.H:
                keypress = "h";
                if (shiftOn) {
                    keypress = "H";
                }
                break;
            case Keys.I:
                keypress = "i";
                if (shiftOn) {
                    keypress = "I";
                }
                break;
            case Keys.J:
                keypress = "j";
                if (shiftOn) {
                    keypress = "J";
                }
                break;
            case Keys.K:
                keypress = "k";
                if (shiftOn) {
                    keypress = "K";
                }
                break;
            case Keys.L:
                keypress = "l";
                if (shiftOn) {
                    keypress = "L";
                }
                break;
            case Keys.M:
                keypress = "m";
                if (shiftOn) {
                    keypress = "M";
                }
                break;
            case Keys.N:
                keypress = "n";
                if (shiftOn) {
                    keypress = "N";
                }
                break;
            case Keys.O:
                keypress = "o";
                if (shiftOn) {
                    keypress = "O";
                }
                break;
            case Keys.P:
                keypress = "p";
                if (shiftOn) {
                    keypress = "P";
                }
                break;
            case Keys.Q:
                keypress = "q";
                if (shiftOn) {
                    keypress = "Q";
                }
                break;
            case Keys.R:
                keypress = "r";
                if (shiftOn) {
                    keypress = "R";
                }
                break;
            case Keys.S:
                keypress = "s";
                if (shiftOn) {
                    keypress = "S";
                }
                break;
            case Keys.T:
                keypress = "t";
                if (shiftOn) {
                    keypress = "T";
                }
                break;
            case Keys.U:
                keypress = "u";
                if (shiftOn) {
                    keypress = "U";
                }
                break;
            case Keys.V:
                keypress = "v";
                if (shiftOn) {
                    keypress = "V";
                }
                break;
            case Keys.W:
                keypress = "w";
                if (shiftOn) {
                    keypress = "W";
                }
                break;
            case Keys.X:
                keypress = "x";
                if (shiftOn) {
                    keypress = "X";
                }
                break;
            case Keys.Y:
                keypress = "y";
                if (shiftOn) {
                    keypress = "Y";
                }
                break;
            case Keys.Z:
                keypress = "z";
                if (shiftOn) {
                    keypress = "Z";
                }
                break;
            case Keys.D0:
                keypress = "0";
                if (shiftOn) {
                    keypress = ")";
                }
                break;
            case Keys.D1:
                keypress = "1";
                if (shiftOn) {
                    keypress = "!";
                }
                break;
            case Keys.D2:
                keypress = "2";
                if (shiftOn) {
                    keypress = "@";
                }
                break;
            case Keys.D3:
                keypress = "3";
                if (shiftOn) {
                    keypress = "#";
                }
                break;
            case Keys.D4:
                keypress = "4";
                if (shiftOn) {
                    keypress = "$";
                }
                break;
            case Keys.D5:
                keypress = "5";
                if (shiftOn) {
                    keypress = "%";
                }
                break;
            case Keys.D6:
                keypress = "6";
                if (shiftOn) {
                    keypress = "^";
                }
                break;
            case Keys.D7:
                keypress = "7";
                if (shiftOn) {
                    keypress = "&";
                }
                break;
            case Keys.D8:
                keypress = "8";
                if (shiftOn) {
                    keypress = "*";
                }
                break;
            case Keys.D9:
                keypress = "9";
                if (shiftOn) {
                    keypress = "(";
                }
                break;
            case Keys.OemMinus:
                keypress = "-";
                if (shiftOn) {
                    keypress = "_";
                }
                break;
            case Keys.Oemplus:
                keypress = "=";
                if (shiftOn) {
                    keypress = "+";
                }
                break;
            case Keys.OemQuestion:
                keypress = "/";
                if (shiftOn) {
                    keypress = "?";
                }
                break;
            case Keys.Oemcomma:
                keypress = ",";
                if (shiftOn) {
                    keypress = "<";
                }
                break;
            case Keys.OemPeriod:
                keypress = ".";
                if (shiftOn) {
                    keypress = ">";
                }
                break;
            case Keys.OemSemicolon:
                keypress = ";";
                if (shiftOn) {
                    keypress = ":";
                }
                break;
            case Keys.OemOpenBrackets:
                keypress = "[";
                if (shiftOn) {
                    keypress = "{";
                }
                break;
            case Keys.OemCloseBrackets:
                keypress = "]";
                if (shiftOn) {
                    keypress = "}";
                }
                break;
            case Keys.NumPad0:
                keypress = "0";
                break;
            case Keys.NumPad1:
                keypress = "1";
                break;
            case Keys.NumPad2:
                keypress = "2";
                break;
            case Keys.NumPad3:
                keypress = "3";
                break;
            case Keys.NumPad4:
                keypress = "4";
                break;
            case Keys.NumPad5:
                keypress = "5";
                break;
            case Keys.NumPad6:
                keypress = "6";
                break;
            case Keys.NumPad7:
                keypress = "7";
                break;
            case Keys.NumPad8:
                keypress = "8";
                break;
            case Keys.NumPad9:
                keypress = "9";
                break;
        }
    }
    else if (language === 1) {
        switch (e.KeyCode) {
            case Keys.Enter:
                keypress = "\n";
                break;
            case Keys.Space:
                keypress = " ";
                break;
            case Keys.A:
                keypress = "ф";
                if (shiftOn) {
                    keypress = "Ф";
                }
                break;
            case Keys.B:
                keypress = "и";
                if (shiftOn) {
                    keypress = "И";
                }
                break;
            case Keys.C:
                keypress = "с";
                if (shiftOn) {
                    keypress = "С";
                }
                break;
            case Keys.D:
                keypress = "в";
                if (shiftOn) {
                    keypress = "В";
                }
                break;
            case Keys.E:
                keypress = "у";
                if (shiftOn) {
                    keypress = "У";
                }
                break;
            case Keys.F:
                keypress = "а";
                if (shiftOn) {
                    keypress = "А";
                }
                break;
            case Keys.G:
                keypress = "п";
                if (shiftOn) {
                    keypress = "П";
                }
                break;
            case Keys.H:
                keypress = "р";
                if (shiftOn) {
                    keypress = "Р";
                }
                break;
            case Keys.I:
                keypress = "ш";
                if (shiftOn) {
                    keypress = "Ш";
                }
                break;
            case Keys.J:
                keypress = "о";
                if (shiftOn) {
                    keypress = "О";
                }
                break;
            case Keys.K:
                keypress = "л";
                if (shiftOn) {
                    keypress = "Л";
                }
                break;
            case Keys.L:
                keypress = "д";
                if (shiftOn) {
                    keypress = "Д";
                }
                break;
            case Keys.M:
                keypress = "ь";
                if (shiftOn) {
                    keypress = "Ь";
                }
                break;
            case Keys.N:
                keypress = "т";
                if (shiftOn) {
                    keypress = "Т";
                }
                break;
            case Keys.O:
                keypress = "щ";
                if (shiftOn) {
                    keypress = "Щ";
                }
                break;
            case Keys.P:
                keypress = "з";
                if (shiftOn) {
                    keypress = "З";
                }
                break;
            case Keys.Q:
                keypress = "й";
                if (shiftOn) {
                    keypress = "Й";
                }
                break;
            case Keys.R:
                keypress = "к";
                if (shiftOn) {
                    keypress = "К";
                }
                break;
            case Keys.S:
                keypress = "ы";
                if (shiftOn) {
                    keypress = "Ы";
                }
                break;
            case Keys.T:
                keypress = "е";
                if (shiftOn) {
                    keypress = "Е";
                }
                break;
            case Keys.U:
                keypress = "г";
                if (shiftOn) {
                    keypress = "Г";
                }
                break;
            case Keys.V:
                keypress = "м";
                if (shiftOn) {
                    keypress = "М";
                }
                break;
            case Keys.W:
                keypress = "ц";
                if (shiftOn) {
                    keypress = "Ц";
                }
                break;
            case Keys.X:
                keypress = "ч";
                if (shiftOn) {
                    keypress = "Ч";
                }
                break;
            case Keys.Y:
                keypress = "н";
                if (shiftOn) {
                    keypress = "Н";
                }
                break;
            case Keys.Z:
                keypress = "я";
                if (shiftOn) {
                    keypress = "Я";
                }
                break;
            case Keys.D0:
                keypress = "0";
                if (shiftOn) {
                    keypress = ")";
                }
                break;
            case Keys.D1:
                keypress = "1";
                if (shiftOn) {
                    keypress = "!";
                }
                break;
            case Keys.D2:
                keypress = "2";
                if (altOn) {
                    keypress = "~";
                }
                break;
            case Keys.D3:
                keypress = '3';
                if (shiftOn) {
                    keypress = "№";
                }
                else if (altOn) {
                    keypress = "#";
                }
                break;
            case Keys.D4:
                keypress = "4";
                if (shiftOn) {
                    keypress = ";";
                }
                break;
            case Keys.D5:
                keypress = "5";
                if (shiftOn) {
                    keypress = "%";
                }
                break;
            case Keys.D6:
                keypress = "6";
                if (shiftOn) {
                    keypress = ":";
                }
                else if (altOn) {
                    keypress = "^";
                }
                break;
            case Keys.D7:
                keypress = "7";
                if (shiftOn) {
                    keypress = "?";
                }
                else if (altOn) {
                    keypress = "&";
                }
                break;
            case Keys.D8:
                keypress = "8";
                if (shiftOn) {
                    keypress = "*";
                }
                break;
            case Keys.D9:
                keypress = "9";
                if (shiftOn) {
                    keypress = "(";
                }
                break;
            case Keys.OemMinus:
                keypress = "-";
                if (shiftOn) {
                    keypress = "_";
                }
                break;
            case Keys.Oemplus:
                keypress = "=";
                if (shiftOn) {
                    keypress = "+";
                }
                break;
            case Keys.OemQuestion:
                keypress = ".";
                if (shiftOn) {
                    keypress = ",";
                }
                break;
            case Keys.Oemcomma:
                keypress = "б";
                if (shiftOn) {
                    keypress = "Б";
                }
                break;
            case Keys.OemPeriod:
                keypress = "ю";
                if (shiftOn) {
                    keypress = "Ю";
                }
                break;
            case Keys.OemSemicolon:
                keypress = "ж";
                if (shiftOn) {
                    keypress = "Ж";
                }
                break;
            case Keys.OemOpenBrackets:
                keypress = "х";
                if (shiftOn) {
                    keypress = "Х";
                }
                break;
            case Keys.OemCloseBrackets:
                keypress = "ъ";
                if (shiftOn) {
                    keypress = "Ъ";
                }
                if (shiftOn) {
                    keypress = "Ъ";
                }
                break;
            case Keys.Oem3:
                keypress = "ё";
                if (shiftOn) {
                    keypress = "Ё";
                }
                break;
            case Keys.Oem5:
                keypress = "*";
                if (shiftOn) {
                    keypress = "µ";
                }
                break;
            case Keys.Oem7:
                keypress = "э";
                if (shiftOn) {
                    keypress = "Э";
                }
                break;
            case Keys.Oem8:
                keypress = "!";
                if (shiftOn) {
                    keypress = "§";
                }
                break;
            case Keys.Oem102:
                keypress = "<";
                if (shiftOn) {
                    keypress = ">";
                }
                break;
            case Keys.NumPad0:
                keypress = "0";
                break;
            case Keys.NumPad1:
                keypress = "1";
                break;
            case Keys.NumPad2:
                keypress = "2";
                break;
            case Keys.NumPad3:
                keypress = "3";
                break;
            case Keys.NumPad4:
                keypress = "4";
                break;
            case Keys.NumPad5:
                keypress = "5";
                break;
            case Keys.NumPad6:
                keypress = "6";
                break;
            case Keys.NumPad7:
                keypress = "7";
                break;
            case Keys.NumPad8:
                keypress = "8";
                break;
            case Keys.NumPad9:
                keypress = "9";
                break;
        }
    }
    if (keypress === "") {
        return;
    }
    if (keypress.length > 0) {
        if (selectedInput === null) {
            return;
        }
        selectedInput.addToInput(keypress);
    }
    else {
        return;
    }
});
API.onUpdate.connect(function () {
    drawNotification();
    drawTextNotification();
    if (!isReady) {
        return;
    }
    for (var i = 0; i < menuElements[currentPage].length; i++) {
        if (!isReady) {
            break;
        }
        menuElements[currentPage][i].draw();
        menuElements[currentPage][i].isHovered();
        menuElements[currentPage][i].isClicked();
    }
});

//------------- Camera Events -------------

function interpolateCamera(args) {
    var cam = cameras[args[0]];
    var duration = args[1];
    var easepos = args[2];
    var easerot = args[3];
    if (API.getActiveCamera() !== cam) {
        interpolating = true;
        API.interpolateCameras(API.getActiveCamera(), cam, duration, easepos, easerot);
        API.sleep(args[1] + 100);
        interpolating = false;
        API.setActiveCamera(cam);
    }
};

function setCameraHeading(args) {
    var cam = args[0];
    var rot = args[1];
    API.setCameraRotation(cam, new Vector3(0, 0, rot));
};

function createCameraAtGamecam() {
    var pos = API.getGameplayCamPos();
    var rot = API.getGameplayCamRot();
    var cam = API.createCamera(pos, rot);
    cameras.push(cam);
    API.setActiveCamera(cam);
};

function createCameraInactive(args) {
    var pos = args[0];
    var rot = args[1];
    var cam = API.createCamera(pos, rot);
    cameras.push(cam);
};

function createCameraActive(args) {
    var pos = args[0];
    var rotation = args[1];
    var cam = API.createCamera(pos, rotation);
    cameras.push(cam);
    API.setActiveCamera(cam);
};

function pointCameraAtPosition(args) {
    var cam = cameras[args[0]];
    var target = args[1];
    API.pointCameraAtPosition(cam, target);
};

function pointCameraAtLocalPlayer(args) {
    var cam = cameras[args[0]];
    var player = API.getLocalPlayer();
    var offset = args[1];
    API.pointCameraAtEntity(cam, player, offset);
};

