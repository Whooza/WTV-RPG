var cef = null;

class CefHelper {
    constructor(resourcePath) {
        this.path = resourcePath
        this.open = false
    }

    show() {
        if (this.open === false) {
            this.open = true

            var resolution = API.getScreenResolution()

            this.browser = API.createCefBrowser(resolution.Width, resolution.Height, true)
            API.waitUntilCefBrowserInit(this.browser)
            API.setCefBrowserPosition(this.browser, 0, 0)
            API.loadPageCefBrowser(this.browser, this.path)
            API.showCursor(true)
        }
    }

    destroy() {
        this.open = false
        API.destroyCefBrowser(this.browser)
        API.showCursor(false)
    }

    eval(string) {
        this.browser.eval(string)
    }
}

API.onServerEventTrigger.connect(function (eventName, _arguments) {
    if (eventName === "showLoginPanel") {
        showLoginCef();
    }

    else if (eventName === "hideLoginPanel") {
        hideLoginCef();
    }

    else if (eventName === "showRegisterPanel") {
        showRegisterCef();
    }

    else if (eventName === "hideRegisterPanel") {
        hideRegisterCef();
    }
});

var registerCef = null;
var loginCef = null;

function showLoginCef() {
    if (loginCef == null) {
        loginCef = new CefHelper('Client/CefSites/html/login.html');
    }
    loginCef.show();
};

function hideLoginCef() {
    if (loginCef != null) {
        loginCef.destroy();
    }
};

function showRegisterCef() {

    if (registerCef == null) {
        registerCef = new CefHelper('Client/CefSites/html/register.html');
    }
    registerCef.show();
};

function hideRegisterCef() {
    if (registerCef != null) {
        registerCef.destroy();
    }
};

function login(pass) {
    API.triggerServerEvent("clientLogin", pass);
};