﻿
var visible = false;
var starvation = 0;
var thirst = 0;

API.onResourceStart.connect(function () {

});


// **** Server Communication ****

API.onServerEventTrigger.connect(function (eventName, args) {

    // data changed
    if (eventName == "UPDATE_STARVATION") {
        starvation = args[0];
        thirst = args[1];
    }

    // show hide display
    if (eventName == "SHOW_STARVATION") {
        visible = args[0];
        API.sendNotification("Show starvation : " + visible);
    }

}); 

// **** Key trigger ****

API.onKeyUp.connect(function (e, key) {
    if (key.KeyCode == Keys.P) {
        if (API.isChatOpen() || API.isAnyMenuOpen()) return;
        var isActive = API.isAIPedsSpawningActive();
        API.toggleAIPedsSpawning(!isActive);
    }
});

API.onUpdate.connect(function () {

    // API.drawText(string caption, double xPos, double yPos, double scale, int r, int g, int b, int alpha, int font, int justify, bool shadow, bool outline, int wordWrap);
    if (visible) {
        var resolution = API.getScreenResolutionMaintainRatio();
        var resX = resolution.Width;
        var resY = resolution.Height;
        var starvText = "Starvation : " + starvation; 
        var thirstText = "Thirst : " + thirst;
        API.drawText(starvText, resX - 25, 50, 0.5, 255, 255, 255, 255, 4, 2, false, true, 0);
        API.drawText(thirstText, resX - 25, 75, 0.5, 255, 255, 255, 255, 4, 2, false, true, 0);
    }
});
