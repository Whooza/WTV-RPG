/// <reference path="../types-gt-mp/index.d.ts" />

let cameras = [];
let interpolating = false;
API.onServerEventTrigger.connect((name, args) => {
    
    if (name === "createCameraActive") {
        let pos: Vector3 = args[0];
        let rotation: Vector3 = args[1];
        let cam = API.createCamera(pos, rotation);
        
        cameras.push(cam);
        API.setActiveCamera(cam);
    }
    else if (name === "createCameraAtGamecam") {
        let pos = API.getGameplayCamPos();
        let rot = API.getGameplayCamRot();
        let cam = API.createCamera(pos, rot);
        cameras.push(cam);
        API.setActiveCamera(cam);
    }

    else if (name === "createCameraInactive") {
        let pos: Vector3 = args[0];
        let rot: Vector3 = args[1];
        let cam = API.createCamera(pos, rot);
        cameras.push(cam);
    }

    else if (name === "setCameraFov") {
        API.setCameraFov(cameras[args[0]], args[1]);
    }

    else if (name === "pointCameraAtLocalPlayer") {
        let cam = cameras[args[0]];
        let player = API.getLocalPlayer();
        let offset: Vector3 = args[1];
        API.pointCameraAtEntity(cam, player, offset);
    }
    
    else if (name === "pointCameraAtPosition") {
        let cam = cameras[args[0]];
        let target: Vector3 = args[1];
        API.pointCameraAtPosition(cam, target);
    }

    else if (name === "setActiveCamera") {
        API.setActiveCamera(cameras[args[0]]);
    }

    else if (name === "interpolateCamera") {
        let cam = cameras[args[0]];
        let duration = args[1];
        let easepos = args[2];
        let easerot = args[3];
        if (API.getActiveCamera() !== cam) {
            interpolating = true;
            API.interpolateCameras(API.getActiveCamera(), cam, duration, easepos, easerot);
            API.sleep(args[1] + 100);
            interpolating = false;
            API.setActiveCamera(cam);
        }
    }

    else if (name === "clearCameras") {
        API.setActiveCamera(null);
        cameras = [];
    }

    else if (name === "setActiveCamera") {
        API.setActiveCamera(args[0]);
    }

    else if (name === "backToGamecam") {
        API.setActiveCamera(null);
    }
    else if (name === "setCameraHeading") {
        let cam = args[0];
        let rot = args[1];
        API.setCameraRotation(cam, new Vector3(0, 0, rot));
    }

});