﻿
let inventory_menu = null;
let item_menu = null;


// trigger inventory
API.onKeyDown.connect(function(Player, args) {
	
	// player pressed I button
    if (args.KeyCode == Keys.I && !API.isChatOpen())
    {
			
		// close inventory if it is open
		if(inventory_menu != null && inventory_menu.Visible &&  item_menu == null) {
			inventory_menu.Visible = false;
			destroyMenu();
			
		}
		
		// open inventory if it is not open
		else if(inventory_menu != null	&& inventory_menu.Visible == false  ) {
			createMenu();
		}

		// open inventory if its not open and does not exist.
		else if(inventory_menu == null ) {
    		createMenu();
		}
    }
	
	/*else if(args.KeyCode == Keys.Escape && !API.isChatOpen() ) {
		
		// close item menue
		if(item_menu != null && item_menu.Visible == true){
			destroyItemMenu();
		}

		// close inventory
		else if(inventory_menu != null && inventory_menu.Visible == true){
			destroyMenu();
		}

		
	}*/
});

// ------------- Server requests -------------

function requestInventory(){
	 API.triggerServerEvent("REQUEST_INVENTORY", null);
};

function requestItemActions(index) {
	 API.triggerServerEvent("REQUEST_ITEM_ACTION",  index);
	
};

function triggerItemAction( index){
	API.triggerServerEvent("REQUEST_ITEM_USE",  index);
};

// ------------- Server reply -------------

API.onServerEventTrigger.connect(function (name, args) {


	if(name == "REPLY_INVENTORY"){
		addPlayerItems(args);
		inventory_menu.Visible = true;

	}else if(name == "ITEM_ACTIONS"){
		
		destroyMenu();
		createItemMenu(args);
		
		item_menu.Visible = true;
	}
});

// ------------- Create Menus -------------

// Creates a new main inventory menu
// If a menu already exists, it will be destoryed.
function createMenu() {
	if(inventory_menu != null){
		destroyMenu();
	}

	inventory_menu = API.createMenu("Inventory", 0, 0 , 6);
	requestInventory();
};


function createItemMenu(args) {
	if(item_menu != null){
		destroyItemMenu();
	}

	item_menu = API.createMenu("use item...", 0,0,6);
	addItemActions(args);

};

// ------------- Create Menu Items -------------

// Adds items from lastInventory to the current menu
function addPlayerItems(lastInventory) {
	
	 for (var i = 0; i < lastInventory[0]; i++) {
		var item = API.createMenuItem(lastInventory[1][i], "");
		var index = i;
		item.Activated.connect(function (menu, item) {
			API.sendChatMessage("Menu item clicked! " + index);
			// request actions for this item
			requestItemActions(index);

		});

		inventory_menu.AddItem(item);
	
	 }
};

// Adds actions for current selected item
function addItemActions(itemActions){
	 for (var i = 0; i < itemActions[0]; i++) {
		var item = API.createMenuItem(itemActions[1][i], "");
		var index = i;
		item.Activated.connect(function (menu, item) {
			API.sendChatMessage("Item action clicked! " + index);
			// send action to server
			triggerItemAction(index);

		});

		//item.OnItemSelect.connect(OnItemActionSelected(itemActions[1][i], item, i));
		item_menu.AddItem(item);
	 }
};

// ------------- Destroy menus -------------

function destroyMenu() {
	if(inventory_menu !== null)
		inventory_menu.Visible = false;
        
	//disableAllControls = false;

	inventory_menu = null;

	return true;
};

function destroyItemMenu() {
	if(item_menu !== null)
		item_menu.Visible = false;
        
	//disableAllControls = false;

	item_menu = null;
	requestInventory();

	return true;
};
