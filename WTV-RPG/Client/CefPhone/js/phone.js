API.onResourceStart.connect(function()
{
    var res = API.getScreenResolution();
    myBrowser = API.createCefBrowser(res.Width, res.Height);
    API.waitUntilCefBrowserInit(myBrowser);
	API.setCefBrowserPosition(myBrowser, (res.Width / 1.5), (res.Height / 1.65));
	// API.setCefBrowserPosition(myBrowser, 0, 0);
    API.loadPageCefBrowser(myBrowser, "html/mainmenu.html");
	// API.setCefBrowserHeadless(myBrowser, true);
    API.showCursor(true);
    API.setCanOpenChat(false);
});