﻿
// Menus
var inventoryMenu = null;
var itemMenu = null;
var inventoryData = [];
var selectedItemIndex = -1;
// **** Resource start ****

API.onResourceStart.connect(function () {

    // +++ create menus +++
    // main menu
    inventoryMenu = API.createMenu("Inventory", "Select an item.", 0, 0, 6);
    itemMenu = API.createMenu("ItemName", "Select an action.", 0, 0, 6);

    // set item menu as child of inventoryMenu
    itemMenu.ParentMenu = inventoryMenu;

    // item selected
    inventoryMenu.OnItemSelect.connect(function (menu, item, index) {
        // update actions menu
        API.sendNotification("inventory Item selected : " + index);
        itemMenu.Clear();
        var inventoryItem = inventoryData[index];

        if (inventoryItem != null) {
            selectedItemIndex = index;
            API.setMenuTitle(itemMenu, inventoryItem.Text);
            // API.sendNotification("actions lenght is : " + inventoryItem.Actions.length);

            for (var i = 0; i < inventoryItem.Actions.length; i++) {
                var action = inventoryItem.Actions[i];
                itemMenu.AddItem(API.createMenuItem(action, action + " " + inventoryItem.Text));
            }

            //itemMenu.AddItem(API.createListItem("Use item", "", inventoryItem.Actions, 0));

            // show item menu and hide inventory
            inventoryMenu.Visible = false;
            itemMenu.Visible = true;
        } else {
            API.sendNotification("Item was null for index " + index);
        }

    });

    // action selected
    itemMenu.OnItemSelect.connect(function (menu, item, index) {
        //TODO Trigger server event
        API.sendNotification("action Item selected : " + index + " selected index is " + selectedItemIndex);
        if (selectedItemIndex >= 0) {
            var item = inventoryData[selectedItemIndex];
            var action = item.Actions[index];

            API.triggerServerEvent("REQUEST_ITEM_USE", item.Id, action)
            selectedItemIndex = -1;
        }
    });

});


// **** Server Communication ****

API.onServerEventTrigger.connect(function (eventName, args) {

    if (eventName == "REPLY_INVENTORY") {
        // set inventory data
        setInventoryData(args);

    }
});

// **** Key trigger ****

API.onKeyUp.connect(function (e, key) {
    if (key.KeyCode == Keys.I) {
        if (API.isChatOpen() || API.isAnyMenuOpen()) return;
        API.triggerServerEvent("REQUEST_INVENTORY");
    }
});

// **** Set Inventory data ****

function setInventoryData(args) {
    inventoryMenu.Clear();
    inventoryData = JSON.parse(args[0]);
    for (var i = 0; i < inventoryData.length; i++) {

        // var name = inventoryData[i].Ammount > 1 ? inventoryData[i].Name + "[" + inventoryData[i].Ammount + "]" : inventoryData[i].Name;
        var temp_item = API.createMenuItem(inventoryData[i].Text, inventoryData[i].Description);
        if (inventoryData[i].Ammount > 1) {
            temp_item.SetRightLabel("" + inventoryData[i].Ammount);
        }

        inventoryMenu.AddItem(temp_item);
    }

    selectedIdx = -1;
    itemMenu.Visible = false;
    inventoryMenu.Visible = (inventoryData.length > 0);

    if (inventoryData.length < 1) {
        API.callNative("_SET_NOTIFICATION_BACKGROUND_COLOR", 11);
        API.sendNotification("Your inventory is empty.");
    }
};
