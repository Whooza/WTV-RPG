﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WTV_RPG.Controller;
using WTV_RPG.Logging;
using WTV_RPG.Model;
using WTV_RPG.Util.Extensions;

namespace WTV_RPG.Handler
{
    internal static class InventoryDataHandler
    {
        internal static readonly string REQUEST_INVENTORY = "REQUEST_INVENTORY";
        internal static readonly string REQUEST_ITEM_ACTIONS = "REQUEST_ITEM_ACTION";
        internal static readonly string REQUAST_ITEM_USE = "REQUEST_ITEM_USE";

        internal static readonly string REPLY_INVENTORY = "REPLY_INVENTORY";
        internal static readonly string REPLY_ITEM_ACTIONS = "ITEM_ACTIONS";

        /// <summary>
        /// Called by the client to request the current inventory.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="arguments"></param>
        /// <param name="callingScript"></param>
        internal static void OnClientRequestedInventory(Client player, object[] arguments, Script callingScript)
        {
            Debug.Log("Inventory was requested");
            PushInventoryToClient(player, callingScript);

        }

        /// <summary>
        /// Called by the client when an item is selected
        /// </summary>
        /// <param name="player"></param>
        /// <param name="arguments"></param>
        /// <param name="callingScript"></param>
        internal static void OnClientSelectedItem(Client player, object[] arguments, Script callingScript)
        {
            int selectedIndex = Convert.ToInt32(arguments[0].ToString());
            Debug.Log("Client " + player.socialClubName + " selected item " + selectedIndex);

            var selectedItem = GetSelectedItem(player, selectedIndex);
            if (selectedItem != null)
            {
                var actionList = GetPossibleActionsForInventoryItem(selectedItem);
                if (actionList.Count > 0)
                {
                    callingScript.API.triggerClientEvent(player, REPLY_ITEM_ACTIONS, actionList.Count, actionList.ToArray());
                    Debug.Log("Replied to client with actionList size : " + actionList.Count);
                }
            }
        }

        /// <summary>
        /// Called by client when an item action is triggered for an item.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="arguments"></param>
        /// <param name="callingScript"></param>
        internal static void OnItemActionTriggered(Client player, object[] arguments, Script callingScript)
        {
            int itemId = Convert.ToInt32(arguments[0]);
            string action = Convert.ToString(arguments[1]);

            var item = ItemController.GetDataById(itemId);

            if (item != null)
            {
                Debug.Log(action + " was triggered for item Id " + itemId);

                // check if player has the item in his inventory
                var playerData = player.GetPlayerDataForClient();
                if (playerData.InventoryContainsItem(itemId))
                {
                    playerData.OnPlayerInventoryItemAction(itemId, action);

                }
                else
                {
                    // Player trying to use an item that is not in his inventory.
                    Debug.Log("Player does not have that item in his inventory");
                }
                             
            }
            // for now just reply the current inventory
            PushInventoryToClient(player, callingScript);
        }

        private static InventoryItem GetSelectedItem(Client player, int index)
        {
            var playerData = player.getData("PlayerData") as PlayerData;
            Debug.Log("Current inventory size : " + playerData.Inventory.Count);
            if (playerData != null && index < playerData.Inventory.Count)
            {
                return playerData.Inventory[index];
            }


            return null;
        }

        internal static List<string> GetPossibleActionsForInventoryItem(InventoryItem item)
        {
            var actions = new List<string>();

            switch (item.ItemType)
            {
                case ItemType.Food:
                    actions.AddRange(new List<string> { "eat", "drop" });
                    break;
                case ItemType.Drink:
                    actions.AddRange(new List<string> { "eat", "drink" });
                    break;
                default:
                    break;
            }

            return actions;
        }

        private static void PushInventoryToClient(Client player, Script callingScript)
        {
            var playerData = player.getData("PlayerData") as PlayerData;
            if (playerData == null)
            {
                Debug.Log("Error getting playerData in ClientHandler");
                return;
            }

            List<ClientUiItem> itemsInInventory = new List<ClientUiItem>();

            for (int i = 0; i < playerData.Inventory.Count; i++)
            {
                var item = playerData.Inventory[i];
                var actions = GetPossibleActionsForInventoryItem(item);
                ClientUiItem clientItem = new ClientUiItem()
                {
                    Id = item.ItemId,
                    Index = i,
                    Ammount = item.Ammount,
                    Description = item.Item.Description,
                    Text = item.Item.Name,
                    Actions = actions
                };
                itemsInInventory.Add(clientItem);
            }

            var inventoryString = JsonConvert.SerializeObject(itemsInInventory, Formatting.Indented);
            callingScript.API.triggerClientEvent(player, REPLY_INVENTORY, inventoryString);

          //  Debug.Log("Inventory replied : " + inventoryString);
        }

    }
}
