﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using WTV_RPG.Controller;
using WTV_RPG.Events;
using WTV_RPG.Logging;
using WTV_RPG.Model;
using WTV_RPG.Util.Extensions;

namespace WTV_RPG.Handler
{
    internal static class LoginHandler
    {
        /// <summary>
        /// Handles the Loing of a user.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="arguments"></param>
        /// <param name="callingScript"></param>
        internal static void HandleLogin(Client player, object[] arguments, Script callingScript)
        {
            Debug.Log("Login Handler Called");
            var password = arguments[0].ToString();
            Account account = AccountController.GetData(player);

            if (account != null)
            {

                // Debug.Log("deserialized account id: " + account.Id);
                // Debug.Log("Account is null ? " + (account == null));

                if (Util.Functions.VerifyMd5Hash(password, account.Password))
                {
                    Camera.ClearCameras(player);

                    Util.Functions.RandomSpawn(player);
                    player.setSkin(Util.Functions.RandomSkin());

                    player.freezePosition = false;
                    player.freeze(false);
                    player.transparency = 255;
                    StartClient(player, account, callingScript);
                }
                else
                {
                    Debug.Log("Wrong Password");
                    UiController.DrawNotification(player, "Wrong password", 2500);
                }
            }
            else
            {
                Debug.Log("Login Error");
                UiController.DrawNotification(player, "Login Error.", 2500);
            }
        }

        /// <summary>
        /// Handles the Registration of a user.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="arguments"></param>
        /// <param name="callingScript"></param>
        internal static void HandleRegistration(Client player, object[] arguments, Script callingScript)
        {
            Camera.ClearCameras(player);

            Util.Functions.RandomSpawn(player);
            player.setSkin(Util.Functions.RandomSkin());

            player.freezePosition = false;
            player.freeze(false);
            player.transparency = 255;


            if (AccountController.RegisterAccount(player, arguments[0].ToString()))
            {
                // account was registered
                Account account = AccountController.GetData(player);
                StartClient(player, account, callingScript);
            }
            else
            {
                UiController.DrawNotification(player, "Error durig registration. Please try again.", 2500);
            }
        }

        internal static void StartClient(Client player, Account account, Script callingScript)
        {
            Debug.Log("Starting player " + account.SocialClubName);

            player.UpdateAccountForClient(account, true);

            // fade screen and reset menus
            callingScript.API.triggerClientEvent(player, "killDrawMenus");
            callingScript.API.triggerClientEvent(player, "hideLoginPanel");
            callingScript.API.triggerClientEvent(player, "hideRegisterPanel");

            callingScript.API.sendNativeToPlayer(player, Hash.DO_SCREEN_FADE_IN, 4000);

            // start player updates
            PlayerControllerScript.Instance.AddPlayerToUpdate(player);
            UiController.ShowHideStarvation(player, true);
            UiController.ShowHideHealthBar(player, true);
        }
    }
}
