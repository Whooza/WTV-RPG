﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using WTV_RPG.Controller;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Util.Extensions
{
    internal static class ClientExtensions
    {
        #region Get data from client object

        internal static Account GetAccountForClient(this Client client)
        {
            return client.getData("Account");
        }

        internal static PlayerData GetPlayerDataForClient(this Client client)
        {
            return client.getData("PlayerData");
        }

        internal static HealthStatus GetHealthStatusForClient(this Client client)
        {
            return client.getData("HealthStatus");
        }

        #endregion

        #region Set AccountData to client object

        internal static void UpdateAccountForClient(this Client client, Account account, bool complete = false)
        {
            client.setData("Account", account);
            client.name = account.UserName;
            client.UpdateSyncedAccountData();

            if (complete)
            {
                client.UpdateClientPlayerData();
                client.UpdateClientHealthStatus();
            }

            account.LoggedIn = true;
        }

        #endregion

        /// <summary>
        /// Called by server in constant intervall to update synced data.
        /// </summary>
        internal static void UpdateSyncedAccountData(this Client player)
        {
            var account = AccountController.GetData(player);
            if (account == null) return;

            player.setSyncedData("SocialClubName", player.socialClubName);
            player.setSyncedData("RegIp", account.RegIp);
            player.setSyncedData("Admin", account.Admin);
            player.setSyncedData("Kills", account.Kills);
            player.setSyncedData("Deaths", account.Deaths);
            player.setSyncedData("Clan", account.Clan);
            player.setSyncedData("ClanState", account.ClanState);
            player.setSyncedData("OnServer", account.OnServer);

            player.setSyncedData("Rang", Functions.GetPlayerRangByKills(account.Kills));
            player.setSyncedData("AdmRang", Functions.GetPlayerAdmName(account.Admin));
            player.setSyncedData("ClanName", ClanController.GetClanNameById(account.Clan));
        }

        #region Set data to client from database or create it

        internal static void UpdateClientPlayerData(this Client player)
        {
            player.setData("PlayerData", PlayerDataController.GetPlayerData(player, true));
            Debug.Log("Set PlayerData to Client");
        }

        internal static void UpdateClientHealthStatus(this Client player)
        {
            player.setData("HealthStatus", HealthStatusController.GetHealthStatus(player, true));
            Debug.Log("Set HealthStatus to Client");
        }

        internal static void ResetHealthStatus(this Client player)
        {
            var healtStatus = player.GetHealthStatusForClient();
            healtStatus.Starvation = 0f;
            healtStatus.Thirst = 0f;
            player.setData("HealthStatus", healtStatus);
        }

        #endregion

        #region Call delegate in future

        internal static void ExecuteInFuture(this Client player, int secondsDelay, Action action, Script executingScript, bool repeat = false)
        {


            executingScript.API.delay(secondsDelay * 1000, !repeat, action);
        }

        #endregion
    }
}
