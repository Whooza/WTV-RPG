﻿using System;
using System.Security.Cryptography;
using System.Text;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using WTV_RPG.Controller;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;

namespace WTV_RPG.Util
{
    class Functions
    {
        internal static string GetMd5Hash(string input)
        {
            var data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public static bool VerifyMd5Hash(string input, string hash) =>
            0 == StringComparer.OrdinalIgnoreCase.Compare(GetMd5Hash(input), hash);

        internal static void RandomSpawn(Client player)
        {
            if (ServerData.Spawns.Count == 0)
            {
                Debug.Log("Spawns is empty ");
                return;
            }

            var currentSpawn = ServerData.Spawns[ServerData.Random.Next(ServerData.Spawns.Count)];



            if (currentSpawn == null)
            {
                Debug.Log("Is currentSpawn null ");
                return;
            }

            player.position = currentSpawn.Position;
            player.rotation = currentSpawn.Angel;

            //  AccountController.GivePlayerWeapons(player);
        }
        
        internal static Vector3 StringToVector(string vectorString)
        {
            var temp = vectorString.Split('/');
            return new Vector3(Convert.ToSingle(temp[0]), Convert.ToSingle(temp[1]), Convert.ToSingle(temp[2]));
        }

        internal static PedHash RandomSkin()
        {
            var values = Enum.GetValues(typeof(PedHash));
            var random = ServerData.Random.Next(values.Length);
            var returnskin = (PedHash)values.GetValue(random);
            if (returnskin == PedHash.BradCadaverCutscene || returnskin == PedHash.ChickenHawk ||
                returnskin == PedHash.Chop || returnskin == PedHash.Cormorant || returnskin == PedHash.Cow ||
                returnskin == PedHash.Coyote || returnskin == PedHash.Crow || returnskin == PedHash.Deer ||
                returnskin == PedHash.Dolphin || returnskin == PedHash.Fish || returnskin == PedHash.HammerShark ||
                returnskin == PedHash.TigerShark || returnskin == PedHash.Hen || returnskin == PedHash.Humpback ||
                returnskin == PedHash.Husky || returnskin == PedHash.KillerWhale || returnskin == PedHash.Marston01 ||
                returnskin == PedHash.MountainLion || returnskin == PedHash.MovAlien01 ||
                returnskin == PedHash.Niko01 || returnskin == PedHash.Orleans || returnskin == PedHash.Orleans ||
                returnskin == PedHash.Pig || returnskin == PedHash.Pigeon || returnskin == PedHash.Poodle ||
                returnskin == PedHash.Pug || returnskin == PedHash.Rabbit || returnskin == PedHash.Rat ||
                returnskin == PedHash.Retriever || returnskin == PedHash.Rhesus || returnskin == PedHash.Rottweiler ||
                returnskin == PedHash.Seagull || returnskin == PedHash.Shepherd || returnskin == PedHash.Stingray)
            {
                RandomSkin(); //recution, but....
                return 0;
            }
            return returnskin;
        }

        public static string GetPlayerRangByKills(int kills)
        {
            var rang = "N/A";
            if (0 <= kills & kills < 49) rang = "Noob";
            if (50 <= kills & kills < 99) rang = "Emigrant";
            if (100 <= kills & kills < 499) rang = "Bandit";
            if (500 <= kills & kills < 999) rang = "Killer";
            if (1000 <= kills & kills < 1499) rang = "Gangster";
            if (1500 <= kills & kills < 1999) rang = "Hitman";
            if (2000 <= kills & kills < 2999) rang = "Professional";
            if (3000 <= kills & kills < 3999) rang = "Terminator";
            if (4000 <= kills & kills < 4999) rang = "Maniac";
            if (5000 <= kills & kills < 8499) rang = "Baron";
            if (8500 <= kills & kills < 9999) rang = "Leader";
            if (10000 <= kills) rang = "Legend";
            return rang;
        }

        public static string GetPlayerAdmName(int level)
        {
            switch (level)
            {
                case 1: return "~g~Moder 1 lvl~s~";
                case 2: return "~o~Moder 2 lvl~s~";
                case 3: return "~p~Moder 3 lvl~s~";
                case 4: return "~r~Admin~w~";
                default: return "Not admin";
            }
        }

    }
}


