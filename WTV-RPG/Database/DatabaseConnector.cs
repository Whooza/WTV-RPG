﻿using System.Data;
using MySql.Data.MySqlClient;
using WTV_RPG.Logging;

namespace WTV_RPG
{
    public static class DatabaseConnector
    {
        public static MySqlConnectionStringBuilder MyConnectionString = new MySqlConnectionStringBuilder
        {
            Server = Config.DbServer,
            Port = Config.DbPort,
            UserID = Config.DbUser,
            Password = Config.DbPass,
            Database = Config.DbName
        };

        public static MySqlConnection Connection;
        public static MySqlCommand Command;
        public static MySqlDataReader Reader;
        
        
        public static void Init()
        {
            Debug.Log("Try to connecting the MySQL Database");
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();
                    Debug.Log("Mysql Database connected");
                }
                catch (MySqlException ex)
                {
                    MySqlEror(ex);
                }
            }

            Connection.Close();
        }

        public static long Query(string query)
        {
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();

                    Command = Connection.CreateCommand();
                    Command.CommandText = query;
                    Command.ExecuteNonQuery();

                    Connection.Close();

                    return Command.LastInsertedId;
                }
                catch (MySqlException e)
                {
                    MySqlEror(e);
                    return -1;
                }
            }
        }

        public static DataTable QueryResult(string query)
        {
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();
                    Command = Connection.CreateCommand();
                    Command.CommandText = query;
                    Reader = Command.ExecuteReader();
                    if (!Reader.HasRows)
                    {
                        Reader.Close();
                        Connection.Close();
                        return null;
                    }
                    var reslut = new DataTable();
                    reslut.Load(Reader);
                    Reader.Close();

                    Connection.Close();

                    return reslut;
                }
                catch (MySqlException e)
                {
                    MySqlEror(e);
                    return null;
                }
            }
        }
/*        
        public static long Insert(string table, string column, string value)
        {
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();
                    
                    var query = "INSERT INTO " + table + " (" + column + ") VALUES (" + value + ")";

                    Command = Connection.CreateCommand();
                    Command.CommandText = query;
                    Command.ExecuteNonQuery();

                    Connection.Close();

                    return Command.LastInsertedId;
                }
                catch (MySqlException e)
                {
                    MySqlEror(e);
                    return -1;
                }
            }
        }
        
        public static long Update(string table, string set, string where)
        {
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();
                    
                    var query = "UPDATE " + table + " SET " + set + " WHERE " + where + "";

                    Command = Connection.CreateCommand();
                    Command.CommandText = query;
                    Command.ExecuteNonQuery();

                    Connection.Close();

                    return Command.LastInsertedId;
                }
                catch (MySqlException e)
                {
                    MySqlEror(e);
                    return -1;
                }
            }
        }
        
        public static long Delete(string table, string where)
        {
            using (Connection = new MySqlConnection(MyConnectionString.ToString()))
            {
                try
                {
                    Connection.Open();
                    
                    var query = "DELETE FROM " + table + " WHERE " + where + "";

                    Command = Connection.CreateCommand();
                    Command.CommandText = query;
                    Command.ExecuteNonQuery();

                    Connection.Close();

                    return Command.LastInsertedId;
                }
                catch (MySqlException e)
                {
                    MySqlEror(e);
                    return -1;
                }
            }
        }
*/
        private static void MySqlEror(MySqlException e)
        {
            Debug.Log("› Mysql error: (#" + e.Number + ") " + e.Message);
        }
    }
}