﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WTV_RPG.Model
{
    internal class Clan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string Leader { get; set; }
        public string Message { get; set; }
        public string Color { get; set; }

        public string Member1 { get; set; }
        public string Member2 { get; set; }
        public string Member3 { get; set; }
        public string Member4 { get; set; }
        public string Member5 { get; set; }
        public string Member6 { get; set; }
        public string Member7 { get; set; }
        public string Member8 { get; set; }
        public string Member9 { get; set; }
        public string Member10 { get; set; }

        public static int ClanMember = 1;
        public static int ClanMeneger = 2;
        public static int ClanLeader = 3;
    }
}
