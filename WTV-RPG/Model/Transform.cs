﻿using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTV_RPG.Model
{
    class Transform
    {
        public Vector3 Position { get; set; }
        public Vector3 Angel { get; set; }

        public Transform(string position, string angel)
        {
            Position = Util.Functions.StringToVector(position);
            Angel = Util.Functions.StringToVector(angel);
        }
    }
}
