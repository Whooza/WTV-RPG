﻿
using System.Collections.Generic;

namespace WTV_RPG.Model
{
    public class ClientUiItem
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int Index { get; set; }
        public int Ammount { get; set; }
        public List<string> Actions { get; set; }

    }
}
