﻿

namespace WTV_RPG.Model
{
    internal class Account
    {
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public string SocialClubName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RegIp { get; set; }
        public string LastIp { get; set; }
        public int Admin { get; set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public string Weapons { get; set; }
        public int Clan { get; set; }
        public int ClanState { get; set; }
        public int OnServer { get; set; }
        public bool LoggedIn { get; set; }

    }
}
