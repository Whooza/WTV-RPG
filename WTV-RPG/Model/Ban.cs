﻿using System;

namespace WTV_RPG.Model
{
    internal class Ban
    {
        public enum BanType
        {
            Name,
            SocialClubName,
            IP,
            HWID
        }


        public Ban() { }

        public int Id { get; set; } = -1;
        public int Type { get; set; }
        public string Value { get; set; }
        public string Reason { get; set; }
        public DateTime? EndDate { get; set; }


    }
}

