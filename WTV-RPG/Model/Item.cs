﻿
namespace WTV_RPG.Model
{
    public enum ItemType { Food, Drink }
    class Item
    {
        public int Id { get; set; }
        public ItemType ItemType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
