﻿using System.Collections.Generic;
using System.Linq;
using WTV_RPG.Handler;
using WTV_RPG.Logging;

namespace WTV_RPG.Model
{
    class PlayerData
    {
        public int Id { get; set; }
        public string Weapon { get; set; }
        public List<InventoryItem> Inventory { get; set; }
        public int AccountId { get; set; }

        /// <summary>
        /// Adds item to inventory.
        /// </summary>
        /// <param name="item">the item to add.</param>
        /// <param name="ammount">how many of this items should be added?</param>
        internal void AddItemToInventory(Item item, int ammount)
        {
            // check if this inventory already contains items of this type.
            var foundInventoryItem = Inventory.FirstOrDefault(i => i.ItemId == item.Id);
            if (foundInventoryItem == null)
            {
                foundInventoryItem = new InventoryItem
                {
                    Ammount = ammount,
                    ItemId = item.Id,
                    ItemType = item.ItemType
                };

                Inventory.Add(foundInventoryItem);
                return;
            }

            foundInventoryItem.Ammount += ammount;
        }

        internal bool InventoryContainsItem(int itemId)
        {
            var item = GetInventoryItemById(itemId);
            return item != null;
        }

        internal bool InvenotryContainsItems(int itemId, int ammount)
        {
            var item = GetInventoryItemById(itemId);
            return item != null && item.Ammount >= ammount;
        }

        /// <summary>
        /// Called when an action is triggered by the player for an inventory item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="action"></param>
        internal void OnPlayerInventoryItemAction(int itemId, string action)
        {
            var item = GetInventoryItemById(itemId);

            if (item == null)
            {
                Debug.Log("Item not found in inventory.");
                return;
            }

            var possibleActions = InventoryDataHandler.GetPossibleActionsForInventoryItem(item);
            if (possibleActions.Contains(action) == false)
            {
                Debug.Log("invalid action for item.");
                return;
            }

            switch (action)
            {
                case "eat":
                case "drink":
                    item.Ammount -= 1;
                    if (item.Ammount <= 0)
                    {
                        Inventory.Remove(item);
                    }
                    // TODO Add logic to add hp to player...
                    Debug.Log("Consumed one " + item.Item.Name);
                    break;


                case "drop":
                    item.Ammount -= 1;
                    if (item.Ammount <= 0)
                    {
                        Inventory.Remove(item);
                    }
                    Debug.Log("Dropped one " + item.Item.Name);
                    break;

                default:
                    Debug.Log("Unknown action " + action);
                    break;
            }
        }

        private InventoryItem GetInventoryItemById(int id)
        {
            return Inventory.FirstOrDefault(i => i.ItemId == id);
        }

    }
}
