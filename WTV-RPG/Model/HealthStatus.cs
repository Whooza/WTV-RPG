﻿
namespace WTV_RPG.Model
{
    class HealthStatus
    {
        public int Id { get; set; }
        public int AccountId { get; set; }

        public float Starvation { get; set; } = 0;
        public float Thirst { get; set; } = 0;
    }
}
