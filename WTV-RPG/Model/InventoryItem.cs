﻿using WTV_RPG.Controller;

namespace WTV_RPG.Model
{
        class InventoryItem
    {
        public int ItemId { get; set; }
        public int Ammount { get; set; }
        public ItemType ItemType { get; set; }

        public Item Item
        {
            get
            {
                return ItemController.GetDataById(ItemId);
            }
        }
    }
}
