﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Controller
{
    class ClanController
    {
        public static void UpdateSyncedData(int id)
        {
            id = id - 1;
            var clan = ServerData.Clans[id];

            id = clan.Id;

            API.shared.setWorldSyncedData("Clan" + id + "_Name", clan.Name);
            API.shared.setWorldSyncedData("Clan" + id + "_Tag", clan.Tag);
            API.shared.setWorldSyncedData("Clan" + id + "_Leader", clan.Leader);
            API.shared.setWorldSyncedData("Clan" + id + "_Message", clan.Message);
            API.shared.setWorldSyncedData("Clan" + id + "_Color", clan.Color);
            API.shared.setWorldSyncedData("Clan" + id + "_Member1", clan.Member1);
            API.shared.setWorldSyncedData("Clan" + id + "_Member2", clan.Member2);
            API.shared.setWorldSyncedData("Clan" + id + "_Member3", clan.Member3);
            API.shared.setWorldSyncedData("Clan" + id + "_Member4", clan.Member4);
            API.shared.setWorldSyncedData("Clan" + id + "_Member5", clan.Member5);
            API.shared.setWorldSyncedData("Clan" + id + "_Member6", clan.Member6);
            API.shared.setWorldSyncedData("Clan" + id + "_Member7", clan.Member7);
            API.shared.setWorldSyncedData("Clan" + id + "_Member8", clan.Member8);
            API.shared.setWorldSyncedData("Clan" + id + "_Member9", clan.Member9);
            API.shared.setWorldSyncedData("Clan" + id + "_Member10", clan.Member10);

            var players = 0;
            for (var i = 1; i <= 10; i++)
            {
                players = (string)API.shared.getWorldSyncedData("Clan" + id + "_Member" + i) != "None" ? players + 1 : players;
            }
            API.shared.setWorldSyncedData("Clan" + id + "_Members", players);
        }

        public static void Init()
        {
            var clans = DatabaseConnector.QueryResult("SELECT * FROM clans");

            if (clans != null)
            {
                foreach (DataRow row in clans.Rows)
                {
                    var clan = new Clan
                    {
                        Id = Convert.ToInt32(row["id"]),
                        Name = row["Name"].ToString(),
                        Tag = row["Tag"].ToString(),
                        Leader = row["Leader"].ToString(),
                        Color = row["Color"].ToString(),
                        Message = row["Message"].ToString(),
                        Member1 = row["Member1"].ToString(),
                        Member2 = row["Member2"].ToString(),
                        Member3 = row["Member3"].ToString(),
                        Member4 = row["Member4"].ToString(),
                        Member5 = row["Member5"].ToString(),
                        Member6 = row["Member6"].ToString(),
                        Member7 = row["Member7"].ToString(),
                        Member8 = row["Member8"].ToString(),
                        Member9 = row["Member9"].ToString(),
                        Member10 = row["Member10"].ToString()
                    };
                    ServerData.Clans.Add(clan);
                    UpdateSyncedData(clan.Id);
                }
            }

            Debug.Log($"› Clans: {ServerData.Clans.Count} loaded");
        }

        public static bool CreateClan(Client player, string clanName, string clanTag)
        {

            foreach (var data in ServerData.Clans)
            {
                if (data.Name == clanName)
                {
                    UiController.DrawNotification(player, "This Clan name already exist", 2000);
                    return false;
                }
                if (data.Tag == clanTag)
                {
                    UiController.DrawNotification(player, "This Clan tag already exist", 2000);
                    return false;
                }
            }

            // ReSharper disable once ObjectCreationAsStatement
            var clan = new Clan
            {
                Id = ServerData.Clans.Count + 1,
                Name = clanName,
                Tag = clanTag,
                Leader = player.name,
                Color = "~r~",
                Message = "Welcome to " + clanName,
                Member1 = player.name,
                Member2 = "None",
                Member3 = "None",
                Member4 = "None",
                Member5 = "None",
                Member6 = "None",
                Member7 = "None",
                Member8 = "None",
                Member9 = "None",
                Member10 = "None"
            };

            ServerData.Clans.Add(clan);
            UpdateSyncedData(clan.Id);

            DatabaseConnector.Query(
                "INSERT INTO clans (Name, Tag, Leader, Color, Message, Member1) VALUES " +
                $"('{clan.Name}', '{clan.Tag}', '{clan.Member1}', '{clan.Color}', '{clan.Message}', '{clan.Member1}')");

            return true;
        }

        public static string GetClanColorById(int id)
        {
            id = id - 1;
            return ServerData.Clans[id].Color;
        }
        public static string GetClanNameById(int id)
        {
            if (id == -1) return "None";
            id = id - 1;
            return ServerData.Clans[id].Name;
        }
        public static string GetClanTagById(int id)
        {
            id = id - 1;
            return ServerData.Clans[id].Tag;
        }

        public static Clan GetData(int id)
        {
            id = id - 1;
            return ServerData.Clans[id];
        }
    }
}
