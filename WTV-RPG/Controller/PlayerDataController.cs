﻿using GrandTheftMultiplayer.Server.Elements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Controller
{
    class PlayerDataController
    {
        /// <summary>
        /// Returns the PlayerData for a given Client. 
        /// Creation can be forced if no playerData was found.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="createIfNotExists"></param>
        /// <returns></returns>
        internal static PlayerData GetPlayerData(Client client, bool createIfNotExists = false)
        {
            var account = AccountController.GetData(client);

            var found = LoadPlayerData(client, account);

            if (found == null && createIfNotExists == true)
            {
                Debug.Log("No PlayerData found. Creating new PlayerData");
                // get account 

                // create new playerData for this account
                PlayerData data = new PlayerData
                {
                    Id = -1,
                    Weapon = string.Empty,
                    Inventory = new List<InventoryItem>(),
                    AccountId = account.Id

                };

                CreatePlayerDataInDatabase(account, data);

            }
            return found;
        }

        internal static void SavePlayerData(Client player)
        {
            var account = player.hasData("Account") ? player.getData("Account") as Account : null;
            if (account == null)
            {
                Debug.Log("Error : Account was null when trying to save it.");
                return;
            }

            var playerData = player.hasData("PlayerData") ? player.getData("PlayerData") as PlayerData : null;
            if (playerData == null)
            {
                Debug.Log("Error : PlayerData was null when trying to save it.");
                return;
            }

            UpdatePlayerDataInDatabase(account, playerData);

        }

        /// <summary>
        /// Creates a new PlayerData object in the database.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="playerData"></param>
        private static void CreatePlayerDataInDatabase(Account account, PlayerData playerData)
        {
            string inventoryString = JsonConvert.SerializeObject(playerData.Inventory, Formatting.Indented);
            var id = DatabaseConnector.Query(
                "INSERT INTO playerdata (weapon, inventory, accountId) VALUES " +
                  $"('{playerData.Weapon}', '{inventoryString}', '{account.Id}')");

            // set the id to the object
            playerData.Id = (int)id;
        }

        /// <summary>
        /// Loads a PlayerData for a given account.
        /// </summary>
        /// <param name="client">Client that called this.</param>
        /// <param name="account">Account of this client</param>
        /// <returns></returns>
        private static PlayerData LoadPlayerData(Client client, Account account)
        {
            var result = DatabaseConnector.QueryResult($"SELECT * FROM playerdata WHERE accountId='{account.Id}'");
            PlayerData playerData = null;
            if (result != null && result.Rows != null)
            {
                var userData = result.Rows[0];
                playerData = new PlayerData
                {
                    Id = Convert.ToInt32(userData["id"]),
                    Weapon = userData["weapon"].ToString(),
                    Inventory = ParseInventory(userData["inventory"].ToString()),
                    AccountId = Convert.ToInt32(userData["accountId"])
                };

                UpdatePlayerDataInDatabase(account, playerData);

            }
            return playerData;

        }

        /// <summary>
        /// Updates a previously saved PlayerData in the database.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="playerData"></param>
        private static void UpdatePlayerDataInDatabase(Account account, PlayerData playerData)
        {
            string inventoryString = JsonConvert.SerializeObject(playerData.Inventory, Formatting.Indented);

            DatabaseConnector.Query(
               "UPDATE playerdata SET " +
               $"weapon = '{playerData.Weapon}', " +
               $"inventory = '{inventoryString}', " +
               $"accountId = '{account.Id}' " +
               $"WHERE accountId = '{account.Id}'"
           );

            Debug.Log("Updated PlayerData in Db for accountId " + account.Id);
        }

        #region Helper

        private static List<InventoryItem> ParseInventory(string inventoryString)
        {
            List<InventoryItem> items = JsonConvert.DeserializeObject<List<InventoryItem>>(inventoryString);
            return items ?? new List<InventoryItem>();
        }

        #endregion
    }
}
