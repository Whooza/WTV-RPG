﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Controller
{
    class ItemController
    {
        private static Dictionary<int, Item> itemDictionary = new Dictionary<int, Item>();

        internal static void Init()
        {
            ReadDataFromDatabase();
        }

        private static void ReadDataFromDatabase()
        {
            var result = DatabaseConnector.QueryResult("SELECT * FROM items");

            foreach (DataRow row in result.Rows)
            {
                Item item = new Item
                {
                    Id = Convert.ToInt32(row["Id"].ToString()),
                    Name = row["Name"].ToString(),
                    Description = row["Description"].ToString(),
                    ItemType = (ItemType)Enum.Parse(typeof(ItemType), row["ItemType"].ToString())
                };

                itemDictionary.Add(item.Id, item);
            }

            Debug.Log("Found " + itemDictionary.Values.Count + " Items.");
        }

        public static Item GetDataById(int id)
        {
            itemDictionary.TryGetValue(id, out Item found);
            return found;
        }

        public static List<Item> GetItemList()
        {
            return itemDictionary.Values.ToList();
        }
    }
}
