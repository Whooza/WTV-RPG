﻿
using System.Data;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Controller
{
    internal class SpawnController
    {

        internal static void Init()
        {
            Debug.Log("Starting SpawnController");
            var spawns = DatabaseConnector.QueryResult("SELECT * FROM spawns");
            if (spawns != null)
            {
               
                foreach (DataRow row in spawns.Rows)
                {
                    AddSpawn(row["Position"].ToString(), row["Angel"].ToString());
                }
            }
            Debug.Log($"› Spawns: {ServerData.Spawns.Count} loaded");
        }

        public static void AddSpawn(string position, string angel)
        {
            var add = new Transform(position, angel);
            ServerData.Spawns.Add(add);
        }
    }
}
