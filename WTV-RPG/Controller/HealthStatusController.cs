﻿using GrandTheftMultiplayer.Server.Elements;
using System;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Controller
{
    class HealthStatusController
    {
        internal static HealthStatus GetHealthStatus(Client client, bool createIfNotExists = false)
        {
            var account = AccountController.GetData(client);
            var found = LoadHealthStatus(client, account);

            if (found == null && createIfNotExists == true)
            {
                Debug.Log("No HealthStatus found. Creating new HealthStatus");
                // get account 

                // create new playerData for this account
                HealthStatus data = new HealthStatus
                {
                    Id = -1,
                    AccountId = account.Id

                };

                CreateHealthStatusInDataBase(account, data);

            }
            return found;
        }

        internal static void SaveHealthStatus(Client player)
        {
            var account = player.hasData("Account") ? player.getData("Account") as Account : null;
            if (account == null)
            {
                Debug.Log("Error : Account was null when trying to save it.");
                return;
            }

            var playerData = player.hasData("HealthStatus") ? player.getData("HealthStatus") as HealthStatus : null;
            if (playerData == null)
            {
                Debug.Log("Error : HealthStatus was null when trying to save it.");
                return;
            }

            UpdateHealthStatusInDataBase(account, playerData);
        }

        private static void CreateHealthStatusInDataBase(Account account, HealthStatus healthStatus)
        {
            Debug.Log("inserting healthStatus with accountId : " + account.Id);
            var id = DatabaseConnector.Query(
                    "INSERT INTO healthstatus (starvation, thirst, accountId) VALUES " +
                    $"('{healthStatus.Starvation.ToString("0.00")}', '{healthStatus.Thirst.ToString("0.00")}', '{account.Id}')");

            // set the id to the object
            healthStatus.Id = (int)id;
        }

        private static HealthStatus LoadHealthStatus(Client client, Account account)
        {
            var result = DatabaseConnector.QueryResult($"SELECT * FROM healthstatus WHERE accountId='{account.Id}'");
            HealthStatus data = null;
            if (result != null && result.Rows != null)
            {
                var userData = result.Rows[0];
                data = new HealthStatus
                {
                    Id = Convert.ToInt32(userData["id"]),
                    Thirst = (float) Convert.ToDouble (userData["thirst"]),
                    Starvation = (float) Convert.ToDouble(userData["starvation"]),
                    AccountId =  Convert.ToInt32(userData["accountId"])
                };

                UpdateHealthStatusInDataBase(account, data);

            }
            return data;
        }

        private static void UpdateHealthStatusInDataBase(Account account, HealthStatus healthStatus)
        {
            DatabaseConnector.Query(
               "UPDATE healthstatus SET " +
               $"thirst = '{healthStatus.Thirst.ToString("0.00")}', " +
               $"starvation = '{healthStatus.Starvation.ToString("0.00")}', " +
               $"accountId = '{account.Id}' " +
               $"WHERE accountId = '{account.Id}'"
           );

            // Debug.Log("Updated HealthStatus in Db for accountId " + account.Id);
        }


    }
}
