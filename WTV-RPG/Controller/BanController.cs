﻿using System;
using System.Linq;
using WTV_RPG.Logging;
using WTV_RPG.GameLogic;
using System.Data;
using WTV_RPG.Model;
using GrandTheftMultiplayer.Server.Elements;

namespace WTV_RPG.Controller
{
    class BanController
    {
        public static void Init()
        {
            ReadDataFromDatabase();
            Debug.Log($"› Bans: {ServerData.Bans.Count} loaded");
        }

        /// <summary>
        /// Reads the current Bans table from Database and saves them to the Main class.
        /// </summary>
        private static void ReadDataFromDatabase()
        {
            lock (ServerData.Bans)
            {
                ServerData.Bans.Clear();
                var bans = DatabaseConnector.QueryResult("SELECT * FROM bans");

                if (bans != null)
                {
                    foreach (DataRow row in bans.Rows)
                    {
                        DateTime? endDate = null;
                        var stringDate = row["EndDate"].ToString();
                        if (stringDate != null && stringDate.Equals("") == false)
                        {
                            endDate = DateTime.Parse(stringDate);

                        }
                        var ban = new Ban
                        {
                            Id = Convert.ToInt32(row["id"]),
                            Type = Convert.ToInt32(row["Type"]),
                            Value = row["Value"].ToString(),
                            Reason = row["Reason"].ToString(),
                            EndDate = endDate
                        };

                        ServerData.Bans.Add(ban);

                    }
                }
            }
        }

        public static Ban GetPlayerBan(Account account)
        {
            Ban found = null;
            if (account != null)
            {
                found = ServerData.Bans.FirstOrDefault(b => b.Value.Equals(account.SocialClubName));
               
            }

            if(found != null)
            {
                Debug.Log("Ban found.");
            }
            else
            {
                Debug.Log("Ban NOT found.");
            }

            return found;
        }

        /// <summary>
        /// Returns the current Ban object for a given client.
        /// </summary>
        /// <param name="client">The client to get ban data for.</param>
        /// <returns>the found Ban object or null</returns>
        public static Ban GetPlayerBan(Client client)
        {
            var account = AccountController.GetData(client);
            return GetPlayerBan(account);
        }

        /// <summary>
        /// Removes a ban from database and Main.
        /// </summary>
        /// <param name="ban"></param>
        internal static void Remove(Ban ban)
        {
            lock (ServerData.Bans)
            {
                ServerData.Bans.Remove(ban);
                DatabaseConnector.Query("DELETE FROM bans WHERE ID = " + ban.Id);
            }
        }


        /// <summary>
        /// Saves a ban to database. 
        /// If the Id of the ban has the default value of -1 a new entry will be created.
        /// If there is already a ban for this player it will update the current db row.
        /// </summary>
        /// <param name="ban">Ban object to save to database.</param>
        public static void SaveBanToDb(Ban ban)
        {
            var dateString = ban.EndDate != null ? ban.EndDate.Value.ToString() : "";

            // insert, new ban
            if (ban.Id < 0)
            {

                DatabaseConnector.Query(
                    "INSERT INTO bans (Type, Value, Reason, EndDate) VALUES " +
                    $"('{ban.Type.ToString()}', '{ban.Value}', '{ban.Reason}', '{ dateString  }')");

            }
            else
            {
                // update
                DatabaseConnector.Query(
                   string.Format("UPDATE bans SET Type = {0}, Value = {1}, Reason = {2}, EndDate = {3} WHERE Id = {4}; ",
                   ban.Type.ToString(), ban.Value, ban.Reason, ban.EndDate.Value.ToString(), ban.Id));
            }
            ReadDataFromDatabase();
        }
    }
}
