﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using WTV_RPG.Logging;
using WTV_RPG.Model;
using WTV_RPG.Util;

namespace WTV_RPG.Controller
{
    class AccountController
    {
        public static bool IsPlayerRegistered(Client player, out Account account)
        {
            account = null;
            var hasUser = DatabaseConnector.QueryResult($"SELECT * FROM accounts WHERE SocialClubName='{player.socialClubName}'");
            if (hasUser != null)
            {

                return LoadAccount(player, hasUser, out account);

            }

            return false;
        }

        /// <summary>
        /// Checks if the passed client is an Admin.
        /// returns the Account.Controller as out parameter.
        /// </summary>
        /// <param name="client">Client to check.</param>
        /// <param name="accountController">Accounts.Controller of the account or null.</param>
        /// <returns></returns>
        internal static bool IsClientAdmin(Client client, out Account account)
        {
            account = GetData(client);
            if (account == null || account.Admin <= 0)
            {
                API.shared.sendNotificationToPlayer(client, "Du bist kein Admin!");
                return false;
            }
            return true;
        }

        public static bool RegisterAccount(Client player, string password)
        {
            var account = new Account
            {
                SocialClubName = player.socialClubName,
                Password = Functions.GetMd5Hash(password),
                RegIp = player.address,
                LastIp = player.address,
                Admin = 0,
                Kills = 0,
                Deaths = 0,
                Clan = -1,
                ClanState = -1,
                OnServer = 0
            };

            var insertedId = DatabaseConnector.Query(
                     "INSERT INTO accounts (UserName, SocialClubName, Password, RegIP, LastIP) VALUES " +
                     $"('{player.name}', '{player.socialClubName}', '{account.Password}', '{player.address}', '{player.address}')");

            if (insertedId > 0)
            {
                account.Id = (int)insertedId;
                return true;
            }
            return false;
        }

        private static bool LoadAccount(Client player, DataTable tales, out Account account)
        {
            // Debug.Log("Loading Account");
            var user = tales.Rows[0];

            account = new Account
            {
                Id = Convert.ToInt32(user["id"].ToString()),
                SocialClubName = user["SocialClubName"].ToString(),
                Password = user["Password"].ToString(),
                RegIp = user["RegIP"].ToString(),
                Admin = Convert.ToInt32(user["Admin"]),
                Kills = Convert.ToInt32(user["Kills"]),
                Deaths = Convert.ToInt32(user["Deaths"]),
                Clan = Convert.ToInt32(user["Clan"]),
                ClanState = Convert.ToInt32(user["ClanState"]),
                OnServer = Convert.ToInt32(user["OnServer"])
            };
            player.resetData("Account");
            player.setData("Account", account);


            Debug.Log("Account for user " + player.socialClubName + " loaded form db.");
            return true;
        }

        public static Account GetData(Client player)
        {
            var returned = player.hasData("Account") ? player.getData("Account") as Account : null;
            return returned;
        }

        public static bool SaveAccount(Client player)
        {
            var account = GetData(player);

            DatabaseConnector.Query(
                "UPDATE accounts SET " +
                $"LastIP = '{player.address}', " +
                $"Kills = '{account.Kills}', " +
                $"Deaths = '{account.Deaths}', " +
                $"OnServer = '{account.OnServer}', " +
                $"Clan = '{account.Clan}', " +
                $"ClanState = '{account.ClanState}' " +
                $"WHERE SocialClubName = '{player.socialClubName}'"
            );

            return true;
        }
    }
}
