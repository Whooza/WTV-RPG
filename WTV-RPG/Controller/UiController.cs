﻿using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.API;

namespace WTV_RPG.Controller
{
    /// <summary>
    /// Used to trigger Ui events on client side
    /// </summary>
    static class UiController
    {
        //  ************** Healthbar Display **************
        internal static void ShowHideHealthBar(Client player, bool show)
        {
            player.healthbarVisible = show;
        }

        // ************** Starvation Display **************
        internal static void ShowHideStarvation(Client player, bool show)
        {
            API.shared.triggerClientEvent(player, "SHOW_STARVATION", show);
        }

        // ************** Login Panel **************
        public static void ShowLoginPanel(Client player)
        {
            API.shared.triggerClientEvent(player, "showLoginPanel");
        }

        // ************** Register Panel **************
        public static void ShowRegisterPanel(Client player)
        {
            API.shared.triggerClientEvent(player, "showRegisterPanel");
        }

        // ************** CLAN **************
        public static void ShowAcceptClanInviteDialog(Client player, int clanId, string clanName)
        {
            API.shared.triggerClientEvent(player, "showAcceptClanInviteDialog", clanId, clanName);
        }

        // ************** Commands Pannel **************
        public static void ShowCommandsPanel(Client player)
        {
            API.shared.triggerClientEvent(player, "showCommandsPanel");
        }

        // ************** Results Pannel ************** 
        public static void ShowRulesPanel(Client player)
        {
            API.shared.triggerClientEvent(player, "showRulesPanel");
        }

        // ************** Draw Menues **************
        public static void DrawNotification(Client player, string text, int displayTime)
        {
            API.shared.triggerClientEvent(player, "drawNotification", text, displayTime);
        }
        public static void KillDrawMenus(Client player)
        {
            API.shared.triggerClientEvent(player, "killDrawMenus");
        }

    }
}
