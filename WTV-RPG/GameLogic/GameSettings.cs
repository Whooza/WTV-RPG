﻿
namespace WTV_RPG.GameLogic
{
    internal static class GameSettings
    {
        /// <summary>
        /// Server settings.
        /// </summary>

        // seconds between player update
        internal static readonly int UpdatePlayerIntervall = 5;


        /// <summary>
        /// Starvation, Thirst Factors per Minute.
        /// </summary>
        internal static readonly float StarvationFactor = 0.03f;
        internal static readonly float ThirstFactor = 0.05f;

        internal static float DMG_PERSTARV_FACTOR = 0.1f;
        internal static float DMG_PERTHIRST_FACTOR = 0.1f;
    }
}
