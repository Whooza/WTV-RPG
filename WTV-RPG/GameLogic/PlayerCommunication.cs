﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;

namespace WTV_RPG.GameLogic
{
    internal static class PlayerCommunication
    {
        /// <summary>
        /// Sends an adming message to all players on the server.
        /// </summary>
        /// <param name="message">message to send</param>
        /// <param name="sendingPlayer">the player sending it.</param>
        /// <param name="sendingScript">the script with the method that is sending the message.</param>
        internal static void SendAdminMessageToAll(string message, Client sendingPlayer)
        {
            API.shared.sendNotificationToAll("[ADMIN] " + sendingPlayer.name + message);
        }

        internal static void SendNotificationToAll(string message)
        {
            API.shared.sendNotificationToAll(message);
        }

        /// <summary>
        /// Sends an adming message to a specific player.
        /// </summary>
        /// <param name="message">the message to send.</param>
        /// <param name="sendingPlayer">the sending player.</param>
        /// <param name="targetPlayer">the player to send the message to.</param>
        /// <param name="sendingScript">the script with the method that is sending the message.</param>
        internal static void SendAdminMessageToPlayer(string message, Client sendingPlayer, Client targetPlayer, Script sendingScript)
        {
            // TODO admin check

            targetPlayer.sendNotification("[ADMIN] " + sendingPlayer.name, message);
        }

        public static void ToAll(string text) => API.shared.sendChatMessageToAll(text);
        public static void ToAll(string sender, string text) => API.shared.sendChatMessageToAll(sender, text);
    }
}
