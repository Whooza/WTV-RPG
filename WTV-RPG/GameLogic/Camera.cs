﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;

namespace WTV_RPG
{
    public static class Camera
    {
        public static void CreateCameraActive(Client player, Vector3 position, Vector3 rotation)
        {
            API.shared.triggerClientEvent(player, "createCameraActive", position, rotation);
        }

        public static void CreateCameraInactive(Client player, Vector3 position, Vector3 rotation)
        {
            API.shared.triggerClientEvent(player, "createCameraInactive", position, rotation);
        }

        public static void CreateCameraAtGamecam(Client player)
        {
            API.shared.triggerClientEvent(player, "createCameraAtGamecam");
        }

        public static void PointCameraAtPosition(Client player, int camera, Vector3 position)
        {
            API.shared.triggerClientEvent(player, "pointCameraAtPosition", camera, position);
        }

        public static void InterpolateCamera(Client player, int tocamera, double duration, bool easepos, bool easerot)
        {
            API.shared.triggerClientEvent(player, "interpolateCamera", tocamera, duration, easepos, easerot);
        }

        public static void SetActiveCamera(Client player, int camera)
        {
            API.shared.triggerClientEvent(player, "setActiveCamera", camera);
        }

        public static void SetCameraFov(Client player, int camera, float fov)
        {
            API.shared.triggerClientEvent(player, "setCameraFov", camera, fov);
        }

        public static void SetCameraHeading(Client player, int camera, double heading)
        {
            API.shared.triggerClientEvent(player, "setCameraHeading", camera, heading);
        }

        public static void PointCameraAtLocalPlayer(Client player, int camera, Vector3 offset)
        {
            API.shared.triggerClientEvent(player, "pointCameraAtLocalPlayer", camera, offset);
        }

        public static void BackToGamecam(Client player)
        {
            API.shared.triggerClientEvent(player, "backToGamecam");
        }

        public static void ClearCameras(Client player)
        {
            API.shared.triggerClientEvent(player, "clearCameras");
        }
    }
}