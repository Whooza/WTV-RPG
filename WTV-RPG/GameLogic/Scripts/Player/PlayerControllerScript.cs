﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using WTV_RPG.Controller;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Util.Extensions;

namespace WTV_RPG.Events
{
    class PlayerControllerScript : Script
    {
        /// <summary>
        /// Singelton
        /// </summary>
        internal static PlayerControllerScript Instance = null;

        private DateTime _lastUpdate;
        private bool _started;
        private List<ClientLastUpdateData> _clientList = new List<ClientLastUpdateData>();

        public PlayerControllerScript()
        {
            if (Instance != null)
            {
                Debug.Log("ERROR !!!! There should only be only one PlayerControllerScript!");
                Instance = null;
                return;
            }

            _lastUpdate = DateTime.Now;
            API.onUpdate += Update;
            Instance = this;
        }

        /// <summary>
        /// Enables and disables this script update loop.
        /// </summary>
        /// <param name="active"></param>
        internal void SetActive(bool active)
        {
            if (_started != active)
            {
                _started = active;
                Debug.Log("PlayerControllerScript started");
            }
        }

        /// <summary>
        /// Adds a connected client to be updated
        /// </summary>
        /// <param name="player"></param>
        internal void AddPlayerToUpdate(Client player)
        {
            lock (_clientList)
            {
                if (_clientList.Any(c => c.Client == player) == false)
                {
                    var data = new ClientLastUpdateData
                    {
                        Client = player,
                        LastUpdate = DateTime.Now
                    };

                    _clientList.Add(data);
                    Debug.Log("Added player to PlayerController : " + player.name);
                }
            }
        }

        /// <summary>
        /// Removes a client from updates.
        /// </summary>
        /// <param name="player"></param>
        internal void RemovePlayerToUpdate(Client player)
        {
            lock (_clientList)
            {
                var found = _clientList.FirstOrDefault(c => c.Client == player);
                if (found != null)
                {
                    _clientList.Remove(found);
                    Debug.Log("Removed player from PlayerController : " + player.name);
                }
            }
        }

        private void Update()
        {
            if (_started == false) { return; }
            if (_clientList.Count == 0) { return; }

            foreach (var client in _clientList)
            {
                UpdateClient(client);
            }
        }

        private void UpdateClient(ClientLastUpdateData clientData)
        {
            float deltaTime = (float)DateTime.Now.Subtract(clientData.LastUpdate).TotalSeconds;
            if (deltaTime >= GameSettings.UpdatePlayerIntervall)
            {
                UpdateHealthStatus(clientData, deltaTime);
                clientData.LastUpdate = DateTime.Now;
                // Debug.Log("Updated Client " + clientData.Client.name);
            }
        }

        private void UpdateHealthStatus(ClientLastUpdateData clientData, float deltaTime)
        {
            var healthStatus = clientData.Client.GetHealthStatusForClient();

            // calculate new starvation
            var newStarvation = healthStatus.Starvation + GameSettings.StarvationFactor * deltaTime;
            var newThirst = healthStatus.Thirst + GameSettings.ThirstFactor * deltaTime; ;

            // cap values to 100
            newStarvation = newStarvation >= 100f ? 100f : newStarvation;
            newThirst = newThirst >= 100f ? 100f : newThirst;

            // set new values
            healthStatus.Thirst = newThirst;
            healthStatus.Starvation = newStarvation;

            // if thirst or starvation gets over the ammount of 50 the player will start to suffer.
            if (healthStatus.Starvation > 50f || healthStatus.Thirst > 50f)
            {
                var thirstDmg = healthStatus.Thirst > 50 ? (healthStatus.Thirst - 50f) * GameSettings.DMG_PERTHIRST_FACTOR * deltaTime : 0f;
                var hungerDmg = healthStatus.Starvation > 50 ? (healthStatus.Starvation - 50f) * GameSettings.DMG_PERSTARV_FACTOR * deltaTime : 0f;
                var expectedHealthDmg = Convert.ToInt32(thirstDmg + hungerDmg);
                var newHealth = clientData.Client.health;
                newHealth = newHealth - expectedHealthDmg <= 0 ? 0 : newHealth - expectedHealthDmg;
                clientData.Client.health = newHealth;
                Debug.Log(clientData.Client.name + " Dmg dealed : (" + thirstDmg + " | " + hungerDmg + ") " + expectedHealthDmg + " Current Client Health : " + clientData.Client.health);

                if(newHealth == 0f)
                {
                    clientData.Client.kill();
                }


            }

            HealthStatusController.SaveHealthStatus(clientData.Client);
            API.triggerClientEvent(clientData.Client, "UPDATE_STARVATION", healthStatus.Starvation.ToString("0.00"), healthStatus.Thirst.ToString("0.00"));
        }

        /// <summary>
        /// Data holder for a client to update.
        /// Contains the client object and the timestamp of its last update.
        /// </summary>
        private class ClientLastUpdateData
        {
            public DateTime LastUpdate { get; set; }
            public Client Client { get; set; }
        }
    }


}
