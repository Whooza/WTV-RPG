﻿using System.Threading;
using GrandTheftMultiplayer.Server.API;
using WTV_RPG.Controller;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Util.Extensions;
// ReSharper disable FunctionNeverReturns

namespace WTV_RPG.Global
{
    public class Timers : Script
    {
      //  private Thread _messagesThread;
        private Thread _updateThread;

        public static void Init()
        {
            var timer = new Timers
            {
              //  _messagesThread = new Thread(RandomMessages),
                _updateThread = new Thread(ServerUpdate)
            };

      //      timer._messagesThread.Start();
      //      Debug.Log("› Timer: RandomMessages start");

            timer._updateThread.Start();
            Debug.Log("› Timer: ServerUpdate start");
        }

        /*
        private static void RandomMessages()
        {
            while (true)
            {
                switch (ServerData.Random.Next(4))
                {
                    case 0:
                        PlayerCommunication.ToAll("~b~[INFO] ~o~CTRL ~s~+ ~o~Z ~s~- Server main menu and Player stats!");
                        break;
                    case 1:
                        PlayerCommunication.ToAll("~b~[INFO] ~s~CTRL + G - Drop Current Weapon!");
                        break;
                    case 2:
                        PlayerCommunication.ToAll("~b~[HELP] ~s~Need some help? Type ~b~/help ");
                        break;
                    case 3:
                        PlayerCommunication.ToAll("~p~[INFO] ~o~Author of this mode: ~b~SkyLaGer (site: gta-dev.ru)");
                        break;
                }
                Thread.Sleep(3 / 2 * 60 * 1000); // Раз в 1.5 минуты
            }
        }*/

        private static void ServerUpdate()
        {
            while (true)
            {
                ServerData.ServerUpTime++;

                foreach (var player in API.shared.getAllPlayers())
                {
                    var account = AccountController.GetData(player);
                    if (account != null)
                        account.OnServer += 1;
                }

                if (ServerData.ServerUpTime % 5 == 0) // 1 per 5 second
                {
                    foreach (var player in API.shared.getAllPlayers())
                    {
                        if (AccountController.GetData(player) != null)
                            player.UpdateSyncedAccountData();
                    }
                   // Debug.Log("ServerUpTime : " + ServerData.ServerUpTime);
                }

                /*
                if (Main.ServerUpTime % (60 * 5) == 0) // 1 per 5 minutes
                    Pickups.Controller.UpdatePickups();
                
                
                if (Main.ServerUpTime % (60 * 30) == 0) // 1 per 30 minutes
                    Cars.Controller.ReturnPositions(); // Раз в 30 минут вернуть машины на свои места, уничтоженные по ноовй создать
                
                if (Main.ServerUpTime % (60 * 60 * 12) == 0)
                    Cars.Controller.UpdateCars();// Раз в 12 часов обновить все динамически авто на сервере
                */

                if (ServerData.ServerUpTime % (60 * 60 * 24) == 0)
                {
                    ServerData.ServerUpTime = 0; //Сбрасываем Счетчик если таймер = 24 часа!
                    ServerData.ServerUpDays++;
                }
                Thread.Sleep(1 * 1000); // Раз в секунду
            }
        }
    }
}