﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using WTV_RPG.Handler;
using WTV_RPG.Logging;

namespace WTV_RPG.Global
{
    public class ServerEvents : Script
    {

        public ServerEvents()
        {
            //API.onEntityEnterColShape += OnEntityEnterColShape;
            API.onClientEventTrigger += OnClientEventTrigger;
        }

        private void OnClientEventTrigger(Client player, string eventName, params object[] arguments)
        {
            // ------------------ LOGIN ------------------
            Debug.Log("Event name : " + eventName);

            if (eventName.Equals("clientRegistration"))
            {
                LoginHandler.HandleRegistration(player, arguments, this);
            }
            else if (eventName.Equals("clientLogin"))
            {
                LoginHandler.HandleLogin(player, arguments, this);
            }

            // ------------------ INVENTORY ------------------
            else if (eventName.Equals(InventoryDataHandler.REQUEST_INVENTORY))
            {
                InventoryDataHandler.OnClientRequestedInventory(player, arguments, this);
            }

            else if (eventName.Equals(InventoryDataHandler.REQUAST_ITEM_USE))
            {
                InventoryDataHandler.OnItemActionTriggered(player, arguments, this);
            }

        }
    }
}