﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System;
using WTV_RPG.Controller;
using WTV_RPG.Events;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Model;
using WTV_RPG.Util.Extensions;

namespace WTV_RPG.Global
{
    public class PlayerEvents : Script
    {
        private static PlayerEvents _instance;

        public PlayerEvents()
        {
            API.onPlayerBeginConnect += OnPlayerBeginConnect;
            API.onPlayerFinishedDownload += OnPlayerFinishedDownload;
            API.onPlayerConnected += OnPlayerConnected;

            API.onPlayerDisconnected += OnPlayerDisconnected;

            API.onChatMessage += OnChatMessage;

            API.onPlayerRespawn += OnPlayerRespawn;
            API.onPlayerDeath += OnPlayerDeath;
            _instance = this;

        }

        private static void OnPlayerBeginConnect(Client player, CancelEventArgs cancelConnection)
        {
            player.sendChatMessage("~b~[INFO] ~s~Server is loading files. Please wait a few seconds...");
        }

        private static void OnPlayerFinishedDownload(Client player)
        {
            player.resetData("Account");

            Camera.CreateCameraInactive(player, new Vector3(-113.8, -800, 196.1), new Vector3());
            //Camera.PointCameraAtPosition(player, 0, new Vector3(729.27, 1118.93, 337.05));

            //check if player is baned.
            if (AccountController.IsPlayerRegistered(player, out Account account))
            {
                // user registered
                Debug.Log("Known user connected and account is null ? " + (account == null));

                Ban foundBan = BanController.GetPlayerBan(account);

                if (foundBan != null)
                {
                    // ban found
                    //  Main.Log("Found ban for player.");
                    // check if user is still banned
                    DateTime current = DateTime.Now;
                    if (foundBan.EndDate > current || foundBan.EndDate == null)
                    {
                        // user is still banned  
                        player.kick(string.Format("You're banned.~n~~n~~b~Reason: ~w~{0}~n~~b~Ends On: ~w~{1}", foundBan.Reason, ((foundBan.EndDate == null) ? "Permanent" : foundBan.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss"))));
                        return;
                    }
                    else
                    {
                        // ban expired
                        // Main.Log("Ban expired.");
                        player.sendChatMessage("Your ban has expired.");
                        BanController.Remove(foundBan);
                    }

                }
                UiController.ShowLoginPanel(player);

            }
            else
            {
                // user not registered
                Debug.Log("New user connected.");
                UiController.ShowRegisterPanel(player);
            }

            player.position = new Vector3(-113.8, -800, 196.1);
            player.rotation = new Vector3(0, 0, 0);
            player.transparency = 0;
            player.giveWeapon(WeaponHash.Unarmed, 1, true, true);
            player.freezePosition = true;
            player.freeze(true);

        }

        private static void OnPlayerConnected(Client player)
        {
            var index = ServerData.GetFreePlayerId();
            if (index != -1)
            {
                ServerData.Players[index] = player;
            }

            PlayerCommunication.SendNotificationToAll($"~g~[+] ~s~Player {player.name} (ID: {index}) has joined the server");
        }

        public static void OnPlayerDisconnected(Client player, string reason)
        {
            PlayerCommunication.ToAll($"~c~[-] ~s~Player {player.name} (ID: {ServerData.GetIdByClient(player)}) left from server");
            var account = AccountController.GetData(player);
            if (account != null)
                if (account.LoggedIn)
                {
                    AccountController.SaveAccount(player);
                    PlayerDataController.SavePlayerData(player);
                    account.LoggedIn = false;
                }


            if (ServerData.Players.IndexOf(player) != -1)
            {
                ServerData.Players[ServerData.Players.IndexOf(player)] = null;
            }

            PlayerControllerScript.Instance.RemovePlayerToUpdate(player);
        }

        private void OnChatMessage(Client player, string message, CancelEventArgs cancel)
        {
            var account = AccountController.GetData(player);

            if (account == null || !account.LoggedIn) cancel.Cancel = true;

            // ReSharper disable once PossibleNullReferenceException
            var clanId = account.Clan;

            var clanFormat = "";
            if (account.Clan != -1)
            {
                var clanColor = ClanController.GetClanColorById(clanId);
                var clanTag = ClanController.GetClanTagById(clanId);
                clanFormat = $"{clanColor}[{clanTag}]~s~";
            }

            var userid = $"(ID: {ServerData.GetIdByClient(player)})";

            PlayerCommunication.ToAll($"{clanFormat} {player.name} {userid}", message);

            cancel.Cancel = true;
        }

        private static void OnPlayerRespawn(Client player)
        {
            Util.Functions.RandomSpawn(player);
            player.setSkin(Util.Functions.RandomSkin());
            UiController.ShowHideStarvation(player, true);
        }

        private void OnPlayerDeath(Client player, NetHandle entityKiller, int weapon)
        {
            UiController.ShowHideStarvation(player, false);

            API.sendNativeToPlayer(player, Hash._RESET_LOCALPLAYER_STATE, player);
            API.sendNativeToPlayer(player, Hash.RESET_PLAYER_ARREST_STATE, player);

            API.sendNativeToPlayer(player, Hash.IGNORE_NEXT_RESTART, true);
            API.sendNativeToPlayer(player, Hash._DISABLE_AUTOMATIC_RESPAWN, true);

            API.sendNativeToPlayer(player, Hash.SET_FADE_IN_AFTER_DEATH_ARREST, true);
            API.sendNativeToPlayer(player, Hash.SET_FADE_OUT_AFTER_DEATH, false);
            API.sendNativeToPlayer(player, Hash.NETWORK_REQUEST_CONTROL_OF_ENTITY, player);

            API.sendNativeToPlayer(player, Hash.FREEZE_ENTITY_POSITION, player, false);
            API.sendNativeToPlayer(player, Hash.NETWORK_RESURRECT_LOCAL_PLAYER, player.position.X, player.position.Y, player.position.Z, player.rotation.Z, false, false);
            API.sendNativeToPlayer(player, Hash.RESURRECT_PED, player);

            API.sendNativeToPlayer(player, Hash.SET_PED_TO_RAGDOLL, player, true);

            API.delay(1500, true, () =>
            {
                OnPlayerRespawn(player);
            });

            var killer = API.getPlayerFromHandle(entityKiller);
            if (killer != null)
            {
                AccountController.GetData(killer).Kills += 1;
                AccountController.GetData(player).Deaths += 1;
            }

            player.ResetHealthStatus();
        }


    }
}