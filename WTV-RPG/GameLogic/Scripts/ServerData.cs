﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using System;
using System.Collections.Generic;
using WTV_RPG.Controller;
using WTV_RPG.Events;
using WTV_RPG.Global;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.GameLogic
{
    internal class ServerData : Script
    {
        private List<Ban> _bans = new List<Ban>();

        public static int ServerUpTime = 0;
        public static int ServerUpDays = 0;

        public static Random Random = new Random();

        public static List<Ban> Bans = new List<Ban>();
        public static List<Client> Players = new List<Client>();
        public static List<Clan> Clans = new List<Clan>();
        public static List<Transform> Spawns = new List<Transform>();


        public ServerData()
        {
            ReservePlayerSlots(API.getMaxPlayers());
            InitServer();
        }

        internal static int GetIdByClient(Client player)
        {
            return Players.IndexOf(player);
        }

        internal static int GetFreePlayerId()
        {
            int freeIndex = -1;
            lock (Players)
            {
                for (int i = 0; i < Players.Count; i++)
                {
                    if (Players[i] == null)
                    {
                        return i;
                    }
                }
            }
            return freeIndex;
        }

        private static void ReservePlayerSlots(int maxPlayers)
        {
            for (var i = 0; i < maxPlayers; i++)
            {
                Players.Add(null);
            }
        }

        private void InitServer()
        {
            API.onResourceStart += () =>
            {
                Debug.Log("Setup ServerSettings");
                API.setServerName("~o~WhoozaTV ~y~Community Server");
                API.setGamemodeName("~r~Hardcore~o~RolePlay~r~Game ~y~v0.0.1~w~");
                API.setServerPassword("draufda");
                API.shared.setTime(09, 00);
                API.setWeather(1);
                Debug.Log("Setup done.");

                for (var i = 0; i < API.getMaxPlayers(); i++)
                {
                    Players.Add(null);
                }
                // " Main.Log(""); " - in anderen Klassen (gleicher Namespace)              

                Debug.Log("Loading Controller...");
                SpawnController.Init();
                ClanController.Init();
                BanController.Init();
                ItemController.Init();
                Timers.Init();
                PlayerControllerScript.Instance.SetActive(true);
                
                Debug.Log("Loading Done.");
            };

            API.onResourceStop += () =>
            {
                foreach (var player in API.getAllPlayers())
                {
                    Global.PlayerEvents.OnPlayerDisconnected(player, "Restart");
                    foreach (var s in API.getAllEntityData(player))
                    {
                        API.resetEntityData(player, s);
                    }

                }
                foreach (var s in API.getAllWorldSyncedData())
                {
                    API.resetWorldSyncedData(s);
                }

                Debug.Log("###########################");
                Debug.Log("###### Recource end ######");
                Debug.Log("###########################");
            };

            API.setServerSocialClubDuplicateSetting(false);
        }
    }
}
