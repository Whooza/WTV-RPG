﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Text;
using WTV_RPG.Controller;
using WTV_RPG.GameLogic;
using WTV_RPG.Logging;
using WTV_RPG.Model;

namespace WTV_RPG.Admin
{
    class AdminTool : Script
    {
        [Command("spawnped")]
        public void SpawnPed(Client sendingPlayer)
        {
            var randomSkin = Util.Functions.RandomSkin();
            var pos = sendingPlayer.position;
            
            NetHandle myPed = API.createPed(randomSkin, pos, 0f);
        }


        [Command("showplayerids")]
        public void ShowPlayerIds(Client sendingPlayer)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < ServerData.Players.Count; i++)
                {
                    var currentPlayer = ServerData.Players[i];
                    if (currentPlayer != null)
                    {
                        builder.AppendLine(i + "\t - \t" + currentPlayer.name);
                    }
                }
                sendingPlayer.sendChatMessage(builder.ToString());
            }
        }

        [Command("additemtoplayer")]
        public void AddItemToPlayer(Client sendingPlayer, int playerId, int itemId, int ammount)
        {
            //   Debug.Log(playerId + " " + itemId + " " + ammount);
            if (AccountController.IsClientAdmin(sendingPlayer, out Account account))
            {
                var player = ServerData.Players[playerId];
                if (player != null && ammount > 0)
                {
                    var item = ItemController.GetDataById(itemId);
                    if (item != null)
                    {
                        PlayerData playerData = player.getData("PlayerData");
                        playerData.AddItemToInventory(item, ammount);
                        sendingPlayer.sendChatMessage(string.Format("Added {0} {1} to Player {2} ", ammount, item.Name, player.name));
                    }
                    else
                    {
                        Debug.Log("Error - Item not found. Id: " + itemId);
                    }
                }
                else
                {
                    Debug.Log("Error - Player with id : " + playerId + " not found or ammount too small.");
                }
            }
        }

        [Command("showitemids")]
        public void ShowItemIds(Client sendingPlayer)
        {
            if (AccountController.IsClientAdmin(sendingPlayer, out Account account))
            {
                StringBuilder builder = new StringBuilder();
                var itemList = ItemController.GetItemList();
                for (int i = 0; i < itemList.Count; i++)
                {
                    var currentItem = itemList[i];
                    if (currentItem != null)
                    {
                        builder.AppendLine(currentItem.Id + "\t - \t" + currentItem.Name);
                    }
                }
                sendingPlayer.sendChatMessage(builder.ToString());
            }
        }

        [Command("getpos")]
        public void GetPosition(Client sendingPlayer)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {
                Vector3 PlayerPos = API.getEntityPosition(sendingPlayer);
                API.sendChatMessageToPlayer(sendingPlayer, "X: " + PlayerPos.X + " Y: " + PlayerPos.Y + " Z: " + PlayerPos.Z);
            }
        }

        /// <summary>
        /// Bans a player by id.
        /// </summary>
        /// <param name="sendingPlayer">Client of the player using the command.</param>
        /// <param name="message">message containing the following format: banplayer <playerId> <duration> <Reason> </param>
        [Command("banplayer", GreedyArg = true)]
        public void BanPlayerById(Client sendingPlayer, int playerId, int duration, string message)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {



                var clientToBan = ServerData.Players[playerId];
                if (clientToBan != null)
                {
                    Account toBanAccount = AccountController.GetData(clientToBan);

                    DateTime? endDate = null;
                    if (duration > 0)
                    {
                        endDate = DateTime.Now.AddDays(duration);
                    }

                    Ban foundBan = BanController.GetPlayerBan(clientToBan);
                    Ban ban;
                    if (foundBan == null)
                    {

                        ban = new Ban
                        {
                            Type = (int)Ban.BanType.SocialClubName,
                            Value = toBanAccount.SocialClubName,
                            Reason = message,
                            EndDate = endDate

                        };
                    }
                    else
                    {
                        ban = foundBan;
                        ban.EndDate = endDate;
                        ban.Reason = message;

                    }

                    BanController.SaveBanToDb(ban);
                    PlayerCommunication.SendAdminMessageToAll(string.Format("Player {0} was banned by {1} for {2} Days. Reason : {3}", clientToBan.name, sendingPlayer.name, duration, ban.Reason), sendingPlayer);
                    clientToBan.kick(string.Format("You're banned.~n~~n~~b~Reason: ~w~{0}~n~~b~Ends On: ~w~{1}", ban.Reason, ((ban.EndDate == null) ? "Permanent" : ban.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss"))));
                }


            }
        }

        [Command("showbans")]
        public void ListBans(Client sendingPlayer)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {
                StringBuilder builder = new StringBuilder();
                foreach (var ban in ServerData.Bans)
                {
                    builder.AppendLine("ID \t name \t\t ends");
                    builder.AppendLine(ban.Id + " \t " + ban.Value + " \t\t " + ban.EndDate.ToString());
                }
                sendingPlayer.sendChatMessage(builder.ToString());
            }
        }

        /// <summary>
        /// Sends an Admin message to a player.
        /// The player is passed as id.
        /// </summary>
        /// <param name="sendingPlayer">The player that used the command.</param>
        /// <param name="action">The text of the command</param>
        [Command("sendtoplayer", GreedyArg = true)]
        public void SendToPlayer(Client sendingPlayer, int playerId, string message)
        {
            // check if the sending client is admin
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account) == false)
            {
                return;
            }
            string messageBody = string.Empty;

            if (playerId >= 0 && playerId < ServerData.Players.Count && ServerData.Players[playerId] != null)
            {
                // id ok
                Client targetPlayer = ServerData.Players[playerId];
                if (targetPlayer != null)
                {
                    PlayerCommunication.SendAdminMessageToPlayer(message, sendingPlayer, targetPlayer, this);
                }
                else
                {
                    API.sendNotificationToPlayer(sendingPlayer, "Ungültige Id.");
                }
            }
            else
            {
                API.sendNotificationToPlayer(sendingPlayer, "Ungültige Id.");
            }
        }

        [Command("sendtoall", GreedyArg = true)]
        public void SendToAll(Client sendingPlayer, string action)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {
                foreach (var player in ServerData.Players)
                {
                    if (player != null)
                    {
                        API.sendNotificationToPlayer(player, action);
                    }
                }
            }
        }

        [Command("weather", Description = "Aendert das Wetter")]
        public void Weather(Client player, int weather)
        {
            var data = AccountController.GetData(player);
            if (data == null || data.Admin <= 0)
            {
                API.sendNotificationToPlayer(player, "Du bist kein Admin!");
                return;
            }

            if (weather < 0 || weather > 13)
                player.sendChatMessage("~r~Ungueltige Wetter-ID.");
            else
            {
                API.setWeather(weather);
                PlayerCommunication.SendAdminMessageToAll("aenderte das Wetter zu: " + weather, player);
                //   API.sendNotificationToAll("[ADMIN] " + player.name + "aenderte das Wetter zu: " + weather);
            }

        }

        [Command("settime", Description = "Aendert die Zeit")]
        public void SetTime(Client sendingPlayer, int hours, int minutes)
        {
            Account account;
            if (AccountController.IsClientAdmin(sendingPlayer, out account))
            {
                var data = AccountController.GetData(sendingPlayer);
                if (data == null || data.Admin <= 0)
                {
                    API.sendNotificationToPlayer(sendingPlayer, "Du bist kein Admin!");
                    return;
                }

                API.setTime(hours, minutes);
            }

        }

        #region Private Helpers

       
    

        #endregion
    }

}
